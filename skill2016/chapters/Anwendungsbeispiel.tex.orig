
\section{PacMan}


Im Computerspiel PacMan ist es die Aufgabe des Spielers, die Spielfigur (PacMan) nur durch die Bewegung nach oben, unten, rechts und links durch ein Labyrinth zu steuern und dabei alle gelben Punkte (\textit{Dots}) einzusammeln. Ber�hrt der PacMan bei diesem Prozess einen der vier umherwandernden Geister, verliert er eines seiner Leben.
Sinkt seine Lebensanzahl auf null, verliert der Spieler das Spiel und er muss von vorne beginnen. 
Sammelt der Spieler hingegen mit PacMan den letzten Dot eines Labyrinths ein, gewinnt er.
%Die Geister folgen unterschiedlichen Verhaltensmustern, so dass sich diese oft aus unterschiedlichen Richtungen auf PacMan zu bewegen.
%Diese und weitere Funktionen wurden f�r das Fallbeispiel implementiert. 

\subsection{Zustandsraum}
Das oben beschriebene Spiel entspricht einem Reinforcement Learning Task. Dabei �bernimmt der PacMan die Rolle des Agenten, der f�r eingesammelte Dots belohnt und f�r Tode bestraft wird. 
Um durch das Labyrinth zu wandern, stehen dem Agenten die Bewegungen in alle vier Himmelsrichtungen zur Verf�gung, was den Aktionsraum definiert.
Die Umgebung besteht aus dem Labyrinth, den Geistern und den Dots. 
Anders als in einfacheren RL-Szenarien k�nnen die resultierende Zust�nde nicht allein durch die Position des Agenten beschrieben werden, sondern m�ssen auch die Geister und Dots einbeziehen.
Nimmt man beispielsweise an, dass das Labyrinth aus 100 Feldern besteht, so erg�ben sich \(100!/95!=9,2\cdot 10^{10}\) M�glichkeiten um den PacMan und die Geister zu positionieren und somit auch mindestens \(9,2\cdot 10^{10}\) Zust�nde. 
In der Realit�t ist das Labyrinth jedoch gr��er und auch die Dots wurden bei der Berechnung noch nicht betrachtet.
Diese einfache Rechnung verdeutlicht jedoch, dass es nicht realistisch ist f�r jeden m�glichen Zustand w�hrend des Lernprozesses Erfahrung zu sammeln und die Ergebnisse abzuspeichern. 

Um dieser Herausforderung zu begegnen, wurden in der Vergangenheit verschiedene Konzepte entwickelt, die z.B. Reinforcement Learning mit Genetic Programming kombinieren \cite{5625586} oder Neuronale Netze einbeziehen \cite{Lucas2005} \cite{6615002}.
In der wissenschaftlichen Abhandlung von \cite{ttb14pacman} wurde zudem ein Entwurf vorgestellt, bei dem ein 10 dimensionaler Feature-Vektor mit diskreten Werten verwendet wird. 
Der Zustandsraum wird bei diesem Ansatz auf 2048 m�gliche Zust�nde reduziert, indem vorhandenes Wissen �ber die Umgebung in die Zustandsberechnung einflie�t.
In der Umsetzung der Autoren wird jedoch auch eine Komponente \textit{Richtung zum n�chsten Ziel} bei der Berechnung genutzt.
Sie zeigt entweder die Richtung zum n�chsten Dot oder zur Fluchtm�glichkeit an, falls der Agent von Geistern bedroht wird.
PacMan muss daher lediglich lernen die Richtungsangabe richtig zu interpretieren, um das RL-Problem zu l�sen. 
Das Vorgehen f�hrte jedoch zu einer indirekten Vorgabe des Agenten-Verhaltens, was nicht mit dem klassischen Reinforcement Learning Ansatz vereinbar ist. 
%Das Ziel der vorliegenden Arbeit ist eine Implementierung zu erzielen, in der ebenfalls durch abstrakte Zusatzinformationen eine Approximierung der Zust�nde erreicht wird, die aber den Nachteil der Realisierung von Tziortziotis et al. nicht aufweist. (eventuell formulieren: dem PacMan erlaubt frei zu lernen? Wie war das damals noch?)
%Um den Lernprozess in einem solchen Szenario zu unterst�tzen ist es daher sinnvoll vergleichbare Situationen zusammenzufassen und die Zust�nde zu approximieren.




\subsection{L�sungsansatz}
%Bisher wurde die Theorie zu Reinforcement Learning eingef�hrt und anhand des Simulationsmodus der Anwendung visualisiert.
%In diesem Abschnitt geht es um das Anwenden der Theorie auf den spezifischen Fall \textit{PacMan}.
%Dazu wird zun�chst allgemein die PacMan Umgebung eingef�hrt und anschlie�end die Realisierung des Szenarios mit Approximate Q-Learning erl�utert. 
%Dies beinhaltet insbesondere die Erkl�rung der gew�hlten Feature-basierten Zust�nde.

%Um die RL-Umgebung des Anwendungsbeispiels erl�utern zu k�nnen, werden zun�chst die Grundlagen des original Spiels kurz vorgestellt. 


%
%Um verschiedene Szenarien untersuchen zu k�nnen, existieren unterschiedliche Funktionen zum Anpassen der Umgebung.
%Es ist m�glich zwischen verschiedenen Labyrinthen, Geisterkonstellationen und Dot-Belegungen zu w�hlen.
%Die Werte der Belohnungen und Strafen, die der Agent w�hrend einer Episode sammelt, k�nnen ebenfalls angepasst werden.
%Belohnungen werden f�r einen Sieg sowie f�r das Einsammeln eines Dots vergeben.
%Strafen erh�lt der Agent, wenn er auf einen Geist st��t, sich auf ein leeres Feld bewegt oder gegen die Wand l�uft.
%Abbildung \ref{fig:runninggame} zeigt das Ergebnis der Umsetzung am Beispiel einer laufende Spielrunde.

%\begin{figure}[h!]
%	\centering
%	\includegraphics[width=1.0\textwidth]{runninggame}
%	\caption{Laufende Spielrunde mit Einstellungsm�glichkeiten}
%	\label{fig:runninggame}
%\end{figure}

%Pac die Rolle des Agenten.
%Seine Umgebung besteht aus dem Labyrinth, den Geistern und den Punkten die der Pac
%fressen muss. Um sein Ziel zu erreichen stehen dem Pac als Aktionen die Bewegungen in alle
%vier Himmelsrichtungen zur Verf�gung. Schafft es der Pac durch das Ausf�hren einer Aktion
%einen Punkt zu fressen ohne einen Geist zu ber�hren, wird der Pac belohnt. L�uft er jedoch
%gegen eine Wand, trifft auf einen Geist oder wandert auf leeren Feldern des Labyrinths umher,
%wird er bestraft.
%
%Zu Beginn einer Episode befinden sich PacMan und die ausgew�hlten Geister jeweils auf den, durch das Labyrinth festgelegten, Startpositionen. 
%Die Dots sind je nach gew�hlter Belegungsart im Labyrinth verteilt.
%Eine Episode endet, wenn PacMan das Spiel auf zuvor beschriebene Weise gewinnt oder verliert.
%Die Zust�nde der Umgebung k�nnen, im Gegensatz zu denen der Simulations-Umgebung aus Abschnitt \ref{ss:simulationsmodus}, nun nicht mehr allein durch die Position des Agenten beschrieben werden.
%Dies liegt daran, dass sich die jetzige Umgebung, aufgrund sich bewegender Geister und verschwindender Dots, dynamisch ver�ndert.
%Abh�ngig von den gew�hlten Einstellungen, kann daher ein sehr gro�er Zustandsraum entstehen.
%In diesem Fall k�nnen nicht alle Zust�nde w�hrend des Lern-Prozesses besucht werden.
%Hier ist das Zusammenfassen von vergleichbaren Situationen durch approximative Zust�nde notwendig.

In der L�sung von \cite{ttb14pacman} werden insgesamt zehn Features verwendet um Approximate Q-Learning zu realisieren. Acht davon stellen dar, ob sich eine Wand oder ein Geist in eine bestimmte Himmelsrichtung direkt neben dem PacMan befindet. Diese Features wurden in dem hier vorgeschlagenen L�sungsansatz konsolidiert. Zudem wurde auf das Feature \textit{Richtung zum n�chsten Ziel} verzichtet und stattdessen eine CLOSEST\_DISTANCE\_TO \_FOOD eingef�hrt. Als Resultat ergeben sich entsprechend die folgenden Features: 

%Dementsprechend m�ssen die ben�tigten Features der jeweiligen Zust�nde berechnet werden.
%Dazu wird unter anderem die Bewegung von PacMan simuliert.
%Die Bewegungen der Geister werden jedoch nicht ber�cksichtigt.
%Befindet sich der Agent beispielsweise in \(s\) und h�tte die Aktionen \(a_1,\dots,a_n\) zur Auswahl, dann wird f�r \(f(s,a_1)\) die Aktion \(a_1\) auf ihn angewandt, wodurch seine Bewegung simuliert und das Feature %berechnet wird.
%Dies wird f�r alle Aktionen und die folgenden Features durchgef�hrt.

\begin{itemize}
	\item \textbf{WALKED\_INTO\_WALL }\(\;\;\;\;f_{\text{wall}}: S \times A \to \{0,1\}\) \\
	\(f_{\text{wall}}(s,a)=1\), wenn PacMan gegen eine Wand l�uft, sonst betr�gt der Wert \(0\).
	Dieses Feature soll verhindern, dass der Agent gegen die Wand l�uft. Da der Wertebereich keine negativen Zahlen enth�lt, muss das Gewicht durch das Lernen mit einer negativen Belohnung selbst negativ werden, um den Gesamtwert \(Q(s,a)\) (siehe Gl. \ref{eq:approx_q_learning}) zu mindern.
	
	\item \textbf{GHOST\_THREAT} \(\;\;\;\;f_{\text{threat}}: S \times A \to  [0,4]\) \\
	Die Bedrohung wird durch das Feature
	\begin{equation}
	f_{\text{threat}}(s,a)= t_{g_1}(s,a) + t_{g_2}(s,a) + t_{g_3}(s,a)+ t_{g_4}(s,a)
	\end{equation}
	berechnet.
	Die Funktion \(t_{g_i}:S \times A \to [0,1]\) repr�sentiert die Bedrohung durch Geist \(i \in \{1,2,3,4\}\).
	Wenn Geist \(i\) getroffen wird, gilt \(t_{g_i}(s,a)=1\).
	In diesem Fall befinden sich Agent und Geist auf dem gleichen Feld. \(t_{g_i}(s,a)\) ist \(0\), wenn sich der \(i\)te Geist acht oder mehr Schritte von PacMan entfernt befindet\footnote{Die Distanz acht basiert auf \cite{ttb14pacman}.}.
	Dazwischen wird linear interpoliert.
	Durch dieses Feature soll der Agent die N�he der Geister meiden.
	
	\item \textbf{TRAPPED\_SITUATION} \(\;\;\;\;f_{\text{trapped}}: S \times A \to  \{0,1\}\) \\
	\(f_{\text{trapped}}(s,a)\) ist \(1\), wenn  PacMan von Geistern umschlossen ist.
	Damit ist gemeint, dass f�r den Agenten keine M�glichkeit besteht, den Geistern zu entkommen, es sei denn die Geister bewegen sich in dieser Situation suboptimal.
	Der Agent soll nicht in solche Situationen gelangen, da er anschlie�end fast immer von einem Geist gefangen wird. \\
	Es ist wichtig anzumerken, dass die Geistbewegungen nicht simuliert werden und somit nicht festzustellen ist, ob die Bewegung eines Geistes den PacMan umschlie�en w�rden.\\
	Zur leichteren Realisierung wurde dieses Feature daher so implementiert, dass PacMan als nicht umschlossen gilt, wenn er sich mindestens acht Felder weit bewegen kann.
	Es sind allerdings durchaus Labyrinthe denkbar, bei dem sich PacMan acht Felder bewegen k�nnte und dennoch umschlossen w�re.
	
	\item \textbf{CLOSEST\_DISTANCE\_TO\_FOOD} \(\;\;\;\;f_{\text{food}}:S \times A \to  [0,1]\) \\
	\(f_{\text{food}}(s,a)\) ist \(1\), wenn PacMan einen Dot isst und \(0\), wenn sich ein Dot
	\begin{equation}
		d = \sqrt{w^2+h^2}
	\end{equation}
	Schritte oder weiter von dem Agenten entfernt befindet.
	\(h\) und \(w\) stehen f�r die H�he und die Breite des Labyrinths, was \(d\) zur Diagonale macht.\footnote{\(d\) dient lediglich als eine L�nge, um \(f_{\text{food}}\) auf das Intervall \([0,1]\) normieren zu k�nnen.
	Bei einem ersten Versuch wurde die Summe \(h+w\) anstelle von \(d\) verwendet, jedoch lieferte diese zu hohe Feature-Werte, so dass die meisten beobachteten Werte im Intervall \([0.8,1]\) lagen.
	Die Diagonale hingegen liefert eine bessere Verteilung.}
	Dieses Feature soll den Agenten zum Essen animieren.
	Au�erdem gilt \(f_{\text{food}}(s,a)=0 \), wenn \(f_{\text{wall}}(s,a)=1\). Dies soll verhindern, dass das Gewicht \(w_{\text{food}}\) durch das Laufen gegen eine Wand gemindert wird.
	
	\item \textbf{GHOST\_IS\_A\_NEIGHBOR} \(\;\;\;\;f_{\text{neighbor}}: S \times A \to \{0,1\}\) \\
	\(f_{\text{neighbor}}(s,a)\) ist \(1\), wenn sich ein Geist auf dem gleichen oder einem  benachbarten Feld (links, rechts, ober- oder unterhalb) von PacMan befindet.
	F�r alle anderen F�lle gilt \(f_{\text{neighbor}}(s,a)=0\).
	Dieses Feature soll zus�tzlich zu \(f_{\text{threat}}\) dazu beitragen, dass der Agent akuten Bedrohungen aus dem Weg geht.
	Auch hier gilt \(f_{\text{food}}(s,a)=0 \), wenn \(f_{\text{neighbor}}(s,a)=1\), um eine Verminderung des Wertes zu verhindern.
\end{itemize}

\todo[inline]{Diesen Abschnitt weglassen? Er geh�rt eigentlich nicht zu den Features sondern zur Implementierung? Alternativ: Den Abschnitt abrunden und sagen wie das �ndern der Gewichte zum Ziel f�hrt?}

%Die f�nf genannten Features werden zur Umsetzung von Approximate Q-Learning verwendet. 
Das Lernen des Agenten mit den vorgeschlagenen Features beginnt damit die jeweiligen Gewichte 
\(w_\text{wall}\),
\(w_\text{threat}\),
\(w_\text{neighbor}\),
\(w_\text{trapped}\) und
\(w_\text{food}\)
 mit \(1,0\) zu initialisieren.
In jedem nachfolgenden Schritt erh�lt der Agent den aktuellen Zustand \(s\) und die m�glichen Aktionen \(A\).
\(s\) erh�lt f�r alle Aktionen \(a \in A\) die vorberechneten numerischen Werte \( f_i(s,a)\) mit \(i \in \{\text{wall}, \text{threat}, \text{neighbor}, \text{trapped}, \text{food}\} \).
Der Agent bewertet die approximierten Q-Werte \(Q(s,a)\) nach Gl. \ref{eq:approx_q_learning}, w�hlt eine Aktion \(a\) aus und bewegt sich entsprechend.
Anschlie�end bewegen sich die Geister.
Am Ende eines Schrittes wird eine Belohnung an PacMan ausgesch�ttet und die Gewichte \(w_i\) gem�� Gl. \ref{eq:approx_q_learning_update}) aktualisiert.


%Der Benutzer kann dem Agenten beim Spielen zusehen oder ihn im Hintergrund mehrere Episoden im Schnelldurchgang laufen lassen. Dabei werden Siege und Niederlagen ausgewertet um Statistiken zu sammeln. Dadurch besteht die M�glichkeit Agenten mit verschiedenen Einstellungen gegen�ber zu stellen, diese zu vergleichen und somit die bestm�gliche Einstellung f�r die jeweilige Umgebung zu finden.

\subsection{Beobachtungen und Ergebnisse}

Um die Eignung der Features zu analysieren, wurde eine Java Applikation entwickelt. 
Sie erlaubt das Approximate Q-Learning Verfahren mit den vorgestellten Features in verschiedenen Umgebungen zu visualisieren und auf Basis von gesammelten Spielstatistiken zu vergleichen.
In einem ersten Beispiel wurde mithilfe der Anwendung ein Szenario betrachtet, in dem sich PacMan in einem kleinen Labyrinth mit zwei Geistern beweisen muss. 
Es stellte sich heraus, dass die ausgew�hlten Features f�r diesen Anwendungsfall sehr wirksam sind. 
PacMan siegt bereits nach einer Episode in jeder weitere Spielrunde. 
Au�erdem wurde beobachtet, dass der Agent oft sehr intelligent agiert. 
Beispielsweise l�uft er teilweise absichtlich gegen die Wand, um danach besser von den Geistern fl�chten zu k�nnen.

Bei der Analyse des regul�ren PacMan-Szenarios fallen die Ergebnisse weniger optimal aus.
Nach etwa 100 Episoden erreicht der Agent sein h�chstes Leistungsniveau (siehe Abbildung \ref{fig:pacman_max_score}).
% Auf diesem erzielt er im Schnitt 3.000 Punkte (siehe Abbildung \ref{fig:pacman_max_score}). Wobei ihm jeder eingesammelten Dot 40 Punkte Belohnung einbringt. F�r ein gewonnenen Spiel, werden nochmal 1000 Punkte ausgesch�ttet. 
Er gewinnt jedoch nur etwa 3\% aller Spiele. Es ist daher davon auszugehen, dass die Features f�r dieses Szenario weiter verfeinert und �berarbeitet werden m�ssen.

\begin{figure}[h!]
	\centering
	\includegraphics[width=1.0\textwidth]{pacman_avg_score}
	\caption{PacMan erreicht nach 100 Episoden sein h�chstes Leistungsniveau. Dort erzielt er im Schnitt 1750 Punkte. Wobei ihm jeder eingesammelten Dot 40 Punkte Belohnung einbringt. F�r ein gewonnenen Spiel, werden nochmal 1000 Punkte ausgesch�ttet.}
	\label{fig:pacman_max_score}
\end{figure}

Einige Features haben sich in den Beobachtungen zudem anders entwickelt als erwartet. 
So hat sich gezeigt, dass sich hinter PacMan beim Einsammeln von Dots oft einen Geist befindet. 
Dadurch erlangt der Agent aufgrund des Features GHOST\_THREAT den Eindruck, dass es positiv sei, wenn ihn beim Fressen ein Geist verfolgt.
In nachfolgenden Episoden kommt es vor, dass der Agent zun�chst auf einen Geist zul�uft und erst mit dem Einsammeln der Dots fortf�hrt, wenn dieser ihm mit einem Feld Abstand folgt. 
Weiterhin wurde GHOST\_THREAT in manchen F�llen so gro�, dass es die Entscheidungen des Agenten zu stark negativ beeinflusste.
Durch diesen Umstand z�gerte PacMan in bestimmten Situationen einen Dot einzusammeln, obwohl ein menschlicher Spieler erkennen w�rde, dass das Risiko vertretbar ist. 
Es schien ferner so, dass das Erforschen der Umgebung in bestimmten F�llen bei Approximate Q-Learning zu einem schlechteren Ergebnis f�hrt. 
In vielen Szenarien verh�lt sich der Agent durch das Erforschen sehr ungeschickt, l�uft beispielsweise oft ergebnislos gegen die Wand und vermeidet es Dots zu essen. 
<<<<<<< HEAD

Auf Basis der gewonnen Erkenntnisse wurden mehrere Verbesserungsm�glichkeiten f�r die vorgestellten Features und das Lernverfahren identifiziert:
\begin{itemize}
	\item Auf das Feature GHOST\_IS\_NEIGHBOR kann m�glicherweise verzichtet werden, da GHOST\_THREAT bereits die Bedrohung durch die Geister abbildet.
	\item Eine Verringerung der Distanz von acht Schritten f�r GHOST\_THREAT k�nnte den Agent anregen Dots in solchen Situationen zu essen, in
	denen er zurzeit noch wegl�uft.
	\item Es w�re denkbar, GHOST\_THREAT in Himmelsrichtungen aufzuteilen und somit bessere Ergebnisse zu erzielen.
	\item Unter Einbeziehung der Geistbewegungen w�re es f�r den Agenten m�glich bereits einen Schritt im Voraus zu denken und somit die Gefahr durch die Geister besser einzusch�tzen.
\end{itemize}
=======
Folglich besteht die M�glichkeit den Agenten durch Anpassen der Features noch weiter zu verbessern.
Im folgenden Kapitel werden entsprechende Verbesserungsm�glichkeiten genannt.
>>>>>>> fazit und danksagung file
