'''
Created on Jul 22, 2015

This small script calculates the values for the Value Learning example of the documentation.
See chapter 2 'Value Learning'.
'''

_ALPHA = 0.5
_GAMMA = 0.


def _calc_value_learning_example():

    V = {"s0":0.0,
              "s1":0.0,
              "s2":0.0,
              "s3":0.0,
              "sm":0.0,
              "sp":0.0}

    nr_iterations = 100

    for i in range(1, nr_iterations+1):
        for s in ("s0", "s1", "s2"):
            a = _policy(s)
            sP = _get_succesor(s, a)
            r = _reward(s, a, sP)

            sample = r + _GAMMA *  V[sP]
            new_val = (1-_ALPHA) * V[s] + _ALPHA * sample
            V[s]=  new_val
        _pretty_print(i, V)

def _policy(s):
    policy = {"s0": "o", "s1": "a_r", "s2": "u"}
    return policy[s];

def _get_succesor(s, a):
    transition = {
                  "s0": {"o": "s1"},
                  "s1": {"a_r": "s2"},
                  "s2": {"u": "sm"}
                  }
    return transition[s][a]

def _reward(s, a, sP):
    if s=="s2" and a=="u" and sP=="sm":
        return -1
    else:
        return 0

def _pretty_print(iteration_nr, V):
    print("After Iteration {}".format(iteration_nr))
    for s in V.keys():
        print("V({}): {}".format(s, V[s]))
    print("")

if __name__ == '__main__':
    _calc_value_learning_example()
