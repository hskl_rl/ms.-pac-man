\graphicspath{{images/appB/}}

\chapter{Aufgaben}
\label{app:app01}

Die folgenden Abschnitte bieten die Möglichkeit die in diesem Dokument vorgestellten Algorithmen zur Lösung von RL Problemen programmatisch umzusetzen.
Außerdem können wichtige Sachverhalte an praktischen Beispielen nachvollzogen werden. 
Als Grundlage für die Aufgaben dienen die in den Kapiteln \ref{chapter:rl_vis} und \ref{chapter:pacman} betrachteten Szenarien und die Anwendung die im Rahmen dieser Projektarbeit entstanden ist.  

\section{Umsetzung der Algorithmen}
\label{app:app01_implementierung}

Für alle in dieser Abhandlung aufgeführten Reinforcement Learning Algorithmen sind bereits entsprechende Stub-Klassen im Package \texttt{model.reinforcement\_learning.value\_function} vorgesehen.
Die Stub-Klassen sind im \textit{Git-branch exercises} zu finden.
Zu diesem \textit{branch} kann man mit dem git-Befehl \texttt{git checkout exercises} wechseln.\footnote{Für weitere Informationen:
	\url{https://git-scm.com/book/de/v1/Git-Branching-Einfaches-Branching-und-Merging}, besucht am 28.07.2015}
Um die entsprechenden Stellen im Quellcode leichter zu finden, ist es möglich nach der Zeichenfolge \glqq TODO: implement\grqq zu suchen. 
Sofern Eclipse als Entwicklungsumgebung eingesetzt wird, sind diese ausstehenden Implementierungen in der View \glqq Tasks\grqq aufgelistet. Andere IDEs besitzen ähnliche Funktionen. 
Sollten die zu bearbeitenden Stellen unbehandelt aufgerufen werden, so löst dies \textit{NotImplemented}-Exceptions aus.

Alle für die Realisierung relevanten Abhängigkeiten und Eingabeparameter der Klassen werden in den jeweiligen Aufgabenstellungen eingeführt.
Für einen Überblick über die Funktionsweise der Applikation, sind die wichtigsten Komponenten und ihre Beziehungen zueinander in Anhang \ref{app:app02_rl_components} dargestellt.

\subsection{Value Learning}
Zunächst wird Reinforcement Learning auf Basis der Lernmethode Value Learning betrachtet. Dazu müssen in der Klasse \texttt{ValueLearning} des oben genannten Packages alle fehlenden Methoden implementiert und eine Datenstruktur zum Abspeichern der Werte \(V(s)\) erstellt werden. 
Diese Struktur ist in den Konstruktoren der Klasse geeignet zu initialisiert. 
Von besonderem Interesse sind darüber hinaus die beiden noch leeren Funktionen \texttt{updateValueWith(ValueParams params)} und \texttt{getValue(ValueParams params)}. 
Die erste Methode soll die Zustandswerte aktualisieren wenn ein Übergang von Zustand \(s\) nach \(s'\) stattfindet. 
Durch die \texttt{ValueParams} stellt die Anwendung hier bereits alle Angaben zur Verfügung, die neben den aktuellen Zustandswerten zur Berechnung notwendig sind. 
Über folgende Methoden der \texttt{ValueParams} können diese Informationen zugegriffen werden: 

\singlespacing
\begin{lstlisting}
params.getState()  //get s  as State Object
params.getSPrime() //get s' as State Object
params.getReward() //get r  as Reward Object
params.getAction() //get a  as Action Object
\end{lstlisting}
\onehalfspacing

Den aktuellen Wert eines bestimmten Zustands sollte die Methode \texttt{getValue(ValueParams params)} zurückliefern. 
Hierbei kann die eindeutige ID der Zustände verwendet werden, welche man über \texttt{state.getSimpleId()} erhält.

Bei Bedarf können die theoretische Beschreibung und die Illustration der Funktionsweise von Value Learning in Kapitel \ref{sub:value_learning} und \ref{sub:value_learning_visualization} rekapituliert werden. 
Am Ende ist es sinnvoll den neuen Quellcode über den Punkt Start Simulation (Manual) der Anwendung zu testen und die Ergebnisse mit den Zustandswerten aus den Beispielen von Abschnitt \ref{sub:value_learning_visualization} zu vergleichen.

\subsection{Q-Learning}
Diese Aufgabe widmet sich der praktischen Umsetzung von Q-Learning auf Basis der theoretischen Ausführungen aus Abschnitt \ref{subsec:theory_q_learning}. 
Ein geeignete Stub-Klasse \texttt{QLearning} ist in dem oben genannten Package des Java Projekts zu finden. 
Bei der Implementierung ist zu beachten, dass der Algorithmus auf Q-Werten \(Q(s,a)\) basiert. Dazu muss die Datenstruktur zum Abspeichern der Werte geeignet erweitert werden. 
Alle Aktionen, die der Agent in einem Zustand ausführen kann, erhält man von der Anwendung beim Erstellen eines Q-Learning Objekts über den Parameter \texttt{actionsForInitStateMap}. Ähnliches gilt für die Lernrate \texttt{learningRate} und den Verminderungsfaktor \texttt{discountRate} für die Berechnung der Q-Werte. In Konstruktoren, die keinen Parameter für \(\alpha\) und  \(\gamma\) vorsehen, ist es erforderlich sinnvolle Standardwerte vorzugeben.
Es ist vorgesehen, dass die zu implementierende Methode \texttt{getValue(ValueParams params)} entweder einen bestimmten Q-Wert \(Q(s,a)\) oder, falls keine Aktion \(a\) in den \texttt{ValueParams} spezifiziert wurde, den maximalen Q-Wert des Zustands \(s\) zurückliefert. 
Auf diese Weise kann sie für \texttt{getMaxValueOf(State state)} wiederverwendet werden. \texttt{QLearning(QLearning qviToCopy)} dient als Kopierkonstruktor. 
Aufgrund der Anwendungslogik muss der Konstruktor bei diesem Vorgang eine tiefe Kopie von allen vorhandenen Datenstrukturen erstellen.

Um die Korrektheit der Implementierung zu verifizieren, ist es möglich die Anwendung auszuführen und die Szenarien aus Abschnitt \ref{sub:q_learning_visualization} zu reproduzieren. 
Da bei Q-Learning die Werte konvergieren, sollten die Ergebnisse identisch sein.


\subsection{Sarsa-Learning}
Ergänzend zu Q-Learning sollen nun alle fehlenden Bestandteile der Klasse \texttt{Sarsa} implementiert werden. Da \texttt{Sarsa} von \texttt{QLearning} erbt, kann hierzu auf die zuvor erarbeitete Lösung für Q-Learning zurückgegriffen und Methoden wie \texttt{getValue(ValueParams params)} und \texttt{getMaxValueOf(State state)} bei Bedarf wiederverwendet werden. 
Allerdings ist zu beachten, dass Sarsa die Zustandswerte in \texttt{updateValueWith(ValueParams params)} erst um einen Zeitschritt versetzt aktualisieren darf. 
Das bedeutet, dass Angaben wie der Vorgängerzustand und die Vorgängeraktion für diesen Zweck entsprechend zwischenzuspeichern sind.

In der Methode \texttt{updateValueWith} ist es außerdem sinnvoll zu prüfen, ob der Zielzustand einem Endzustand entspricht. 
In diesem Fall ist \(s\) ein präterminaler Zustand und muss direkt berechnet werden.
Andernfalls würde die Episode enden und der Wert bliebe unverändert. 



\subsection{Approximate Q-Learning} 

Nachdem bisher nur Algorithmen aus dem Bereich der Visualisierung in Kapitel \ref{chapter:rl_vis} betrachtet wurden, wendet sich diese Aufgabe dem Fallbeispiel PacMan zu. 
Wie in Kapitel \ref{chapter:pacman} erläutert, wird für dieses Szenario Approximate Q-Learning eingesetzt, welches eine featurebasierte Repräsentation der Zustände vorsieht. 
Es ist dieses mal nicht erforderlich alle Methode der Stub-Klasse \texttt{ApproximateQLearning} mit zu implementieren. 
Die meisten Datenstrukturen und Funktionen sind bereits vorgegeben. 
Lediglich die Methode \texttt{updateValueWith(ValueParams params)} fehlt. 
Sie muss die Gewichte der Features neu berechnen und in dem Array \texttt{featureWeights} abspeichern. 
Alle dazu benötigten Features erhält man über die Anweisung \texttt{((FeaturesState) s).getFeatures(params)}.

Mehr Informationen zur Funktionsweise des Algorithmus sind im Abschnitt \ref{sub:approx_q_learning} und Kapitel \ref{chapter:pacman} zu finden. 

Zum Testen der Umsetzung ist es hilfreich zunächst einen Breakpoint in die Methode \texttt{updateValueWith(ValueParams params)} zu setzen und die Anwendung im Debug Mode zu starten. 
Über den Punkt \glqq Start Reinforcement Learning\grqq im Menü zeigt die Applikation eine Übersicht an. 
Sie dient dazu verschiedene PacMan Szenarien als sogenannte \glqq Tasks\grqq zu hinterlegen.  
Als Test-Task eignet sich ein neuer einfacher Task mit folgenden Einstellungen bei \glqq Game Conditions\grqq:
\begin{itemize}
\item Maze: Simulation
\item Ghost: Pinky oder Blinky
\item Dots: One
\end{itemize}
Die anderen Werte können auf den Standardvorgaben belassen werden.
Danach ist es möglich diesen Task zu trainieren und dabei nachzuverfolgen ob und wie die Gewichte der Features aktualisiert werden. 
Nach einigen Episoden sollte PacMan dem Geist ausweichen, den Dot finden und gewinnen.

\section{Verstehen}

In den folgenden Aufgaben wird das Verständnis rund um die Funktionsweise von Q-Learning, Sarsa-Learning und Value-Learning vertieft. Dazu dient wieder die Reinforcement Learning Applikation.

\subsection{Value-Learning}

Zuerst soll das Konvergenzverhalten von Value-Learning in Bezug auf die beiden Methoden zur Aktionsauswahl greedy und \(\epsilon\)-greedy analysiert werden. Dazu ist es notwendig die Applikation zu starten und den Punkt \glqq Start Simulation (Manual)\grqq zu wählen. Danach ist sicherzustellen, dass folgende Einstellungen in den Feldern hinterlegt sind:

\begin{itemize}
\item Maze: Simulation
\item Training Method: Value Learning
\item Discount Rate: 0.9
\item Learning Rate: 0.1
\item Action Selection Policy: Greedy
\item Epsilon: 0.3
\end{itemize}

Weichen die Angaben ab, können die Werte über den \glqq Edit\grqq  bearbeitet und mit \glqq Adopt Values\grqq übernommen werden. Im Feld \glqq Episoden (no visual tracking)\grqq sind vorerst zehn Episoden einzutragen, damit der Agent anschließend über \glqq Go\grqq trainiert werden kann. Dadurch spielt PacMan die entsprechende Anzahl an Episoden in der Umgebung und die Zustandswerte werden am UI aktualisiert. 

Welches Verhalten ist beim wiederholten Trainieren des Agenten zu beobachten? Konvergieren die Zustandswerte \(V(s)\) nach 150 Episoden?

Um greedy und \(\epsilon\)-greedy zu vergleichen, muss nun die Aktionsauswahl verändert werden. Die Optionen sind jetzt wie folgt zu wählen:

\begin{itemize}
\item Action Selection Policy: Epsilon-Greedy
\item Epsilon: 0.3
\end{itemize}

Wie stellt sich die Konvergenz der Werte bei diesen Einstellungen nach 10 $\cdot$ 100 Episoden dar? Auf welchen Umstand ist das beobachtete Verhalten zurückzuführen? Was könnte im schlechtesten Fall passieren, wenn ohne Exploration gelernt wird? 

\subsection{Q-Learning vs. Sarsa-Learning}
Abschließend soll noch einmal der Unterschied zwischen Q-Learning und Sarsa-Learning sowie greedy und \(\epsilon\)-greedy untersucht werden. Hierzu wird die schon zuvor verwendete Simulation mit folgenden Einstellungen aufgerufen:

\begin{itemize}
\item Maze: Bridge
\item Training Method: Q-Learning
\item Discount Rate: 0.9
\item Learning Rate: 0.1
\item Action Selection Policy: Epsilon-Greedy
\item Epsilon: 0.3
\end{itemize}

Welches Konvergenzverhalten tritt bei Q-Learning bezüglich der Q-Werte \(Q(s,a)\) nach 10 $\cdot$ 10000 Episoden auf?
Was geschieht wenn der Agent die gleiche Anzahl Episoden nicht \(\epsilon\)-greedy sondern greedy trainiert?\footnote{Um das zu testen, muss die Simulation neu gestartet und die Einstellungen erneut vorgenommen werden.}
Konvergieren die Werte? Wie unterscheidet sich die Einfärbung der beiden Spielfelder voneinander?

Zuletzt werden folgende Optionen gewählt und mit Sarsa gelernt:

\begin{itemize}
\item Training Method: Sarsa
\item Action Selection Policy: Epsilon-Greedy
\item Epsilon: 0.3
\end{itemize}

Unterscheidet sich das Feld mit Sarsa nach 10 $\cot$ 10000 Episoden von dem Feld mit Q-Learning und \(\epsilon\)-greedy? Welcher Weg wird von den beiden Algorithmen im Allgemeinen gewählt? Um die Vermutung zu überprüfen, können einzelne Episoden über den \glqq Start\grqq -Button nachverfolgt werden.
