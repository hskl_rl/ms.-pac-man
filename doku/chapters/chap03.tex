\graphicspath{{images/chap03/}}

\chapter{Reinforcement Learning Visualisierung}
\label{chapter:rl_vis}

Dieses Kapitel dient dazu, dem Leser einen tieferen Einblick in die Funktionsweise der drei TD-Algorithmen Value-Learning, Q-Learning und Sarsa-Learning zu verschaffen.\\
Hierzu wird ein Reinforcement Learning Szenario betrachtet, in welchem sich der Agent in einer der in Abbildungen \ref{fig:chap03_sim_szenario_1} und \ref{fig:chap03_sim_szenario_2} dargestellten Umgebungen befindet\footnote{Die in diesem Kapitel vorkommenden Abbildungen sind dem Simulationsmodus, der im Rahmen dieser Arbeit entwickelten Anwendung (siehe Anhang \ref{app:app02}), entnommen. Die folgenden Beispiele können deshalb vom Leser mit Hilfe der Anwendung praxisnah und selbständig nachvollzogen werden.}.
\begin{figure}[h!]
  \centering
    \includegraphics[width=0.8\textwidth]{sim_szenario1}
    \caption{Das Simulation Labyrinth mit Geist, Dot, Agent und einfachen Zustandswerten}
    \label{fig:chap03_sim_szenario_1}
\end{figure}
\begin{figure}[h!]
  \centering
    \includegraphics[width=0.8\textwidth]{sim_szenario2}
    \caption{Das Bridge Labyrinth mit Geistern, Dot, Agent und einfachen Zustandswerten}
    \label{fig:chap03_sim_szenario_2}
\end{figure}
Beide entsprechen unterschiedlichen Labyrinthen eines stark vereinfachten PacMan Spiels\footnote{Stark vereinfacht bedeutet in diesem Zusammenhang, dass sich die Geister nicht bewegen und es nur einen Dot zu fressen gibt}.
Dabei stellt jedes Feld eines Labyrinths einen Zustand der aktuellen Umgebung dar.
Darüber hinaus entsprechen manche Felder einem der sogenannten \textit{präterminalen Zustände} $s_d$, $s_{g1}$, ... , $s_{gn}$. 
Diese werden durch die statischen Geister $s_{g1}$, ..., $s_{gn}$ und einen kleinen, gelben Punkt $s_d$ (\textit{Dot}) repräsentiert. 
Ein präterminaler Zustand kann als eine Art Vorstufe zum eigentlichen Endzustand $s_t$ der Umgebung angesehen werden. 
Demnach ist $s_t$ der Folgezustand eines jeden präterminalen Zustands. 
Außerdem markiert er das Ende einer Episode.
Der große, gelbe Punkt der Umgebung stellt den Agenten beziehungsweise den PacMan dar. 
Dieser kann sich innerhalb des Labyrinths in alle vier Himmelsrichtungen bewegen.
Dazu wählt er in jedem Entscheidungszyklus zwischen den Aktionen \(A = \{a_n, a_e, a_s, a_w\}\).
Es ist ihm jedoch nicht möglich gegen eine Wand zu laufen.
Beispielsweise kann der Agent in Abbildung \ref{fig:chap03_sim_szenario_1} nur die Aktionen \(a_w\) und \(a_e\) auswählen.
Führt ihn eine seiner Aktionen in einen der präterminalen Zustände, wechselt die Umgebung automatisch im nächsten Zeitschritt in den Endzustand. 
Bei diesem Wechsel erhält der Agent für das Erreichen des Dots eine Belohnung.
Sollte er anstelle des Dots einen Geist berührt haben, wird er bestraft.\\
Zu Beginn sind dem Agenten die Zustände, deren Werte und somit die Konsequenzen seiner Aktionen noch unbekannt.
In den folgenden Abschnitten wird erläutert wie der Agent mit Hilfe der verschiedenen RL-Algorithmen die Umgebung kennenlernt und langsam versteht, dass er die Geister meiden und sich auf den Dot zubewegen muss.

\section{Value-Learning}
\label{sub:value_learning_visualization}
Wie bereits in Abschnitt \ref{sub:value_learning} erwähnt, bietet Value-Learning die Möglichkeit eine Policy zu evaluieren.
Wollen wir unseren Agenten mit dieser Methode aktiv lernen lassen, müssen entsprechende Anpassungen vorgenommen werden.
Während das Evaluieren modell-frei vonstatten gehen kann, tauchen bei der Aktionsauswahl Probleme auf.
Man kann mit dem Zustand \(s\), seinem Wert \(V(s)\) und den möglichen Aktionen \(A\) nicht bewerten, welche Aktion die für den Agenten zu einem bestimmten Zeitpunkt die beste Wahl wäre.
Somit würde eigentlich eine feste Policy $\pi$ benötigt, die den Agenten anweist, welche Aktion er in welchem Zustand wählen soll (Beispiel: $\pi(s_0)=a_n$). 
Diese Vorgaben erlauben jedoch nicht die Gesamtbelohnung zu maximieren, es sei denn, es läge bereits die optimale Policy $\pi^*$ vor. 
Um einen zu Sarsa oder Q-Learning ähnlichen Lernprozess zu erzielen, wird die in Abschnitt \ref{sec:mdp} eingeführte Übergangsfunktion $T:S\times A\times S \to [0,1]$ ergänzt.
Durch diese Erweiterung wird die Aktionsauswahl modell-basiert und es kann aktiv gelernt werden. (siehe Abschnitt \hyperlink{sub:active_passive_learning}{\ref{sub:value_learning}}). 
Auf Anwendungsseite ist diese als \textit{Transition Model} (TM) implementiert, das Informationen über mögliche Folgezustände und deren Wahrscheinlichkeiten  verwaltet.
Statt fester Aktionen zurückzugeben, greift die Policy nun auf das TM zurück und bestimmt die Aktionen anhand der Werte der möglichen Folgezustände.
Es sollte angemerkt werden, dass diese Auswahl nicht so präzise ist, wie bei Sarsa oder Q-Learning.
Der Grund hierfür liegt in der fehlenden Modellierung der Belohnungsfunktion \(R\).
Eine Bewertung, angelehnt an die Bellman Gleichung \ref{eq:bellman_equation}, für einen Zustand \(s\) und eine Aktion \(a\) erfolgt nach \(\sum_{s'} T(s,a,s') [0 + \gamma \; V(s')]\).
Da \(R\) nicht modelliert wird, fließt die Belohnung immer als \(0\) in die Bewertung ein.
Obwohl eine direkte Belohnung demnach nicht berücksichtigt wird, funktioniert das Verfahren sehr gut, da eine unmittelbare Bewertung in der Regel nur schwach in die Gesamtbewertung eines Zustands einfließt.
Würde eine hohe negative Belohnung ausgeschüttet, so wird der Zustand \(s\), der nun einen hohen negativen Wert hat, bei der nächsten Auswahl vermieden.
Folgendes Beispiel verdeutlicht das Vorgehen.

Es wird das in Abbildung \ref{fig:chap03_sim_szenario_1} dargestellte Labyrinth betrachtet. 
In der Ausgangssituation befindet sich der Agent am unteren Rand des Gitternetzes und die Zustandswerte aller Felder sind 0.00. 
Der Agent möchte von seiner Policy $\pi$ wissen, in welche Richtung er sich bewegen soll. 
Dazu übergibt er ihr die im aktuellen Zustand $s_0$ möglichen Aktionen, welche von ihr an das Transition Model weitergeleitet werden. 
Dessen Aufgabe ist es, den wahrscheinlichsten Folgezustand jeder Aktion in $s_0$ zurückzuliefern. 
Im ersten Zeitschritt existieren noch keine Angaben zu möglichen Zustandsübergängen.
In diesem Fall bewertet die Policy alle Bewegungsrichtungen als gleichwertig und leitet die Aktionen samt einheitlicher Werte an den aktuellen Algorithmus zur Aktionsauswahl greedy, $\epsilon$-greedy oder softmax weiter (vgl. Abschnitt \ref{subsec:exploration_exploitation}). 
Dort kann aufgrund der einheitlichen Werte keine der Aktionen präferiert werden, weshalb $a$ zufällig gewählt und an den Agenten geliefert wird.
Anschließend führt der Agent die Bewegung aus und erreicht einen neuen Zustand $s_1$. 
Dieser wird im Transition Model für die Zustands-Aktions-Kombination als möglicher Folgezustand hinterlegt: \((s_0, a) \rightarrow s_1\).
In $s_1$ angekommen erhält der Agent noch kein Feedback von der Umgebung, da er sich im ersten Zug weder auf dem Dot $s_d$ noch auf einem Geist $s_{g1}$ befindet. 
Ohne Rückmeldung wird zwar der Wert des Zustands $s_0$ nach der Value-Learning Formel (vgl. \ref{eq:value_learning_with_difference}) aktualisiert, bleibt jedoch unverändert, weil weder $V(s_0)$ als Startzustandswert noch $V(s_1)$ als Folgezustandswert einen Beitrag leisten. Das Einsetzen in die Value-Learning Formel

\begin{equation}
	\label{eq:value_learning_v_s_0}
	V^\pi(s_0) \leftarrow V^\pi(s_0) + \alpha \; [(r+ \gamma\;V^\pi(s_1)) - V^\pi(s_0)]
\end{equation}

zeigt, dass die Aktionen nicht aussagekräftig bewertet werden können, solange alle Zustandswerte 0.00 sind. 
Der Agent wird daher unter Umständen lange, zufällig und ohne Ergebnis in dem Gitternetz umherlaufen und dabei sein Transition Model aktualisieren. 
Das ändert sich erstmalig, wenn der PacMan den Geist oder Dot erreicht. 
Verlässt der Agent einen dieser präterminalen Zustände, erhält er von der Umgebung  eine Belohnung in Höhe von 1,0 für $s_d$ oder -1,0 für $s_{g1}$. 
Die Belohnung fließt als $r$ in die Formel ein und ergibt zusammen mit der Lernrate $\alpha$ den neuen Wert des präterminalen Zustands.

\begin{equation}
	\label{eq:value_learning_s_d}
	V^\pi(s_d) \leftarrow V^\pi(s_d) + \alpha \; [(r + \gamma \cdot V^\pi(s_t)) - V^\pi(s_d)]
\end{equation}

Geht man beispielsweise davon aus, dass der PacMan wie in Abbildung \ref{fig:chap03_vl_01} und \ref{fig:chap03_vl_02} $s_d$ besucht und eine Lernrate von 0.5 sowie einen Verminderungssatz von 0.9 zugrunde liegt, erhält $s_d$ den Wert 0.5 sobald der Agent durch eine beliebige Aktion in $s_t$ wechselt (siehe Formel \ref{eq:value_learning_example}). 
$\gamma$ nimmt hier keinen Einfluss auf das Ergebnis, da $V(s')=V(s_t)$ immer 0 ist. 

\begin{figure}[h!]
  \centering
    \includegraphics[width=1.0\textwidth]{vl_01}
    \caption{Der PacMan erhält keine Belohnung bis er einen Endzustand erreicht}
    \label{fig:chap03_vl_01}
\end{figure}

\begin{figure}[h!]
  \centering
    \includegraphics[width=1.0\textwidth]{vl_02}
    \caption{Beim Verlassen des Endzustands wird die Belohnung ausgeschüttet und der Wert des Zustands ändert sich}
    \label{fig:chap03_vl_02}
\end{figure}

\begin{equation}
\label{eq:value_learning_example}
	V(s_d) \leftarrow 0 + 0.5 \; [(1.0 + 0.9 \cdot 0) - 0] = 0.5
\end{equation}

Wurde die Belohnung verteilt und der Wert für $s_d$ verändert, endet die Episode und PacMan wird auf die Startposition gesetzt.
Beginnend von dort läuft er erneut zufällig umher, bis er eventuell den Zustand $s_x$ in der direkten Nachbarschaft von $s_d$ erreicht (siehe Abbildung \ref{fig:chap03_vl_03}). 
Hat er zuvor von $s_x$ aus mit $a_n$ den präterminalen Zustand betreten, ist im Transition Model \((s_x, a_n) \rightarrow s_d\) hinterlegt. 
Die Policy ermittelt die möglichen Aktionen für den Zustand $s_x$, erhält vom Transition Model die Zielzustände, erkennt das für den Zielzustand $s_d$ bereits ein (geschätzter) Wert vorhanden ist und verknüpft diesen mit der korrespondierenden Aktion $a_n$. 
Der Algorithmus zur Aktionsauswahl benutzt diese Angabe zusammen mit den Werten der anderen Bewegungsrichtungen (im Beispiel sind diese 0.00) und liefert dem Agenten die Entscheidung zurück.
Ist die Aktionsauswahl greedy, so bewegt sich der Agent in dieser Situation immer auf den Dot zu, da 0.5 der höchste Wert ist. 
Bei softmax oder $\epsilon$-greedy kann es trotz des Vorwissens zu einem explorativen Verhalten kommen. 
In diesem Fall wird mit einer bestimmten Wahrscheinlichkeit eine andere Aktion durchgeführt, um die umliegenden Zustände zu erkunden.
Erkundet der Agent nicht, sondern wechselt in $s_d$ wird der geschätzte Wert von $s_x$ wie in Abbildung \ref{fig:chap03_vl_03} verändert.
Dies liegt daran, dass $V(s_d)$ in der Value-Learning Formel \ref{eq:value_learning_with_difference} einen Beitrag leistet. 
Verlässt der PacMan im Anschluss den präterminalen Zustand, so wird auch dessen Wert berechnet, wodurch sich dieser der Belohnung 1.0 annähert.

\begin{figure}[h!]
  \centering
    \includegraphics[width=1.0\textwidth]{vl_03}
    \caption{Value-Learning mit einer Lernrate von 0.5}
    \label{fig:chap03_vl_03}
\end{figure}

Auf diese Weise breitet sich der Wert, der zuerst nur $s_d$ zugewiesen wurde, langsam über das Gitternetz aus. 
Allerdings wird er durch $\gamma$ immer weiter verringert je mehr er sich vom präterminalen Zustand entfernt. 
Analog verhält sich auch der negative Wert für $s_{g1}$ (siehe Abbildung \ref{fig:chap03_vl_04}).

\begin{figure}[h!]
  \centering
    \includegraphics[width=1.0\textwidth]{vl_04}
    \caption{Value-Learning nach 30 Episoden, mit $\alpha$ = 0,5, $\gamma$ = 0,9 und softmax Aktionsauswahl}
    \label{fig:chap03_vl_04}
\end{figure}

Sobald es einen zusammenhängenden Pfad von der Startposition zum Dot gibt, bei dem alle Zustandswerte > 0.00 sind, werden andere Felder nur noch durch die Aktionsauswahl-Methoden $\varepsilon$-greedy und softmax erkundet. 
Im Falle von greedy würde der Agent ausschließlich dem bereits bekannten, positiven Pfad zum Dot folgen.

Bei genauerer Betrachtung von Abbildung \ref{fig:chap03_vl_04} fällt auf, dass der Zustand links von $s_{g1}$ allgemein als negativ (-0.56) eingestuft wird. 
Genau genommen ist jedoch nur die Aktion $a_e$, welche den Agenten zu $s_{g1}$ führt, als negativ zu bewerten. 
Würde der Agent ausgehend von diesem Zustand jedes Mal den Dot erreichen, würde sein Wert irgendwann positiv. 
Dies zeigt, dass Value-Learning unter besonderen Umständen die Umgebung falsch einschätzen kann, weil die Werte nicht den Aktionen sondern den Zuständen selbst zugewiesen werden. 
Dieser Nachteil wird durch die Lernmethoden Sarsa-Learning und Q-Learning behoben.

\section{Sarsa-Learning}
\label{subsec:sarsa_learning_visualization}
Wie in Abschnitt \ref{subsec:theory_sarsa_learning} bereits beschrieben werden bei Sarsa Q-Werte \(Q(s,a)\) verwendet, welche mithilfe des Tupels \((s,a,r,s',a')\) um eine Iteration versetzt aktualisiert werden.
Entsprechend der Erweiterung zu Q-Werten, verändert sich die Darstellung der Felder der beiden Labyrinthe wie in Abbildung \ref{fig:chap03_sim_szenario_1_sarsa} dargestellt.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{sim_szenario1_sarsa}
	\caption{Das Labyrinth mit Geist, Dot, Agent und Q-Werten}
	\label{fig:chap03_sim_szenario_1_sarsa}
\end{figure}

Die Zustandsfelder sind nun in vier Abschnitte unterteilt. 
Jeder Abschnitt repräsentiert einen der Q-Werte des jeweiligen Zustands.
Dabei entspricht die Himmelsrichtung des Abschnitts der Bewegungsrichtung der Aktion, zu welcher der Q-Wert gehört.

Im Gegensatz zu Value-Learning, kann durch die Verwendung von Q-Werten nun auf das Transition Model und eine fixe Policy verzichtet werden (modell-freie, aktive Lernmethode). 
Dies ist möglich, da jeder Aktion eines Zustands ein eigener Wert zugewiesen wird, wodurch die jeweils beste Aktion abgeleitet werden kann.
Die für den Agenten zum Zeitpunkt der Entscheidung beste Aktion entspricht dabei der mit dem höchsten Q-Wert. 
Analog zum Value-Learning Beispiel in Abschnitt \ref{sub:value_learning_visualization}, soll folgendes Beispiel das Vorgehen von Sarsa-Learning verdeutlichen.

Als Ausgangspunkt dient die in Abbildung \ref{fig:chap03_sim_szenario_1_sarsa} dargestellte Situation.
Zur Entscheidungsfindung orientiert sich die Aktionsauswahl an den Q-Werten der Zustände. 
Da diese zu Beginn noch alle 0.00 sind, werden die Aktionen zufällig gewählt.
Zusätzlich haben die Aktionen des Agenten in diesem Fall noch keine Auswirkung auf die Aktualisierung der Werte, da alle Werte noch \(0.00\) sind und es nur bei den Endzuständen Belohnungen ungleich \(0\) gibt.
Betrachten wir die erste Iteration. Der Agent möchte von der Policy wissen, welche Aktion er ausführen soll. 
Diese betrachtet die Q-Werte des aktuellen Zustands $s_0$, leitet sie an den Aktionsauswahl-Algorithmus weiter, welcher aufgrund der Gleichheit der Werte eine zufällige Aktion $a_{r_0}$ zurückliefert.
Anschließend bewegt sich der Agent in die entsprechende Richtung, wodurch er in Zustand $s_1$ landet. 
An dieser Stelle würde Value-Learning nun den Zustandswert aktualisieren.
Da bei Sarsa-Learning allerdings die im Folgezustand gewählte Aktion zur Berechnung verwendet, kann $Q(s_0, a_{r_0})$ erst in der nächsten Iteration aktualisiert werden (vgl. Gl. \ref{eq:sarsa_learning_complete_formula}).
In der nächsten Iteration wiederholt sich der Prozess, so dass der Agent durch die neue, zufällig ermittelte Aktion $a_{r_1}$ von $s_1$ nach $s_2$ gelangt. 
Erst jetzt wird $Q(s_0, a_{r_0})$ der vorherigen Iteration durch die Sarsa-Learning Formel berechnet:

\begin{equation}
	\label{eq:sarsa_learning_s0_ar0}
	Q(s_0, a_{r_0}) \leftarrow Q(s_0, a_{r_0}) + \alpha \; ([r+ \gamma\;Q(s_1, a_{r_1})] - Q(s_0, a_{r_0}))
\end{equation}

Da immer noch alle Q-Werte 0.00 sind, hat die Aktualisierung keine Auswirkung auf $Q(s_0, a_{r0})$. 
Analog zum Value-Learning Beispiel wandert der Agent so lange ohne Feedback auf dem Gitternetz umher, bis einer der präterminalen Zustände erreicht und eine Belohnung oder Strafe ausgeschüttet wird.
Angenommen der Agent hat zwischenzeitlich zwei Mal den Dot erreicht, die Belohnungen von 1.0 bei einem $\alpha$ von 0.75 und einem $\gamma$ von 0.9 erhalten und befindet sich nun erneut auf dem Weg zum Dot. 
Der linke Teil von Abbildung \ref{fig:chap03_sarsa_01} visualisiert die Situation.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{sarsa_01}
	\caption{Vorgang zur Berechnung eines Q-Wertes mit Sarsa-Learning}
	\label{fig:chap03_sarsa_01}
\end{figure}

Sei der gerade geschilderte Zustand $s_{d-2}$, das Feld oberhalb $s_{d-1}$ und der präterminale Zustand $s_d$. 
In der nächsten Iteration wird die vom Agenten gewählte Aktion $a_{d-2}$ immer noch zufällig bestimmt, da alle Q-Werte des aktuellen Zustands $s_{d-2}$ gleichwertig sind.
Im Übergang vom linken zum mittleren Teil der Abbildung \ref{fig:chap03_sarsa_01}, bringt ihn Aktion $a_{d-2}$ zum nördlich gelegenen Zustand $s_{d-1}$. 
Im Hintergrund wird nun der Q-Wert aktualisiert, welcher zur Aktion gehört, die den Agenten zu $s_{d-2}$ geführt hat. 
Dies bewirkt jedoch noch keine Wertänderung, da alle Q-Werte von $s_{d-2}$ 0.00 sind.
Der nächste Schritt ist interessanter. 
Die Aktionsauswahl basiert nun auf den Q-Werten des aktuellen Zustands $s_{d-1}$. 
Da die nach rechts führende Aktion $a_{d-1}$ den höchsten Q-Wert (0.51) besitzt und greedy der momentane Auswahl-Algorithmus ist, wird sich der Agent in jedem Fall nach rechts bewegen. 
Wie im rechten Teil von Abbildung \ref{fig:chap03_sarsa_01} dargestellt, wird beim Übergang von $s_{d-1}$ nach $s_d$ der Q-Wert der vergangenen Iteration $Q(s_{d-2}, a_{d-2})$ von 0.00 auf 0.34 mit dem Übergangstupel \((s,a,r,s',a')=(s_{d-2},a_{d-2},0,s_{d-1}, a_{d-1})\) aktualisiert:

\begin{equation}
	\label{eq:sarsa_learning_sd2_ad2}
	\begin{split}
	Q(s_{d-2}, a_{d-2}) &\leftarrow Q(s_{d-2}, a_{d-2}) + \alpha \; ([r+ \gamma\;Q(s_{d-1}, a_{d-1})] - Q(s_{d-2}, a_{d-2}))\\
	&\leftarrow 0 + 0.75 \; ([0 + 0.9\cdot0.51] - 0) \\
	&\leftarrow 0.34425
	\end{split}
\end{equation}

In der nächsten Iteration wechselt der Agent vom präterminalen Zustand in den Endzustand, wodurch $Q(s_{d-1}, a_{d-1})$ aktualisiert wird. 
Zusätzlich verändert sich beim Erreichen des Endzustands auch der Wert von $s_d$, da mit dem Ende einer Episode keine Folgezustände zum versetzten Berechnen mehr erreicht werden.
Dazu wird entsprechend die vom Endzustands ausgeschüttete Belohnung herangezogen.
So wird der Aktualisierungsversatz, welcher bei der ersten Iteration entsteht, mit dem Erreichen des Endzustands ausgeglichen.

%Im vorgestellten Beispiel wurde die Funktionsweise von Sarsa-Learning ausführlich beschrieben. 
Dadurch, dass bei dem Prozess für alle Übergänge nur der Q-Wert der jeweils gewählten Aktion aktualisiert wird, ist der in Abschnitt \ref{sub:value_learning_visualization} erwähnte Nachteil von Value-Learning behoben.
Die Q-Werte der Aktionen, die sicher in einer Bestrafung enden, bleiben somit immer negativ (siehe Abbildung \ref{fig:chap03_sarsa_02}).
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{sarsa_02}
	\caption{Q-Werte von Sarsa-Learning nach mehreren tausend Episoden}
	\label{fig:chap03_sarsa_02}
\end{figure}
Im Vergleich zu Q-Learning, besteht bei Sarsa-Learning allerdings das Problem, dass die Werte \(Q(s,a)\) nicht gegen die optimalen Werte \(Q^*(s,a)\) konvergieren, wenn nicht der optimalen Policy \(\pi^*\) gefolgt wird (vgl. \cite{suttonbarto98}, Abs. 6.5).
Welche Auswirkungen das haben kann, wird im folgenden Abschnitt zu Q-Learning betrachtet.

\section{Q-Learning}
\label{sub:q_learning_visualization}
Ähnlich wie beim Sarsa-Learning stehen auch beim Q-Learning die Q-Werte $Q(s,a)$ im Mittelpunkt der Betrachtung. 
Allerdings unterscheiden sich die beiden Verfahren an einer wichtigen Stelle in der Berechnung beziehungsweise Aktualisierung dieser Werte.
Wie in Abschnitt \ref{subsec:theory_q_learning} gezeigt, verändert Q-Learning die Q-Werte nicht zeitversetzt wie Sarsa, sondern aktualisiert sie direkt beim Übergang von $s$ zu $s'$.
Dies wird möglich, da nicht die tatsächlich durchgeführte Aktion des Folgezustands abgewartet wird. 
Stattdessen bezieht die Berechnung den höchsten Q-Wert für den Folgezustand mit ein (siehe Gl. \ref{eq:q_learning_complete_formula}). 
Da beide Formeln ansonsten identisch sind, wird an dieser Stelle auf ein ausführliches Berechnungsbeispiel verzichtet und auf die Erläuterungen in Abschnitt \ref{subsec:sarsa_learning_visualization} verwiesen. \\
Ein wichtiger Punkt ist jedoch, dass durch den kleinen Unterschied die Werte \(Q(s,a)\) bei Q-Learning immer gegen \(Q^*(s,a)\) konvergieren. 
Dies ist, anders als bei Sarsa, sogar dann garantiert, wenn nicht der optimalen Policy \(\pi^*\) gefolgt wird. 

Am anschaulichsten lässt sich der Sachverhalt an einem Beispiel betrachten. 
Dazu werden beide Lernmethoden unter identischen Bedingungen im Brücken-Labyrinth (siehe Abbildung \ref{fig:chap03_sim_szenario_2}) getestet und die Umgebung mit Hilfe von $\varepsilon$-greedy und einem $\varepsilon$ von 0.3, über 300000 Episoden hinweg erkundet.
Anschließend wird der Aktionsauswahl-Algorithmus zu greedy gewechselt, um zu überprüfen, welchen zusammenhängenden Pfad die Lernmethoden als den Besten erachten.
Die Lernrate und der Verminderungssatz bleiben während des gesamten Versuchs unverändert bei $\alpha = 0.3$ und $\gamma = 0.9$.

Wie in Abbildung \ref{fig:bridge_sarsa} zu erkennen, sind die Q-Werte bei Sarsa-Learning nicht konvergiert.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{bridge_sarsa}
	\caption{Q-Werte von Sarsa-Learning nach mehreren tausend Episoden in der Brückenumgebung}
	\label{fig:bridge_sarsa}
\end{figure}
Es lässt sich außerdem beobachten, dass der Agent bei Sarsa-Learning in einen großen Bogen um die präterminalen Geisterzustände herumläuft. 
Durch das Einbeziehen der Nachfolgeaktion erreicht der Algorithmus eine höhere Sicherheit, da er seltener Gefahr läuft durch die $\epsilon$ Komponente der Aktionsauswahl auf einen Geist zu treffen. 
Im Gegenzug nimmt er dafür einen Umweg in Kauf. 

Bei Q-Learning, sind alle Werte bereits nach weniger als 10000 Episoden vollständig konvergiert.
Abbildung \ref{fig:bridge_ql} zeigt die berechneten Q-Werte, aus welchen die optimale Policy $\pi^*$ abgeleitet werden kann.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{bridge_ql}
	\caption{Q-Werte von Q-Learning nach mehreren tausend Episoden in der Brückenumgebung}
	\label{fig:bridge_ql}
\end{figure}
Im Gegensatz zu Sarsa-Learning, schafft es Q-Learning durch Ausnutzen von $\pi^*$, auf kürzestem Weg zum Ziel zu gelangen. 
Allerdings ist anzunehmen, dass der Agent aus diesem Grund vor dem Erreichen der optimalen Policy häufiger an den Geistern scheitert als bei Sarsa-Learning. Dieser Effekt kann auch anhand der Übungsaufgaben in Anhang \ref{app:app01} nachverfolgt werden.




