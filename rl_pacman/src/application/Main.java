package application;

import model.environment.Settings;
import controller.MainMenuController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * This class serves as entry point of the application.
 * It loads the application's Main Menu, initializes the javafx specific setting (root, scene, stage)
 * and the application specific settings.
 */
public class Main extends Application {

	@Override
	public void start(Stage primaryStage) {
		try {

			// load fxml file of the main menu and init javafx setting
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MainMenuView.fxml"));
			Parent root = (Parent) loader.load();
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();

			// pass stage to the first controller (MainMenuController)
			MainMenuController ctrl = (MainMenuController) loader.getController();
			ctrl.setStage(primaryStage);

			// create application specific settings object and pass it to the controller
			Settings settings = new Settings();
			ctrl.setSettings(settings);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
