package model.reinforcement_learning.policy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import model.reinforcement_learning.Action;
import model.reinforcement_learning.State;
import model.reinforcement_learning.StateActionPair;
import model.reinforcement_learning.value_function.ValueParams;
import utils.MyMath;

/**
 * This class represents the transition model T of the MDP (Markov Decision Process) to determine which
 * state/action-pair leads to which successor state. It is used by model based policies for model based learning.
 * 
 * It keeps track over the probability to land in state s' when performing action a in state s
 * <br/><br/>
 * (s,a) -> prob(s')
 */
public class TransitionModel {

	/**
	 * The model itself. This is where the collected data is stored and this
	 * is the source for the calculations of the specific probabilities.
	 * 
	 * The map represents the following mapping:
	 *  <br/><br/>
	 * (s,a) -> (s', count)
	 *  <br/><br/>
	 * Count describes how often the successorstate s' has been reached when action a has been performed in state s.
	 * Thus the probability of landing in any state by performing a in s can be calculated.
	 */
	private Map<StateActionPair, Map<State, Integer>> stateActionToState;
	
	public TransitionModel(){
		stateActionToState = new HashMap<>();
	}
	
	public TransitionModel(TransitionModel tmToCopy){
		this.stateActionToState = new HashMap<>();

		// deep copy
		for(Entry<StateActionPair, Map<State, Integer>> outerEntry : tmToCopy.stateActionToState.entrySet()){
			HashMap<State, Integer> innerMap = new HashMap<>();
			this.stateActionToState.put(new StateActionPair(outerEntry.getKey()), innerMap);
			for(Entry<State, Integer> innerEntry : outerEntry.getValue().entrySet()){
				innerMap.put(innerEntry.getKey().getCopy(), innerEntry.getValue());
			}
		}
	}
		
	/**
	 * Returns a value between 0 and 1 representing the probability
	 * of landing in the given successor state by executing the state-action-pair.
	 * <br/><br/>
	 * (s,a,s') -> prob(s')
	 */
	public double getProbability(StateActionPair sap, State successor){
		
		Map<State, Integer> successorCountMap = stateActionToState.get(sap);
		if(null == successorCountMap) return 0.0;
		
		Integer successorCount = successorCountMap.get(successor);
		if(null == successorCount) return 0.0;
		
		//count all general executions of the given SAP, independent from successorState
		double stateActionCount = 0.0;
		Set<Entry<State,Integer>> stateActionCountList = successorCountMap.entrySet();
		for (Entry<State, Integer> entry : stateActionCountList) {
			stateActionCount += entry.getValue().doubleValue();
		}
		
		return MyMath.almostEquals(stateActionCount, 0.0) ? 0.0 : successorCount.doubleValue() / stateActionCount;
	}
	
	/**
	 * Returns a map of successorState->probability pairs, which represent the probability of landing in
	 * the respective successorState by executing the given state-action-pair
	 * <br/><br/>
	 * (s,a) -> (s', prob(s')) for all s' that have ever been reached by (s,a)
	 */
	public Map<State, Double> getSuccessorStateToProbability(StateActionPair sap){
		Map<State, Double> successorToProb = new HashMap<>();
		Map<State, Integer> successorToCount = stateActionToState.get(sap);
		if(null != successorToCount && !successorToCount.isEmpty()){
			Set<State> successors = successorToCount.keySet();
			for(State successor : successors){
				double prob = getProbability(sap, successor);
				successorToProb.put(successor, prob);
			}
		}
		return successorToProb;
	}
	
	/**
	 * Returns a map of action->(successorState, probability) pairs, which represent the probabilities of landing in 
	 * a certain successorState by performing the respective action in the given currentState.
	 * <br/><br/>
	 * for every action: (s, a) -> [a -> (s',prob(s'))]
	 */
	public Map<Action, Map<State, Double>> getProbabilities(State currentState, List<Action> possibleActions){
		Map<Action, Map<State, Double>> actionToSuccessorProb = new HashMap<>();
		for(Action a : possibleActions){
			StateActionPair sap = new StateActionPair(currentState, a);
			actionToSuccessorProb.put(a, getSuccessorStateToProbability(sap));
		}
		return actionToSuccessorProb;
	}
	
	/**
	 * Returns the state which is most probable to land in when executing the given action in the given state
	 */
	public State getMostProbableSuccessorState(State currentState, Action action){
		StateActionPair sap = new StateActionPair(currentState, action);
		Map<State, Double> successorToProb = getSuccessorStateToProbability(sap);
		State mostProbableSuccessor = null;
		double bestProbability = -1;
		
		for(Entry<State, Double> entry : successorToProb.entrySet()){
			if(entry.getValue().doubleValue() > bestProbability){
				bestProbability = entry.getValue().doubleValue();
				mostProbableSuccessor = entry.getKey();
			}
		}
		
		return mostProbableSuccessor;
	}
	
	/**
	 * Increments the counter of reaching the given successor state
	 * in combination with the given state action pair.
	 * @param possibleActions
	 * 			: the actions which are generally possible in the currentState. the chosen action must be an element of this list.
	 * @param currentState
	 * 			: the state in which the given action is executed.
	 * @param action
	 * 			: the action which is executed in currentState to transit to successorState. The action must be an element of the given availableActions!
	 * @param successorState
	 * 			: the state which succeeds when executing the given action in the currentState.
	 */
	public void update(List<Action> possibleActions, ValueParams params){
		State currentState = params.getState();
		Action action = params.getAction();
		State successorState = params.getSPrime();

		if(null == possibleActions || possibleActions.isEmpty())
			return;
		
		if(!possibleActions.contains(action)){
			throw new RuntimeException("The given action ("+action+") is not an element of the possibleActions ("+possibleActions.toString()+")!");
		}
		
		update(possibleActions, new StateActionPair(currentState, action), successorState);
	}
	
	/**
	 * Increments the counter of reaching the given successor state
	 * in combination with the given state action pair.
	 * @param possibleActions
	 * 			: the actions which are generally possible in the state of the SAP. The action of the SAP must be an element of this list.
	 * @param sap
	 * 			: the stateActionPair from which we are switched to the successorState. The action of the SAP must be an element of the possibleActions list.
	 * @param successorState
	 * 			: the state which succeeds when executing the given sap.
	 */
	private void update(List<Action> possibleActions, StateActionPair sap, State successorState){
		
		if(!stateActionToState.containsKey(sap)){
			initStateActionPairs(sap.getState(), possibleActions, successorState);
		}
		
		Map<State, Integer>	successorCountMap = stateActionToState.get(sap);
		
		Integer successorCount;
		if(!successorCountMap.containsKey(successorState)){
			successorCount = 0;
			successorCountMap.put(successorState, successorCount);
		}
		else{
			successorCount = successorCountMap.get(successorState);
		}
		successorCountMap.put(successorState, successorCount.intValue()+1);
	}
	
	/**
	 * Puts all given stateActionPair-combinations to the stateActionToState map.
	 * Also adds the successorStateCount for each created entry to prevent endless loops in action selection.
	 * @param currentState
	 * 			: key of the stateActionPair combinations
	 * @param possibleActions
	 * 			: actions for which a stateActionPair should be generated
	 * @param successorState
	 * 			: state in which the SAP execution might lead
	 */
	private void initStateActionPairs(State currentState, List<Action> possibleActions, State successorState){
		for(Action action : possibleActions){
			Map<State, Integer> curSuccessorToCount = new HashMap<>();
			stateActionToState.put(new StateActionPair(currentState, action), curSuccessorToCount);
			curSuccessorToCount.put(successorState, 0);
		}
	}
}
