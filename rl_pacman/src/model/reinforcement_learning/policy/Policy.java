/**
 * 
 */
package model.reinforcement_learning.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.reinforcement_learning.Action;
import model.reinforcement_learning.State;
import model.reinforcement_learning.value_function.Value;
import model.reinforcement_learning.value_function.ValueFunction;
import model.reinforcement_learning.value_function.ValueLearning;
import model.reinforcement_learning.value_function.ValueParams;

/**
 * Superclass for modelbased and modelfree policies.
 * Basically a policy represents the set of rules which
 * a reinforcement learning agent follows. The rules are
 * determined by the policie's action selection method in 
 * combination with the policie's value function.
 */
public abstract class Policy implements Serializable {

	private static final long serialVersionUID = -5599549036452548818L;

	/**
	 * The action selection method used by this policy.
	 */
	private ActionSelection actionSelection;

	/**
	 * the value function used and updated by this policy
	 */
	protected ValueFunction valueFunction;

	/**
	 * Instantiates a policy object with the given action selection method and
	 * valueLearning as default value function.
	 */
	public Policy(ActionSelection actionSelection) {
		this.actionSelection = actionSelection;
		this.valueFunction = new ValueLearning();
	}

	public Policy(Policy policyToCopy) {
		this.actionSelection = policyToCopy.actionSelection.getCopy();
		this.valueFunction = policyToCopy.valueFunction.getCopy();
	}

	public void setValueFunction(ValueFunction valueFunction) {
		this.valueFunction = valueFunction;
	}

	/**
	 * Updates the policies value function by the given value parameters
	 */
	public void updateValueFunction(ValueParams params) {
		valueFunction.updateValueWith(params);
	}

	public ValueFunction getValueFunction() {
		return this.valueFunction;
	}

	/**
	 * Uses the policies action selection method to select an action among the
	 * given ActionValuePairs
	 */
	protected Action selectAction(List<ActionValuePair> estimatedActionValuePairs) {
		return actionSelection.selectAction(estimatedActionValuePairs);
	}

	/**
	 * Generates a list of ActionValuePairs by using the subclasses'
	 * getEstimatedValue method. For each of the given possible actions an
	 * estimated action-value pair is calculated based on the given state. Then
	 * uses the selectAction method to determine the next action among the
	 * determined pairs.
	 */
	public Action getNextAction(State state, List<Action> possibleActions) {
		if (possibleActions == null || possibleActions.isEmpty())
			return null;

		List<ActionValuePair> estimatedActionValuePairs = new ArrayList<>();

		for (Action action : possibleActions) {
			Value estimatedValue = getEstimatedValue(state, action);
			estimatedActionValuePairs.add(new ActionValuePair(action, estimatedValue));
		}
		return selectAction(estimatedActionValuePairs);
	}

	/**
	 * Calculates an estimated value for the given action based on the given state as origin.
	 * This value describes the value that is expected to reach when performing the given
	 * action in the given state and then following the current policy until a terminal state
	 * is reached.
	 * @return
	 * 		The calculated expected value
	 */
	public abstract Value getEstimatedValue(State state, Action action);

	public abstract Policy getCopy();

	@Override
	public String toString() {
		return actionSelection.getName();
	}

	public ActionSelection getActionSelectionMethod() {
		return actionSelection;
	}

	public void setActionSelectionMethod(ActionSelection actSel) {
		actionSelection = actSel;
	};

}
