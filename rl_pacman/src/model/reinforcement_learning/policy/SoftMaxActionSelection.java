package model.reinforcement_learning.policy;

import java.util.List;

import utils.MyMath;
import model.reinforcement_learning.Action;

/**
 * Action Selection method where the probability depends on the value of a
 * state. In comparison to Eps-Greedy which treats all of the actions (apart
 * from the best action) equivalently, this action selection method weights the
 * probability of the actions depending on the expected value. Thus, better
 * actions are more likely to be chosen than bad actions, but there is still a
 * certain probability to choose bad ones.
 * 
 * Algorithm is based upon Gibbs distribution. See also:
 * http://artint.info/html/ArtInt_266.html
 */
public class SoftMaxActionSelection extends ActionSelection {

	/**
	 * The lower bound of the temp value. Values lower than 0.01 caused issues in
	 * probability calculation because of floating precision issues.
	 */
	private static final double MIN_TEMP = 0.01;

	private static final long serialVersionUID = -4527148340930854587L;

	/**
	 * the temperature specifying how randomly actions should be chosen
	 * according to the Gibbs distribution. When temp is high, the
	 * actions are chosen in almost equal amounts. As the temperature is
	 * reduced, the highest-valued actions are more likely to be chosen and, in
	 * the limit as temp->0, the best action is always chosen (greedy). temp
	 * must be > 0.
	 */
	private double temp;

	/**
	 * Initializes the action selection method with a default temp of MIN_TEMP
	 */
	public SoftMaxActionSelection() {
		this(MIN_TEMP);
	}

	/**
	 * Initializes the action selection method with the given temp value
	 */
	public SoftMaxActionSelection(double temp) {
		super(SOFTMAX);
		setTemp(temp);
	}

	public SoftMaxActionSelection(SoftMaxActionSelection smToCopy) {
		this(smToCopy.temp);
	}

	@Override
	public ActionSelection getCopy() {
		return new SoftMaxActionSelection(this);
	}

	/**
	 * Sets the given temp value, but not to a lower value than MIN_TEMP
	 * @param temp
	 */
	public void setTemp(double temp) {
		if (temp < MIN_TEMP)
			temp = MIN_TEMP;

		this.temp = temp;
	}

	public double getTemp() {
		return temp;
	}

	@Override
	public Action selectAction(List<ActionValuePair> estimatedActionValuePairs) {
		assert (null != estimatedActionValuePairs);
		assert (0 != estimatedActionValuePairs.size());

		// Calc the sum of the gibbs distribution
		double sum = 0.0;
		for (ActionValuePair pair : estimatedActionValuePairs) {
			sum += Math.exp(pair.value.getValue() / temp);
		}

		// Prevent division by zero and return random action in case
		ActionValuePair chosenPair = estimatedActionValuePairs.get(random.nextInt(estimatedActionValuePairs.size()));
		if (MyMath.almostEqualsToZero(sum))
			return chosenPair.action;

		// Dice a randomNumber and then iterate through the probabilities
		// until we have a "runningProbability" > randomNumber. Thus the actions are chosen
		// according to their own probabilities.
		double randomNumber = random.nextDouble();
		double runningProbability = 0.0;
		for (ActionValuePair pair : estimatedActionValuePairs) {
			double actionValue = pair.value.getValue();
			double probability = Math.exp(actionValue / temp) / sum;

			runningProbability += probability;

			if (runningProbability > randomNumber) {
				chosenPair = pair;
				break;
			}
		}

		return chosenPair.action;
	}

}
