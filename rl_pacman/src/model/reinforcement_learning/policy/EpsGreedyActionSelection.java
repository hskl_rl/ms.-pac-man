package model.reinforcement_learning.policy;

import java.util.List;

import model.reinforcement_learning.Action;

/**
 * This ActionSelection method chooses either the best action
 * or, depending on a given epsilon probability, one of the other actions.
 */
public class EpsGreedyActionSelection extends ActionSelection {

	private static final long serialVersionUID = -59054280598667328L;
	
	/**
	 * Upper bound for the epsilon value. Equals 100 percent. 
	 */
	private static final double MAX_EPSILON = 1.0;
	
	/**
	 * Lower bound for the epsilon value. Equals 0 percent. 
	 */
	private static final double MIN_EPSILON = 0.0;
	
	/**
	 * A value between 0.0 and 1.0 which describes the probability
	 * of choosing a different action than the best.
	 */
	private double epsilon;
	
	/**
	 * Initializes the action selection method with the given epsilon value
	 */
	public EpsGreedyActionSelection(double epsilon) {
		super(EPSGREEDY);
		setEpsilon(epsilon);
	}

	/**
	 * Initializes the action selection method with a default epsilon of 0.3
	 */
	public EpsGreedyActionSelection() {
		this(0.3);
	}
	
	public EpsGreedyActionSelection(EpsGreedyActionSelection epsToCopy) {
		this(epsToCopy.epsilon);
	}
	
	@Override
	public ActionSelection getCopy() {
		return new EpsGreedyActionSelection(this);
	}

	/**
	 * Sets the epsilon value of the action selection method.
	 * Keeps the value within the bounds, meaning that the min
	 * or the max value will be applied if the given value exceeds
	 * the bounds.
	 */
	public void setEpsilon(double epsilon) {
		if(epsilon > MAX_EPSILON)
			epsilon = MAX_EPSILON;
		else if(epsilon < MIN_EPSILON)
			epsilon = MIN_EPSILON;
		
		this.epsilon = epsilon;
	}

	public double getEpsilon() {
		return epsilon;
	}

	@Override
	public Action selectAction(List<ActionValuePair> estimatedActionValuePairs) {
		assert(null != estimatedActionValuePairs);
		assert(0 != estimatedActionValuePairs.size());
		
		if (1 == estimatedActionValuePairs.size())
			return estimatedActionValuePairs.get(0).action;

		// choose the best action
		ActionValuePair bestPair = estimatedActionValuePairs.get(random.nextInt(estimatedActionValuePairs.size()));
		for (int i=0; i < estimatedActionValuePairs.size() ;++i) {
			if (bestPair.value.getValue() < estimatedActionValuePairs.get(i).value.getValue()) {
				bestPair = estimatedActionValuePairs.get(i);
			}
		}
		
		// choose a different action with epsilon probability (equally among the
		// remaining actions)
		ActionValuePair chosenPair = bestPair;
		double dice = random.nextDouble();
		if (dice <= epsilon) {
			while (bestPair == chosenPair) {
				chosenPair = estimatedActionValuePairs.get(random.nextInt(estimatedActionValuePairs.size()));
			}
		}

		return chosenPair.action;
	}

}
