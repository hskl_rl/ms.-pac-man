package model.reinforcement_learning.value_function;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import model.reinforcement_learning.Action;
import model.reinforcement_learning.Reward;
import model.reinforcement_learning.State;

public class QLearning extends ValueFunction {

	private static final long serialVersionUID = 8302683292617338434L;

	/** Maps from State to <Action, Value> */
	protected final Map<Long, Map<Long, Value>> stateValues;

	private final List<Action> ACTIONS_FOR_INIT_STATE_MAP;

	protected double learningRate;
	protected double discountRate;

	/**
	 * @param actionsForInitStateMap
	 *            is used to initialize the map of a state if its map is null.
	 */
	public QLearning(List<Action> actionsForInitStateMap) {
		this(null, actionsForInitStateMap, 0.1, 0.9);
	}

	/**
	 * Note: do not access this function directly! Access to ValueFunction should only
	 * be handled by AgentFactory.
	 **/
	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
	}

	/**
	 * Note: do not access this function directly! Access to ValueFunction should only
	 * be handled by AgentFactory.
	 **/
	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}

	/**
	 * 
	 * @param actionsForInitStateMap
	 *            is used to initialize the map of a state.
	 * @param learningRate
	 *            represents alpha of the update formula
	 * @param discountRate
	 *            represents gamma of the update formula
	 */
	public QLearning(List<Action> actionsForInitStateMap, double learningRate, double discountRate) {
		this(null, actionsForInitStateMap, learningRate, discountRate);
	}

	/**
	 * 
	 * @param initialStateValues
	 *            can be used to initialize special states. For example if there
	 *            are states which have other actions than
	 *            actionsForInitStateMap then this map could provide already
	 *            maps for these states.
	 * @param actionsForInitStateMap
	 *            is used to initialize the map of a state.
	 * @param learningRate
	 *            represents alpha of the update formula
	 * @param discountRate
	 *            represents gamma of the update formula
	 */
	public QLearning(Map<Long, Map<Long, Value>> initialStateValues, List<Action> actionsForInitStateMap, double learningRate,
			double discountRate) {
		super(Q_LEARNING);

		if (null != initialStateValues)
			this.stateValues = initialStateValues;
		else
			this.stateValues = new HashMap<>();
		this.ACTIONS_FOR_INIT_STATE_MAP = actionsForInitStateMap;
		this.learningRate = learningRate;
		this.discountRate = discountRate;
	}

	public QLearning(QLearning qviToCopy) {
		super(Q_LEARNING);

		// deep copy of stateValues
		stateValues = new HashMap<Long, Map<Long, Value>>();
		for (Entry<Long, Map<Long, Value>> outerEntry : qviToCopy.stateValues.entrySet()) {
			Map<Long, Value> innerMap = new HashMap<Long, Value>();
			stateValues.put(outerEntry.getKey(), innerMap);
			for (Entry<Long, Value> innerEntry : outerEntry.getValue().entrySet()) {
				innerMap.put(innerEntry.getKey(), new Value(innerEntry.getValue()));
			}
		}

		// deep copy of actionlist
		ACTIONS_FOR_INIT_STATE_MAP = new ArrayList<Action>();
		for (Action a : qviToCopy.ACTIONS_FOR_INIT_STATE_MAP) {
			ACTIONS_FOR_INIT_STATE_MAP.add(a.getCopy());
		}

		learningRate = qviToCopy.learningRate;
		discountRate = qviToCopy.discountRate;
	}

	@Override
	public ValueFunction getCopy() {
		return new QLearning(this);
	}

	/**
	 * @return the learningRate
	 */
	public double getLearningRate() {
		return learningRate;
	}

	/**
	 * @return the discountRate
	 */
	public double getDiscountRate() {
		return discountRate;
	}

	/**
	 * Returns the qvalue for Q(s,a). <br/>
	 * <br/>
	 * Considered parameters: <br/>
	 * - s State in time t <br/>
	 * - a Action. If null == a then the highest value <br/>
	 * for state s will be returned: max_a Q(s,a)
	 */
	@Override
	public Value getValue(ValueParams params) {
		State state = params.getState();
		Action action = params.getAction();

		assert (null != state);

		Map<Long, Value> mapActionToValue = stateValues.get(state.getSimpleId());
		if (null == mapActionToValue)
			return new Value();

		if (null == action) // Then return the max value
			return pickMaxOfAllValues(mapActionToValue);

		Value valOfState = mapActionToValue.get(action.getSimpleId());

		if (null == valOfState)
			return new Value();
		else
			return valOfState;
	}

	/**
	 * Picks the max value of the given map (!=null). If there are no values
	 * then the value 0.0 will be returned.
	 * 
	 * @param mapActionToValue
	 * @return
	 */
	private Value pickMaxOfAllValues(Map<Long, Value> mapActionToValue) {
		Collection<Value> allValues = mapActionToValue.values();
		double maxValue = 0 == allValues.size() ? 0.0 : Double.NEGATIVE_INFINITY;
		for (Value val : allValues) {
			if (maxValue < val.getValue())
				maxValue = val.getValue();
		}
		return new Value(maxValue);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public void updateValueWith(ValueParams params) {
		State state = params.getState();
		State sPrime = params.getSPrime();
		Reward reward = params.getReward();

		assert (null != state && null != sPrime && null != reward);

		Value oldValue = getValue(params);
		Value maxValOfSPrime = getMaxValueOf(sPrime);
		double sample = reward.getValue() + discountRate * maxValOfSPrime.getValue();
		double newVal = oldValue.getValue() + learningRate * (sample - oldValue.getValue());

		insertNewValue(params, newVal);
	}

	protected void insertNewValue(ValueParams params, double newVal) {
		State state = params.getState();
		Action action = params.getAction();

		assert (null != state && null != action);

		Map<Long, Value> mapActionToValues = stateValues.get(state.getSimpleId());

		if (null == mapActionToValues) {
			mapActionToValues = initStateMap();
			stateValues.put(state.getSimpleId(), mapActionToValues);
		}

		mapActionToValues.put(action.getSimpleId(), new Value(newVal));
	}

	private Map<Long, Value> initStateMap() {
		Map<Long, Value> initialMapForAState = new HashMap<>();

		for (Action a : ACTIONS_FOR_INIT_STATE_MAP) {
			initialMapForAState.put(a.getSimpleId(), getInitValue());
		}

		return initialMapForAState;
	}

	private Value getInitValue() {
		// Currently returns always 0.0. Could be used to generate random
		// init values
		return new Value(0.0);
	}

	@Override
	public Value getMaxValueOf(State state) {
		// If we feed getValue with null action then it will give us max of all
		// possible actions
		return getValue(new ValueParams(state));
	}

}
