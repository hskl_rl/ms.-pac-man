/**
 * 
 */
package model.reinforcement_learning.value_function;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import model.reinforcement_learning.State;

/**
 * Base class for all value functions.
 */
public abstract class ValueFunction implements Serializable {

	private static final long serialVersionUID = 3107993475346286388L;

	// Valuefunctions for exact statespaces (here we are able to save all states)
	public final static String VALUE_LEARNING = "Value Learning";
	public final static String Q_LEARNING = "Q-Value Learning";
	public final static String SARSA = "Sarsa Learning";
	private final static String[] VALUE_FUNCTIONS = { VALUE_LEARNING, Q_LEARNING, SARSA };
	public static final List<String> valueFunctions = Collections.unmodifiableList(new ArrayList<String>(Arrays.asList(VALUE_FUNCTIONS)));
	
	// Valuefunctions for approximate statespaces (the state space is too big, thus we need to approximate it)
	public final static String APPROX_QLEARNING = "Approx. Q-Learning";
	private final static String[] APPROXIMATE_VALUE_FUNCTIONS = { APPROX_QLEARNING };
	public static final List<String> approximateValueFunctions = Collections.unmodifiableList(new ArrayList<String>(Arrays.asList(APPROXIMATE_VALUE_FUNCTIONS)));


	protected String name;

	public ValueFunction(String name){
		this.name = name;
	}
	
	/**
	 * Returns the value of state or state/action pair. In case the value
	 * function holds state/action pairs then a call with action == null will
	 * deliver max_a Q(s,a)
	 */
	public abstract Value getValue(ValueParams params);

	/**
	 * This function shall return always the max value of a state. Especially
	 * for functions in which qvalues are used it clarifies the code.
	 */
	public abstract Value getMaxValueOf(State state);

	public abstract void updateValueWith(ValueParams params);

	public abstract ValueFunction getCopy();
	
	/**
	 * @return the NAME
	 */
	public String getName() {
		return name;
	}

}
