/**
 * 
 */
package model.reinforcement_learning.value_function;

/**
 *	Represents a value for the reionforcement_learning package.
 *  Basically it is a double.
 */
public class Value {

	private double value;

	/** Generates a Value object with value 0.0. */
	public Value() {
		value = 0.0;
	}

	public Value(double v) {
		value = v;
	}
	
	public Value(Value v){
		value = v.value;
	}

	public double getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "Value: " + value;
	}
}
