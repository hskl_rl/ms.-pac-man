package model.reinforcement_learning.value_function;

import model.reinforcement_learning.Action;
import model.reinforcement_learning.Reward;
import model.reinforcement_learning.State;

/**
 * This class solves the need of different parameters in different value
 * functions.
 * 
 * For example to get the value of a state in ValueLearning it needs only a
 * given state. QLearning needs a state and an action. During the update
 * of SARSA we need five different parameter. IN QLearning we only need 4.
 */
public class ValueParams {

	/** State in time t. */
	State state;
	/** Action chosen in state s and time t. */
	Action action;
	/** Received reward for action a in state s. */
	Reward reward;
	/** sPrime Successor state of state a and action a. */
	State sPrime;

	/*
	 * Various c'tors for different purposes. We don't chain (calling another
	 * c'tor with this-function) them because of performance reasons.
	 */

	/* For getting a value from ValueLearning */
	public ValueParams(State state) {
		this.state = state;
	}

	/* For getting a value from QLearning */
	public ValueParams(State state, Action action) {
		this.state = state;
		this.action = action;
	}

	/* For updating a state value in ValueLearning */
	public ValueParams(State state, Action action, Reward reward) {
		this.state = state;
		this.action = action;
		this.reward = reward;
	}

	/* For updating a state/action value pair in QLearning */
	public ValueParams(State state, Action action, Reward reward, State s_tPlus1) {
		this.state = state;
		this.action = action;
		this.reward = reward;
		this.sPrime = s_tPlus1;
	}

	public ValueParams(Action action) {
		this.action = action;
	}

	public State getState() {
		return state;
	}

	public Action getAction() {
		return action;
	}

	public Reward getReward() {
		return reward;
	}

	public State getSPrime() {
		return sPrime;
	}

}
