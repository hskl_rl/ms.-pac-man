package model.reinforcement_learning.value_function;

import java.util.HashMap;
import java.util.Map;

import model.reinforcement_learning.Reward;
import model.reinforcement_learning.State;

/** Implementation of Value Learning.
 *  
 *
 */
public class ValueLearning extends ValueFunction {

	private static final long serialVersionUID = 8302683292617338434L;

	private Map<Long, Value> stateValues = new HashMap<>();

	// Alpha
	private double learningRate = 0.1;
	// Gamma
	private double discountRate = 0.9;

	public ValueLearning() {
		super(VALUE_LEARNING);
	}

	public ValueLearning(double learningRate, double discountRate) {
		super(VALUE_LEARNING);
		this.learningRate = learningRate;
		this.discountRate = discountRate;
	}
	
	public ValueLearning(ValueLearning viToCopy){
		this(viToCopy.learningRate, viToCopy.discountRate);
		// Deep copy
		for(Long key : viToCopy.stateValues.keySet()) {
			stateValues.put(key, viToCopy.stateValues.get(key));
		}
	}
	
	@Override
	public ValueFunction getCopy() {
		return new ValueLearning(this);
	}

	@Override
	public Value getValue(ValueParams params) {
		State state = params.getState();
		assert (null != state);
		Value valOfState = stateValues.get(state.getSimpleId());

		if (null == valOfState)
			return new Value();
		else
			return valOfState;
	}

	@Override
	public String toString() {
		return name;
	}
	
	/**
	 * Updates the value function based on the transition (s,r,s').
	 * 
	 * V_{k+1}(s) <-- V_k(s) + \alpha {[r + \gamma V_k(s')] - V_k(s)}
	 */
	@Override
	public void updateValueWith(ValueParams params) {
		State state = params.getState();
		State stateTPlusOne = params.getSPrime();
		Reward reward = params.getReward();

		Value oldValue = getValue(params);
		Value valOfState_tPlusOne = getValue(new ValueParams(stateTPlusOne));
		double sample = reward.getValue() + discountRate * valOfState_tPlusOne.getValue();
		double newVal = oldValue.getValue() + learningRate * (sample - oldValue.getValue());

		stateValues.put(state.getSimpleId(), new Value(newVal));
	}

	/**
	 * @return the learningRate
	 */
	public double getLearningRate() {
		return learningRate;
	}

	/**
	 * @return the discountRate
	 */
	public double getDiscountRate() {
		return discountRate;
	}

	@Override
	public Value getMaxValueOf(State state) {
		// Same es getValue since a state has only one value
		return getValue(new ValueParams(state));
	}
	
	/**
	 * Note: do not access this function directly! Access to ValueFunction should only
	 * be handled by AgentFactory.
	 **/
	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
	}
	
	/**
	 * Note: do not access this function directly! Access to ValueFunction should only
	 * be handled by AgentFactory.
	 **/
	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}

	
}
