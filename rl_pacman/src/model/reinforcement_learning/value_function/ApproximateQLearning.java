package model.reinforcement_learning.value_function;

import model.reinforcement_learning.Action;
import model.reinforcement_learning.FeaturesState;
import model.reinforcement_learning.Reward;
import model.reinforcement_learning.State;

/**
 * This class is an implementation of QLearning with FeatureStates. See
 * https://youtu.be/yNeSFbE1jdY?t=37m45s
 */
public class ApproximateQLearning extends ValueFunction {

	private static final long serialVersionUID = -8674393291304420186L;

	// Alpha
	private double learningRate = 0.1;
	// Gamma
	private double discountRate = 0.9;

	/**
	 * Instead of holding all states in a map or table we need to save the
	 * weights
	 */
	private double[] featureWeights;

	public ApproximateQLearning(int numberOfFeatures, double learningRate, double discountRate) {
		super(APPROX_QLEARNING);

		featureWeights = new double[numberOfFeatures];
		this.learningRate = learningRate;
		this.discountRate = discountRate;

		// Init weight
		for (int i = 0; i < featureWeights.length; ++i) {
			featureWeights[i] = 1.0;
		}
	}
	
	public ApproximateQLearning(ApproximateQLearning aqlToCopy) {
		super(APPROX_QLEARNING);
		
		// deep copy featureWeights
		featureWeights = new double[aqlToCopy.featureWeights.length];
		for(int i=0; i<aqlToCopy.featureWeights.length; i++){
			featureWeights[i] = aqlToCopy.featureWeights[i];
		}
		
		learningRate = aqlToCopy.learningRate;
		discountRate = aqlToCopy.discountRate;
	}
	
	@Override
	public ValueFunction getCopy() {
		return new ApproximateQLearning(this);
	}

	/**
	 * Returns the qvalue for Q(s,a). <br/>
	 * <br/>
	 * Considered parameters: <br/>
	 * - s State in time t <br/>
	 * - a Action. If null == a then the highest value <br/>
	 * for state s will be returned: max_a Q(s,a)
	 */
	@Override
	public Value getValue(ValueParams params) {

		State state = params.getState();
		Action action = params.getAction();

		assert (null != state && state instanceof FeaturesState);

		if (null == action) // Then return the max value
			return new Value(calcMaxValueOf((FeaturesState) state));
		else
			return new Value(calcValueOf((FeaturesState) state, action));
	}

	private double calcMaxValueOf(FeaturesState state) {

		double maxValue = 0.0;

		assert (0 != state.getPossibleActions().size());

		for (Action action : state.getPossibleActions()) {
			double currentValue = calcValueOf(state, action);
			if (maxValue < currentValue)
				maxValue = currentValue;
		}
		return maxValue;
	}

	private double calcValueOf(FeaturesState state, Action action) {

		double[] features = state.getFeatures(new ValueParams(action));
		double currValue = 0.0;

		assert (features.length == featureWeights.length);

		// Q(s,a) = w_1*f_1(s,a) + w_2*f_2(s,a) + ... + w_n*f_n(s,a)
		for (int i = 0; i < features.length; ++i) {
			currValue += featureWeights[i] * features[i];
		}

		return currValue;
	}

	/**
	 * For updating we need a transition with (s,a,r,s'). <br/>
	 * <br/>
	 * s FeaturesState in time t <br/>
	 * a chosen action in time t <br/>
	 * r reward for action a in state s and time t <br/>
	 * s' resulting FeaturesState for (s,a) <br/>
	 * <br/>
	 * These parameters have to be provided in params. <br/>
	 */
	@Override
	public void updateValueWith(ValueParams params) {
		State s = params.getState();
		Action a = params.getAction();
		Reward r = params.getReward();
		State sPrime = params.getSPrime();

		assert (null != s && null != a && null != r && null != sPrime);
		assert (s instanceof FeaturesState && sPrime instanceof FeaturesState);

		Value oldValue = getValue(params);
		Value maxQValueOfSPrime = getValue(new ValueParams(sPrime));

		// difference = [r + \gamma max_{a'} Q(s',a')] - Q(s,a)
		double difference = (r.getValue() + discountRate * maxQValueOfSPrime.getValue()) - oldValue.getValue();

		// Update weights

		double[] features = ((FeaturesState) s).getFeatures(params);

		assert (features.length == featureWeights.length);

		for (int i = 0; i < features.length; ++i) {
			// w_i <-- w_i + \alpha * difference * f_i(s,a)
			featureWeights[i] = featureWeights[i] + learningRate * difference * features[i];
		}
	}

	@Override
	public Value getMaxValueOf(State state) {

		assert (state instanceof FeaturesState);

		return new Value(calcMaxValueOf((FeaturesState) state));
	}

	@Override
	public String toString() {
		return name;
	}

	public double getLearningRate() {
		return learningRate;
	}

	public void setLearningRate(double learningRate) {
		this.learningRate = learningRate;
	}

	public double getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}

}
