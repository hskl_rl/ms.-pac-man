/**
 * 
 */
package model.reinforcement_learning;

import java.io.Serializable;

/**
 * Represents a reward for the reinforcment_learning package.
 * Basically it is a double. However, a own data type is used for
 * semantic reasons. A double is only a number. A reward is a
 * number which can only be applied to special cases.
 */
public class Reward implements Serializable {
	
	private static final long serialVersionUID = 6007796672721437510L;
	private double value;
	
	/** Generates a reward of 0.0 */
	public Reward() {
		this(0.0);
	}
	
	public Reward(Reward rwdToCopy){
		this(rwdToCopy.value);
	}
	
	public Reward(double value) {
		this.value = value;
	}
	
	public double getValue() {
		return value;
	}
}
