/**
 * 
 */
package model.reinforcement_learning;

/**
 * An abstract Action. Thus the RL package can be adapted to any
 * scenario.
 */
public interface Action {
	
	/**
	 * The abstract identifier of this action. Should be unique for
	 * every action.
	 */
	public long getSimpleId();
	
	public Action getCopy();
}
