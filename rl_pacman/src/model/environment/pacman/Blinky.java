package model.environment.pacman;

import java.awt.Point;

/**
 * Blinky is the red ghost. He starts outside of the jail and is not initially
 * waiting.
 */

public class Blinky extends Ghost {

	/**
	 * Creates a blinky object.
	 * 
	 * @param env
	 *            the PacEnvironment of the current task
	 * */
	public Blinky(PacEnvironment env) {
		super(env);
		remainingWaiting = 0;		
	}

	/**
	 * Provides the start location of Blinky. Blinky start location is outside
	 * the jail.
	 * 
	 * @return the start location of the ghost as point object
	 * */
	@Override
	protected Point getStartLocation() {
		return getFirstWalkablePosition();
	}

	/**
	 * Provides the target of Blinky. Blinkys target is the pac itself.
	 * 
	 * @return the target of the ghost as point object
	 * */
	@Override
	public Point getTarget() {
		return environment.getPac().getPosition();
	}

}
