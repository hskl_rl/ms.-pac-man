package model.environment.pacman;

import java.awt.Point;

/**
 * The model class of the dots, which are collectable by the pac.
 */
public class Dot implements IPosition{
	
	private static final int DOT_VALUE = 10;
	
	protected Point position;

	
	public Dot(Point position){
		this.position = position;
	}
	
	public Point getPosition(){
		return this.position;
	}
	
	public int getScoreValue(){
		return DOT_VALUE;
	}

	@Override
	public boolean equals(Object obj) {
		if (null == obj)
			return false;
		if (obj instanceof Dot) {
			Dot dot = (Dot) obj;
			return position.equals(dot.position);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return position.hashCode();
	}

}
