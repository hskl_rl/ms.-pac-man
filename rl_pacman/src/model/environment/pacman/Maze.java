/**
 * 
 */
package model.environment.pacman;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Random;

/**
 * The model class of the PacMan mazes. Mazes are instantiated by the MazeLoader class.
 * It consists of a two dimensional array (grid) of mazePoints. Each entry represents
 * a wall, walkable field, spawn position, etc. according to its block type.
 */
public class Maze {

	/**
	 * A Random number generator which is used to get random positions within the maze.
	 **/
	private static Random randGenerator = new Random();

	/**
	 * A list of all existing blocktypes. Used to determine a blocktype by its value.
	 */
	private static final List<BlockType> ALL_BLOCKTYPES = Collections.unmodifiableList(new ArrayList<BlockType>(EnumSet.allOf(BlockType.class)));

	/**
	 * The BlockType enum is used to specify the type of the single entries within the maze grid.
	 * Each element of the maze grid represents a "block" with certain properties.
	 * 
	 * WALL = a position which is not walkable by any movable object
	 * OPEN = a position which is walkable by any movable object
	 * PACSPAWN = the location where the pac spawns
	 * GHOSTAREA = the ghost jail and spawn area
	 * GHOSTEXIT = the escape path from jail to maze corridor
	 * TELEPORTPOS = a position which is like an open position, but can be reached from other fields than its neighbors
	 * POSITIVE_REWARD_FIELD = for simulation. a field which rewards the agent for entering.
	 * NEGATIVE_REWARD_FIELD = for simulation. a field which punishes the agent for entering.
	 */
	public enum BlockType {
		WALL(0), OPEN(1), PACSPAWN(2), GHOSTAREA(3), GHOSTEXIT(4), TELEPORTPOS(5), POSITIVE_REWARD_FIELD(6), NEGATIVE_REWARD_FIELD(7);

		/**
		 * Each BlockType has an assigned value. This value is used to
		 * determine the correct BlockType when interpreting a maze file
		 * from the assets.maze package.
		 */
		public final int value;

		private BlockType(final int value) {
			this.value = value;
		}

		public static BlockType getTypeByValue(int value) {
			for (BlockType t : ALL_BLOCKTYPES) {
				if (t.value == value)
					return t;
			}
			throw new RuntimeException("No BlockType with value=" + value + " found!");
		}
	}
	
	/**
	 * A MazePoint is a combination of blocktype and grid-coordinate (point).
	 * It is mainly used to store instantiated point objects. 
	 * Thus unnecessary instantiation of points with same coordinates can be prevented.
	 */
	private static class MazePoint{
		private Point point;
		private BlockType blockType;
		
		public MazePoint(int x, int y, BlockType bt){
			this.point = new Point(x,y);
			this.blockType = bt;
		}
		
		public Point getPoint()  { return point; }
		public BlockType getBlockType() { return blockType; }
	}

	/**
	 * The width of the maze grid (x dimension).
	 * In other words: the length of of the first grid dimension.
	 */
	private int width;
	
	/**
	 * The height of the maze grid (y dimension).
	 * In other words: the length of of the second grid dimension.
	 */
	private int height;
	
	/**
	 * This is the mazes core.
	 * It describes its structure, the walls, the spawn positions,
	 * the neighbour relationships etc.
	 */
	private MazePoint[/* x */][/* y */] grid;
	
	/**
	 * A list of points which describe the ghostArea within the maze grid.
	 * The ghostArea is used as spawn location and jail of the ghosts.
	 */
	private List<Point> ghostArea;
	
	/**
	 * Describes the way out from jail to maze corridors.
	 * Must be a chained way (must lead from a neighbor of the ghost area
	 * to a neighbor of the desired corridor (walkable area).
	 */
	private List<Point> ghostExitArea;
	
	/**
	 * A point describing the spawn position of the pacman within the maze.
	 */
	private Point pacSpawnPos;
	
	/** Represents fields which will give positive reward (used in simulation). */
	private List<Point> positivRewardFields;
	
	/** Represents fields which will give negative reward (used in simulation). */
	private List<Point> negativeRewardFields;

	/**
	 * The only constructor of the Maze class.
	 * Instantiates the lists and grid of the maze
	 * by interpreting the given integer grid.
	 * @param grid the structure of the maze which should be read from a maze file. 
	 * the values of the grid's elements describe the block types of the fields.
	 */
	public Maze(int[][] grid) {
		this.ghostArea = new ArrayList<Point>();
		this.ghostExitArea = new ArrayList<Point>();
		this.positivRewardFields = new ArrayList<>();
		this.negativeRewardFields = new ArrayList<>();
		setGrid(grid);
	}
	
	/**
	 * Interprets the given integer grid and instantiates the maze's structure (grid).
	 * Also populates the list attributes of the class.
	 * @param grid the structure of the maze which should be read from a maze file. 
	 * the values of the grid's elements describe the block types of the fields.
	 */
	private void setGrid(int[][] grid) {
		// check the grid
		if (grid == null)
			throw new RuntimeException("Maze initialized with null grid");

		int w = grid.length;
		int h = grid.length > 0 ? grid[0].length : 0;
		for (int i = 0; i < grid.length; i++)
			if (grid[i].length != h)
				throw new RuntimeException("Maze initialized with a non-rectangular grid");

		// set constraints
		this.width = w;
		this.height = h;
		
		// instantiate the maze's grid and set the coordinates and blockTypes of each field.
		this.grid = new MazePoint[w][h];
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				BlockType bt = BlockType.getTypeByValue(grid[i][j]);
				this.grid[i][j] = new MazePoint(i,j,bt);
				Point pos = this.grid[i][j].getPoint();
				
				if (bt == BlockType.PACSPAWN)
					this.pacSpawnPos = pos;

				if (bt == BlockType.GHOSTAREA)
					this.ghostArea.add(pos);

				if (bt == BlockType.GHOSTEXIT)
					this.ghostExitArea.add(pos);

				if (bt == BlockType.POSITIVE_REWARD_FIELD)
					this.positivRewardFields.add(pos);

				if (bt == BlockType.NEGATIVE_REWARD_FIELD)
					this.negativeRewardFields.add(pos);
			}
		}
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public List<Point> getGhostArea() {
		return this.ghostArea;
	}

	public Point getPacSpawnPos() {
		return this.pacSpawnPos;
	}

	/**
	 * Sets pac man's spawn position to the given coordinates.
	 * If the coordinates do not represent a walkable position
	 * within the maze, an exception is thrown.
	 */
	public void setPacSpawnPos(int x, int y) {
		if (!isWalkable(x,y))
			throw new RuntimeException("Pac spawnposition set to a nonwalkable position");
		
		this.pacSpawnPos = getPointAt(x,y);
	}

	public List<Point> getGhostExitArea() {
		return this.ghostExitArea;
	}

	public List<Point> getPositiveRewardFields() {
		return this.positivRewardFields;
	}

	public List<Point> getNegativeRewardFields() {
		return this.negativeRewardFields;
	}

	public BlockType getBlockTypeAt(int x, int y) {
		return this.grid[x][y].getBlockType();
	}
	
	public Point getPointAt(int x, int y) {
		// out of bounds check
		x = x < 0 ? 0 : x >= width ? width - 1 : x;
		y = y < 0 ? 0 : y >= height ? height - 1 : y;
		
		return this.grid[x][y].getPoint();
	}

	/**
	 * Yields whether the given position is accessible by the pac.
	 */
	public boolean isWalkable(Point p) {
		return isWalkable(p.x, p.y);
	}

	/**
	 * Yields whether the given position is accessible by the pac.
	 */
	public boolean isWalkable(int x, int y) {
		BlockType bt = getBlockTypeAt(x, y);
		return isWalkable(bt);
	}

	/**
	 * Yields whether the given blocktype is accessible by the pac.
	 * Blocktypes that are accessible: OPEN, PACSPAWN, TELEPORTPOS, POSITIVE_REWARD_FIELD and NEGATIVE_REWARD_FIELD
	 */
	public boolean isWalkable(BlockType bt) {
		return bt == BlockType.OPEN || bt == BlockType.PACSPAWN || bt == BlockType.TELEPORTPOS ||
				bt == BlockType.POSITIVE_REWARD_FIELD || bt == BlockType.NEGATIVE_REWARD_FIELD;
	}

	/**
	 * Yields whether the given position belongs to the ghost exit area.
	 */
	public boolean isGhostExitingArea(int x, int y) {
		BlockType bt = getBlockTypeAt(x, y);
		return isGhostExitingArea(bt);
	}

	/**
	 * Yields whether the given position belongs to the ghost exit area.
	 */
	public boolean isGhostExitingArea(Point p) {
		BlockType bt = getBlockTypeAt(p.x, p.y);
		return isGhostExitingArea(bt);
	}

	/**
	 * Yields whether the given blocktype is of type GHOSTEXIT.
	 */
	public boolean isGhostExitingArea(BlockType bt) {
		return bt == BlockType.GHOSTEXIT;
	}

	/**
	 * Yields whether the given position belongs to the ghost area (jail).
	 */
	public boolean isGhostArea(Point p) {
		return isGhostArea(p.x, p.y);
	}

	/**
	 * Yields whether the given position belongs to the ghost area (jail).
	 */
	public boolean isGhostArea(int x, int y) {
		BlockType bt = getBlockTypeAt(x, y);
		return isGhostArea(bt);
	}

	/**
	 * Yields whether the given blocktype is of type GHOSTAREA (jail).
	 */
	public boolean isGhostArea(BlockType bt) {
		return bt == BlockType.GHOSTAREA;
	}

	/**
	 * Yields whether the given position equals the pac spawn position of the maze.
	 */
	public boolean isPacSpawn(Point p) {
		return isPacSpawn(p.x, p.y);
	}

	/**
	 * Yields whether the given position equals the pac spawn position of the maze.
	 */
	public boolean isPacSpawn(int x, int y) {
		BlockType bt = getBlockTypeAt(x, y);
		return isPacSpawn(bt);
	}

	/**
	 * Yields whether the given blocktype is of type PACSPAWN.
	 */
	public boolean isPacSpawn(BlockType bt) {
		return bt == BlockType.PACSPAWN;
	}

	/**
	 * Yields whether the given position belongs to the teleport positions of the maze.
	 */
	public boolean isTeleportPos(Point p) {
		return isTeleportPos(p.x, p.y);
	}

	/**
	 * Yields whether the given position belongs to the teleport positions of the maze.
	 */
	public boolean isTeleportPos(int x, int y) {
		BlockType bt = getBlockTypeAt(x, y);
		return isTeleportPos(bt);
	}

	/**
	 * Yields whether the given blocktype is of type TELEPORTPOS
	 */
	public boolean isTeleportPos(BlockType bt) {
		return bt == BlockType.TELEPORTPOS;
	}

	/**
	 * Yields the point which is below the given point (lower neighbor) within the maze.
	 * If the given point belongs to the southern border of the grid, the most upper point
	 * with same x value is returned (wrap).
	 */
	public Point getLowerNeighbor(Point p) {
		return getLowerNeighbor(p.x, p.y);
	}

	/**
	 * Yields the point which is below the given point (lower neighbor) within the maze.
	 * If the given point belongs to the southern border of the grid, the most upper point
	 * with same x value is returned (wrap).
	 */
	public Point getLowerNeighbor(int x, int y) {
		y = y + 1 >= height ? 0 : y + 1;
		return getPointAt(x,y);
	}

	/**
	 * Yields the point which is above the given point (upper neighbor) within the maze.
	 * If the given point belongs to the northern border of the grid, the most lower point
	 * with same x value is returned (wrap).
	 */
	public Point getUpperNeighbor(Point p) {
		return getUpperNeighbor(p.x, p.y);
	}

	/**
	 * Yields the point which is above the given point (upper neighbor) within the maze.
	 * If the given point belongs to the northern border of the grid, the most lower point
	 * with same x value is returned (wrap).
	 */
	public Point getUpperNeighbor(int x, int y) {
		y = y - 1 < 0 ? height - 1 : y - 1;
		return getPointAt(x,y);
	}

	/**
	 * Yields the point which is left to the given point (left neighbor) within the maze.
	 * If the given point belongs to the left border of the grid, the outer right point
	 * with same y value is returned (wrap).
	 */
	public Point getLeftNeighbor(Point p) {
		return getLeftNeighbor(p.x, p.y);
	}

	/**
	 * Yields the point which is left to the given point (left neighbor) within the maze.
	 * If the given point belongs to the left border of the grid, the outer right point
	 * with same y value is returned (wrap).
	 */
	public Point getLeftNeighbor(int x, int y) {
		x = x - 1 < 0 ? width - 1 : x - 1;
		return getPointAt(x,y);
	}

	/**
	 * Yields the point which is right to the given point (right neighbor) within the maze.
	 * If the given point belongs to the right border of the grid, the outer left point
	 * with same y value is returned (wrap).
	 */
	public Point getRightNeighbor(Point p) {
		return getRightNeighbor(p.x, p.y);
	}

	/**
	 * Yields the point which is right to the given point (right neighbor) within the maze.
	 * If the given point belongs to the right border of the grid, the outer left point
	 * with same y value is returned (wrap).
	 */
	public Point getRightNeighbor(int x, int y) {
		x = x + 1 >= width ? 0 : x + 1;
		return getPointAt(x,y);
	}

	/**
	 * Provides a random position of the ghost area as Point Object
	 * 
	 * @return a random point in ghost area
	 * */
	public Point getRandomGhostAreaPosition() {
		if(width < 1 || height < 1)
			return null;
		
		Point p;

		// if there is a ghost area spawn somewhere in this area
		if (ghostArea != null && ghostArea.size() > 0) {
			// choose a random position in the ghost area
			int n = randGenerator.nextInt(ghostArea.size());
			int x = ghostArea.get(n).x;
			int y = ghostArea.get(n).y;
			p = getPointAt(x,y);
		} else {
			throw new RuntimeException("There is no ghost area (jail) for the ghosts.");
		}

		return p;
	}

	/**
	 * Provides a random walkable position of the maze as Point Object
	 * 
	 * @return a random walkable point of the maze
	 * */
	public Point getRandomWalkablePosition() {
		while (true) {
			int x = randGenerator.nextInt(width);
			int y = randGenerator.nextInt(height);
			if (isWalkable(x, y)){
				return getPointAt(x,y);
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("*****MAZE Begin******\n");
		sb.append("Width: " + this.width + "\n");
		sb.append("Height: " + this.height + "\n");
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				sb.append(grid[j][i].getBlockType().value + " ");
			}
			sb.append("\n");
		}
		sb.append("*****MAZE End******");
		return sb.toString();
	}

	/**
	 * Returns a list of all walkable neighbor positions of the
	 * given point. 
	 */
	public List<Point> getWalkableNeighbors(Point point) {

		List<Point> neighbors = new ArrayList<>();

		Point currNeighbor = getUpperNeighbor(point);
		if (isWalkable(currNeighbor))
			neighbors.add(currNeighbor);

		currNeighbor = getRightNeighbor(point);
		if (isWalkable(currNeighbor))
			neighbors.add(currNeighbor);

		currNeighbor = getLowerNeighbor(point);
		if (isWalkable(currNeighbor))
			neighbors.add(currNeighbor);

		currNeighbor = getLeftNeighbor(point);
		if (isWalkable(currNeighbor))
			neighbors.add(currNeighbor);

		return neighbors;
	}

}
