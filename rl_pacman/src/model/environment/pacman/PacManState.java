package model.environment.pacman;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import model.environment.MoveAction;
import model.reinforcement_learning.Action;
import model.reinforcement_learning.FeaturesState;
import model.reinforcement_learning.State;
import model.reinforcement_learning.value_function.ValueParams;
import utils.MyMath;

/**
 * This implementation of the RL state is based on
 * "Play Ms. Pac-Man Using an Advanced Reinforcement Learning Agent":
 * http://link.springer.com/chapter/10.1007%2F978-3-319-07064-3_6#page-1
 */
public class PacManState extends FeaturesState {

	public static final int NR_OF_FEATURES = 5;

	/**
	 * Represents the features for current state without an action.<br>
	 * This is the base because all subsequent featureStates(s,a) will be based
	 * on these features.
	 */
	double[] baseFeatures = new double[NR_OF_FEATURES];

	public final int IDX_WALKED_INTO_WALL = 0; // \in {0,1}
	public final int IDX_GHOST_THREAT = 1; // \in [0,4]
	public final int IDX_TRAPPED_SITUATION = 2; // \in {0,1}
	public final int IDX_CLOSEST_DISTANCE_TO_FOOD = 3; // \in [0,1]
	public final int IDX_GHOST_IS_A_NEIGHBOR = 4; // \in {0,1}

	private PacEnvironment env;
	
	/**
	 * Containes the features f(s,a) for all possible a
	 */
	private Map<Long, double[]> precalculatedFeatures = new HashMap<>();
	
	/**
	 * We need this to norm the feature closest_distance_to_food to a value between 0 and 1.
	 * This is the diagonal of the maze. We could take the value width*height but this value
	 * is to high.
	 */
	private final double longestDistance;

	public PacManState(PacEnvironment pacEnvironment, List<Action> possibleActions) {

		env = pacEnvironment;
		
		double h = env.getMaze().getWidth();
		double w = env.getMaze().getHeight();
		longestDistance = Math.sqrt(w*w + h*h);

		for (int i = 0; i < NR_OF_FEATURES; ++i) {
			baseFeatures[i] = 0.0;
		}
		
		// Calculate basestate
		Point pacPos = env.getPac().getPosition();
		setIfPacIsTrapped();
		setClosestDistanceToFood(baseFeatures, pacPos);
		setGhostThreat(baseFeatures, pacPos);
		if (MyMath.almostEquals(1.0, baseFeatures[IDX_GHOST_IS_A_NEIGHBOR])) {
			baseFeatures[IDX_CLOSEST_DISTANCE_TO_FOOD] = 0;
		}
		
		// Calculate f(s,a) for all a
		for(Action a : possibleActions) {
			precalculatedFeatures.put(a.getSimpleId(), getFeatures(a));
		}

	}
	
	public PacManState(PacManState pmsToCopy){
		env = pmsToCopy.env.getCopy();
		longestDistance = pmsToCopy.longestDistance;
		
		//deep copy features
		baseFeatures = new double[pmsToCopy.baseFeatures.length];
		for(int i=0; i<pmsToCopy.baseFeatures.length ; i++){
			baseFeatures[i] = pmsToCopy.baseFeatures[i];
		}
		
		//deep copy precalculatedFeatures
		for(Entry<Long, double[]> entry : pmsToCopy.precalculatedFeatures.entrySet()){
			double[] fts = new double[entry.getValue().length];
			for (int i = 0; i < entry.getValue().length; i++) {
				fts[i] = entry.getValue()[i];
			}
			precalculatedFeatures.put(entry.getKey(), fts);
		}
	}
	
	@Override
	public State getCopy() {
		return new PacManState(this);
	}

	private void setClosestDistanceToFood(double[] features, Point position) {

		int distance = calcClosestDistance(position, env.getDots(), getGhostPoints());
		
		if (0 == distance) {
			features[IDX_CLOSEST_DISTANCE_TO_FOOD] = 1;
		} else {
			double normDistance = distance / longestDistance ;
			normDistance = (1 - normDistance);
			if (0 < normDistance)
				features[IDX_CLOSEST_DISTANCE_TO_FOOD] = normDistance;
			else
				features[IDX_CLOSEST_DISTANCE_TO_FOOD] = 0.0;
		}
	}

	private List<Point> getGhostPoints() {
		List<Point> ghostPoints = new ArrayList<>(env.getGhosts().size());
		
		for (Ghost g : env.getGhosts()) {
			ghostPoints.add(g.getPosition());
		}
		return ghostPoints;
	}

	private void setGhostThreat(double[] features, Point position) {
		features[IDX_GHOST_THREAT] = 0;
		features[IDX_GHOST_IS_A_NEIGHBOR] = 0;

		for (Ghost g : env.getGhosts()) {
			boolean pacHitGhost = position.equals(g.getPosition());
			if (pacHitGhost) {
				features[IDX_GHOST_THREAT] += 1;
				features[IDX_GHOST_IS_A_NEIGHBOR] = 1.0;
				continue;
			}

			double distBetweenPacAndGhost = MyMath.distanceBetween(position, g.getPosition());

			if (distBetweenPacAndGhost < 8.0) {
				// 8.0 is based on the paper
				// "Play Ms. Pac-Man Using an Advanced..."
				features[IDX_GHOST_THREAT] += 1-(distBetweenPacAndGhost/8.0);
				if (MyMath.almostEquals(1.0, distBetweenPacAndGhost))
					features[IDX_GHOST_IS_A_NEIGHBOR] = 1.0;
			}
		}

	}

	private final int[] SOME_PRIMES_FOR_HASH = {1531, 1543, 1549, 1553, 1559, 1567, 1571, 1579, 1583, 1597, 1601, 1607, 1609, 1613, 1619, 1621, 1627, 1637, 1657, 1663, 1667, 1669, 1693, 1697, 1699, 1709, 1721, 1723, 1733, 1741, 1747, 1753, 1759};
	@Override
	public long getSimpleId() {
		/*
		 * This function is not used. We do not have to save PacManStates (or Features).
		 * Just an addition of prime numbers. Not really a god hash function but here is it fine.
		 */
		double sum = 0.0;
		for (int i=0; i < baseFeatures.length ;++i)
			sum += baseFeatures[i] + SOME_PRIMES_FOR_HASH[i % SOME_PRIMES_FOR_HASH.length];

		return (long)sum;
	}

	@Override
	public int hashCode() {
		return (int) getSimpleId();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (null == obj || obj.getClass() != this.getClass())
			return false;

		PacManState other = (PacManState) obj;

		return Arrays.equals(baseFeatures, other.baseFeatures);
	}

	private void setIfPacIsTrapped() {
		if (pacIsTrapped(env.getPac().getPosition()))
			baseFeatures[IDX_TRAPPED_SITUATION] = 1;
		else
			baseFeatures[IDX_TRAPPED_SITUATION] = 0;
	}

	private boolean pacIsTrapped(Point startPosition) {
		// Basic idea: If pac can move 8 steps long without hitting a wall then
		// we assume that we are not trapped. This is not entirely correct.
		// There could be mazes in which pac could move these steps but is
		// trapped. We are ignoring this case.
		// (vimaier)
		final int MAX_DISTANCE = 8;

		List<Point> visitedPoints = new ArrayList<>();
		List<PointDistancePair> pointsToCheck = new ArrayList<>();

		PointDistancePair currPair = new PointDistancePair(startPosition, 0);
		pointsToCheck.add(currPair);

		int idxOfLastElem = pointsToCheck.size() - 1;

		while (!pointsToCheck.isEmpty()) {
			// pop(): Get last element and remove it from list
			currPair = pointsToCheck.get(idxOfLastElem);
			pointsToCheck.remove(idxOfLastElem);
			idxOfLastElem--;

			if (MAX_DISTANCE <= currPair.distance)
				return false; // We can walk at least MAX_DISTANCE steps and
								// thus are not trapped

			if (visitedPoints.contains(currPair.point))
				continue;
			visitedPoints.add(currPair.point);

			// Try to spread out from the location of the walkable neighbors
			List<Point> walkableNeighbors = env.getMaze().getWalkableNeighbors(currPair.point);
			for (Point position : walkableNeighbors) {
				if (meetsAGhost(position))
					continue;
				pointsToCheck.add(new PointDistancePair(position, currPair.distance + 1));
				idxOfLastElem++;
			}
		}

		return true; // We are not able to walk at least MAX_DISTANCE

	}

	private boolean meetsAGhost(Point position) {
		for (Ghost g : env.getGhosts()) {
			if (g.getPosition().equals(position))
				return true;
		}
		return false;
	}

	@Override
	public int getNumberOfFeatures() {
		return baseFeatures.length;
	}

	@Override
	public List<Action> getPossibleActions() {
		return PacEnvironment.ALL_ACTIONS_EXCEPT_DO_NOTHING;
	}
	
	@Override
	public double[] getFeatures(ValueParams params) {
		Action action = params.getAction();
		
		assert (null != action && action instanceof MoveAction);
		
		double[] features = precalculatedFeatures.get(action.getSimpleId());
		
		assert(features != null && features.length == baseFeatures.length);
		
		return features;
	}

	private double[] getFeatures(Action action) {
		assert (null != action && action instanceof MoveAction);

		// Here we need to adapt current features to the action
		Point newPos = getResultingPositionFor((MoveAction) action);

		if (newPos.equals(env.getPac().getPosition())) {
			// This represents an action which will result in walking against the wall
			double[] copiedFeatures = getCopyOfBaseFeatures();
			copiedFeatures[IDX_WALKED_INTO_WALL] = 1.0;
			// We don't want that the closest distance to food weight will be affected 
			copiedFeatures[IDX_CLOSEST_DISTANCE_TO_FOOD] = 0.0;
			return copiedFeatures; 
		}

		// Else adapt features to new position

		double[] copiedFeatures = getCopyOfBaseFeatures();

		// Action does not change anything on the situation of being trapped
		setClosestDistanceToFood(copiedFeatures, newPos);
		setGhostThreat(copiedFeatures, newPos);
		if (MyMath.almostEquals(1.0, copiedFeatures[IDX_GHOST_IS_A_NEIGHBOR])) {
			copiedFeatures[IDX_CLOSEST_DISTANCE_TO_FOOD] = 0;
		}
		
		return copiedFeatures;
	}

	private double[] getCopyOfBaseFeatures() {
		double[] newFeatures = new double[NR_OF_FEATURES];
		// Deep copy
		for (int i=0; i < NR_OF_FEATURES ;++i) {
			newFeatures[i] = baseFeatures[i];
		}
		return newFeatures;
	}

	private Point getResultingPositionFor(MoveAction action) {
		Maze maze = env.getMaze();
		Point desiredPosition = maze.getPointAt(0, 0);
		Point currPos = env.getPac().getPosition();

		switch (action) {
		case GO_NORTH:
			desiredPosition = maze.getUpperNeighbor(currPos);
			break;
		case GO_EAST:
			desiredPosition = maze.getRightNeighbor(currPos);
			break;
		case GO_SOUTH:
			desiredPosition = maze.getLowerNeighbor(currPos);
			break;
		case GO_WEST:
			desiredPosition = maze.getLeftNeighbor(currPos);
			break;
		default:
			desiredPosition = currPos;
		}

		if (maze.isWalkable(desiredPosition))
			return desiredPosition;
		else
			return currPos;
	}

	/**
	 * Calculates the smallest number of steps to get from startPosition to one
	 * of desiredPositions.
	 * 
	 * @param startPosition
	 *            represents a walkable location on the maze (e.g. current pac
	 *            position).
	 * @param desiredPositions
	 *            walkable positions. Could be dots or ghosts.
	 * @param additionalNotWalkablePoints
	 * 			  Additional points which will be handled as not walkable. This is necessary 
	 * if we are searching for dots but are not allowed to walk through ghosts. In this case 
	 * the ghosts would be the additionalNotWalkablePoints.
	 * 			
	 */
	private int calcClosestDistance(Point startPosition, List<? extends IPosition> desiredPositions, List<Point> additionalNotWalkablePoints) {
		
		if (null == additionalNotWalkablePoints)
			additionalNotWalkablePoints = new LinkedList<>();
		
		List<Point> visitedOrPlannedToVisitPoints = new ArrayList<>();
		LinkedList<PointDistancePair> pointsToCheck = new LinkedList<>();

		PointDistancePair currPair = new PointDistancePair(startPosition, 0);
		pointsToCheck.add(currPair);
		visitedOrPlannedToVisitPoints.add(currPair.point);

		while (!pointsToCheck.isEmpty()) {
			// pop(): Get first element and remove it from list
			currPair = pointsToCheck.pop();

			// If we find here a desiredPosition then return distance
			for (IPosition pos : desiredPositions)
				if (pos.getPosition().equals(currPair.point))
					return currPair.distance;

			// Else spread out from the location of the walkable neighbors
			List<Point> walkableNeighbors = env.getMaze().getWalkableNeighbors(currPair.point);
			for (Point position : walkableNeighbors) {
				if (visitedOrPlannedToVisitPoints.contains(position))
					continue;
				if (additionalNotWalkablePoints.contains(position))  
					// Then we have a position which is walkable in the maze but currently a ghost is in this position
					continue;
				
				pointsToCheck.addLast(new PointDistancePair(position, currPair.distance + 1));
				visitedOrPlannedToVisitPoints.add(position);
			}
		}

		return Integer.MAX_VALUE;

	}

	private static class PointDistancePair {
		Point point;
		int distance;

		public PointDistancePair(Point p, int dist) {
			point = p;
			distance = dist;
		}
	}

}
