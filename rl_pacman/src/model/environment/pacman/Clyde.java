package model.environment.pacman;

import java.awt.Point;

/**
 * Clyde is the orange ghost. He starts in the jail and is initially waiting. In
 * contrast to the other ghosts clyde will use a scatter target. As long as 
 * Clyde is not in scatter range he will move towards the pac but as soon as he
 * reaches the scatter range he will change his target and most likely run away
 * from the pac until he leaves the scatter range again.
 */
public class Clyde extends Blinky {

	public static final int DIST = 8;

	/**
	 * only Clyde uses (and sets) this. the scatter target is the bottom left
	 * corner.
	 */
	public Point scatterTarget;

	public Clyde(PacEnvironment env) {
		super(env);
		this.scatterTarget = environment.getMaze().getPointAt(0, environment.getMaze().getHeight() - 1);
	}

	/**
	 * Provides the start location of Clyde. Clydes start location is a point in
	 * the jail.
	 * 
	 * @return the start location of the ghost as point object
	 * */
	@Override
	protected Point getStartLocation() {
		return environment.getMaze().getRandomGhostAreaPosition();
	}

	/**
	 * Provides the target of Clyde. Clydes target is the pac itself.If Clyde is
	 * within 8 tiles of the Pac, he changes his target to be the bottom left
	 * corner of the screen.
	 * 
	 * @return the target of the ghost as point object
	 * */
	@Override
	public Point getTarget() {

		Point pacPos = environment.getPac().getPosition();

		// as long as the Clyde is not in scatter range move towards pac.
		if (position.distanceSq(pacPos) > DIST * DIST)
			return super.getTarget();

		else
			return scatterTarget;
	}

}
