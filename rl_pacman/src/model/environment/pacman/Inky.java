/**
 * 
 */
package model.environment.pacman;

import java.awt.Point;

/**
 * Inky is the cyan ghost. He starts in the box and initially waiting. His
 * target (see Ghost.java) is complex: it's on the opposite side of Pinky's
 * target than the location of Blinky. So if Pinky's target is the vector p, and
 * Blinky is at b, then Inky's target is p + (p - b).
 */
public class Inky extends Pinky {

	// we need blinky to calculate our target
	private Blinky blinky;

	public Inky(Blinky blinky, PacEnvironment env) {
		super(env);
		this.blinky = blinky;
	}

	/**
	 * Provides the target of inky. Inkys target is on the opposite side of
	 * Blinky's target
	 * 
	 * @return the target of the ghost as point object
	 * */
	@Override
	public Point getTarget() {
		Point target = super.getTarget();
		Point blinkyPos = blinky.position;
		int targetX = 2 * blinkyPos.x - target.x;
		int targetY = 2 * blinkyPos.y - target.y;
		
		return environment.getMaze().getPointAt(targetX, targetY);
	}

}
