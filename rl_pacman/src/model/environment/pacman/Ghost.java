package model.environment.pacman;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import model.environment.MoveAction;

/**
 * 
 * Abstract Class for Ghosts. There are four specific ghosts: Blinky, Pinky,
 * Inky, and Clyde. Each with his special behavior. (see subclasses for details)
 * 
 * Every Ghost has a target that they are trying to achieve. While trying to
 * achieve the target the ghost determines in the move() method which direction
 * it takes. Ghosts do not reverse their direction if not necessary (only if it
 * is the only possible action).
 * 
 * Ghosts are not allowed to walk back to the jail once they left it.
 * 
 */

public abstract class Ghost extends MovableObject {

	private GhostState state = GhostState.INJAIL;

	/**
	 * a default value for delaying the ghost waiting. thus it is easier to
	 * manage ghost exiting (ghost exists one after another)
	 */
	public static final int GHOST_WAITING_PERIOD_DELAY = 2;

	/**
	 * waiting periods for ghosts. Defines how long each ghost is waiting at the
	 * beginning of the game and after reset (Note that Blinky is not in jail at
	 * the beginning of the game, therefore his waiting period at the beginning
	 * is 0 seconds ).
	 */
	private static final int DEFAULT_WAITING_PERDIOD = 5; // in time steps

	protected PacEnvironment environment;	

	/**
	 * Ghost waiting period in time steps. can be set using a setter method with
	 * the number of ghosts. else it will be just the default value and all
	 * ghosts will have the same waiting period. ghostWaiting is the time the
	 * ghost will stay in jail after reset.
	 */
	protected int ghostWaiting = DEFAULT_WAITING_PERDIOD;

	/**
	 * waiting time counter. If remainingWaiting <= 0, the ghost is free to
	 * move.
	 */
	protected int remainingWaiting = ghostWaiting;

	/** target position of the ghost (used by subclasses) */
	protected Point target;

	/**
	 * Provides the current target of the ghost. the target is different for
	 * every specific ghost
	 **/
	protected abstract Point getTarget();

	protected Ghost(PacEnvironment env) {
		environment = env;
		position = getStartLocation();
		remainingWaiting = ghostWaiting;
	}

	/**
	 * Provides a start location for the ghost
	 * 
	 * @return Point a random point in ghost area
	 * */
	protected abstract Point getStartLocation();

	private void setToStartLocation() {
		this.position = getStartLocation();
		this.state = GhostState.INJAIL;
	}

	/**
	 * Provides a walkable position in the maze
	 * 
	 * @return the first walkable location of the maze that comes along
	 * */
	protected Point getFirstWalkablePosition() {
		Maze maze = environment.getMaze();
		for (int x = 0; x < maze.getWidth(); x++) {
			for (int y = 0; y < maze.getHeight(); y++) {
				if (maze.isWalkable(x, y)) {
					return maze.getPointAt(x, y);
				}
			}
		}

		// this should not happen
		throw new RuntimeException("There is no walkable location for ghost spawning in this maze");
	}

	/**
	 * Provides the state of the ghost (hunting, injail, exiting)
	 * 
	 * @return the current state of the ghost
	 * */
	public GhostState getState() {
		return state;
	}

	/**
	 * Sets the waiting period for this ghost (the time before the ghost will
	 * leave the jail)
	 * 
	 * @param waitingPeriod
	 *            a positive integer
	 * */
	public void setWaitingPeriod(int nrOfGhost) {
		if (nrOfGhost > 0) {
			ghostWaiting = nrOfGhost * GHOST_WAITING_PERIOD_DELAY;
		}
	}

	/**
	 * Moves the ghost. If the ghost is currently in jail he will stay there
	 * until the timer is 0. After this the ghost will try to exit the jail.
	 * Afterwards the ghost will chase the pac according to his special
	 * behaviour.
	 * */
	public void move() {

		MoveAction action = MoveAction.DO_NOTHING;

		// if ghost is still waiting, do nothing
		if (remainingWaiting > 0) {
			// the ghost will stay at the position
			action = MoveAction.DO_NOTHING;
			remainingWaiting--;
		}

		// if the ghost is currently in jail but timer is over => exit
		else if (state == GhostState.INJAIL) {

			state = GhostState.EXITING;

			List<Point> exitPoints = environment.getMaze().getGhostExitArea();

			// we will move to a exit point
			if (exitPoints != null && exitPoints.size() > 0) {
				target = exitPoints.get(0);
			}
			action = actionToTarget(position, target);

		}

		// if the ghost is exiting
		else if (state == GhostState.EXITING) {

			// if we reached the exit point (target) choose old target (we will
			// leave the exit area anyway, because the ghost will not turn
			// around (back to jail) and will head toward his new target)
			// but we have to stay in exiting mode because we can't walk in the
			// exit area otherwise. the exit mode will be reset after we reached
			// the first walkable grid field
			if (position == target) {
				target = getTarget();
			}

			// note: if we didn't reached the exit point yet, the
			// target is still the exit point
			action = actionToTarget(position, target);
		}

		// we are currently hunting the pac
		else {

			// we have to call getTarget because Clyde has a scattertarget,
			// depending on how close he is to pac
			target = getTarget();

			// move toward target
			Point target = getTarget();
			action = actionToTarget(position, target);

		}

		super.performAction(action, environment.getMaze());

		// if we left the jail with our last move we are no longer exiting but
		// hunting
		if (state == GhostState.EXITING && environment.getMaze().isWalkable(position)) {
			state = GhostState.HUNTING;
		}
	}

	public void reset() {
		setToStartLocation();
		remainingWaiting = ghostWaiting;
	}

	/**
	 * Determines the 'best' action for the ghost to reach its target
	 * 
	 * @param current
	 *            the current location of the ghost
	 * @param target
	 *            the target location
	 * @return the "best" action for the ghost. (Means: the action that leads to
	 *         the grid field that is closest to the target.)
	 * 
	 * */
	private MoveAction actionToTarget(Point current, Point target) {

		MoveAction bestAction = MoveAction.DO_NOTHING;

		// the distance between ghost and target (if ghost would move
		// in the desired direction)
		// target is not necessary the pac
		double bestDistance = Double.MAX_VALUE;
		double tmpDistance;

		Point desiredPosition;

		// reverse action will only occur if it is the only possible action
		List<MoveAction> possibleActions = getPossibleActions(current);

		for (int i = 0; i < possibleActions.size(); i++) {

			desiredPosition = getDesiredPosition(current, possibleActions.get(i), environment.getMaze());
			tmpDistance = target.distance(desiredPosition);

			// choose the action with the smallest distance to target
			if (tmpDistance < bestDistance) {
				bestDistance = tmpDistance;
				bestAction = possibleActions.get(i);
			}

		}

		return bestAction;

	}

	/**
	 * Determines all possible actions for the ghost at its current location
	 * 
	 * @param current
	 *            the current location of the ghost
	 * @param all
	 *            possible actions
	 * */
	private List<MoveAction> getPossibleActions(Point current) {

		MoveAction reverseAction = reverseOf(lastAction);

		// check all possibilities
		List<MoveAction> possibleActions = new ArrayList<MoveAction>();

		// prefer these actions
		if (MoveAction.GO_NORTH != reverseAction && isActionPossible(current, MoveAction.GO_NORTH)) {
			possibleActions.add(MoveAction.GO_NORTH);
		}

		if (MoveAction.GO_EAST != reverseAction && isActionPossible(current, MoveAction.GO_EAST)) {
			possibleActions.add(MoveAction.GO_EAST);
		}

		if (MoveAction.GO_SOUTH != reverseAction && isActionPossible(current, MoveAction.GO_SOUTH)) {
			possibleActions.add(MoveAction.GO_SOUTH);
		}

		if (MoveAction.GO_WEST != reverseAction && isActionPossible(current, MoveAction.GO_WEST)) {
			possibleActions.add(MoveAction.GO_WEST);
		}

		// maybe there's no choice but to reverse
		if (possibleActions.size() < 1)
			possibleActions.add(reverseAction); // always possible to do

		return possibleActions;

	}

	/**
	 * Determines if the given action is possible at the given location of the
	 * ghost
	 * 
	 * @param position
	 *            the current position of the ghost
	 * @param action
	 *            the action which it should perform
	 * @return true if the action is possible, false if not (e.g. if the action
	 *         would cause the ghost to walk into a wall)
	 * 
	 * 
	 * */
	private boolean isActionPossible(Point position, MoveAction action) {
		Point desiredPosition = getDesiredPosition(position, action, environment.getMaze());
		return isPositionAllowed(desiredPosition, environment.getMaze());
	}

	/**
	 * Determines whether the given position is walkable for the ghost in the
	 * current situation or not
	 * 
	 * @param desiredPosition
	 *            the position to check
	 * @param maze
	 *            the current maze where the ghost is located in
	 * @return true if ghost is allowed at the given position, false otherwise
	 * */
	@Override
	protected boolean isPositionAllowed(Point desiredPosition, Maze maze) {
		// we can allow the ghost to exit the jail without using the
		// normal exit way if there is a immediate exit possibility to a
		// walkable area
		// however he will never walk through walls
		if (maze.isWalkable(desiredPosition))
			return true;

		// Ghost is only allowed to walk in Ghost Area or Ghost Exit Area if he
		// is currently exiting the jail
		if ((maze.isGhostExitingArea(desiredPosition) || maze.isGhostArea(desiredPosition)) && state == GhostState.EXITING)
			return true;

		return false;
	}

}
