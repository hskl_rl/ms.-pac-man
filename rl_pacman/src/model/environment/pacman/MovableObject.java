package model.environment.pacman;

import java.awt.Point;
import java.util.List;

import model.environment.EnvironmentChangeListener;
import model.environment.MoveAction;
import model.reinforcement_learning.Action;

/**
 * An abstract class representing an object that can take moveactions to move
 * from one position to another within the game environment.
 * Provides several methods to maintain current and future positions according
 * to the move actions. Also notifies its environmentchangelisteners about objectMoved.
 */
public abstract class MovableObject implements IPosition {

	/**
	 * The previously taken move action.
	 */
	protected MoveAction lastAction = MoveAction.GO_NORTH;

	/**
	 * Listeners to notify when the object moved
	 */
	private List<EnvironmentChangeListener> envChangedListeners;

	/**
	 * The position of the object within the game environment.
	 */
	protected Point position;

	/**
	 * Provides the previously taken move action of the object 
	 */
	public MoveAction getLastAction() {
		return lastAction;
	}

	/**
	 * Provides the current position of the object
	 */
	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}

	/**
	 * @return the opposite direction action of the one provided. The opposite
	 *         of DO_NOTHING is DO_NOTHING.
	 */
	protected static MoveAction reverseOf(MoveAction action) {
		switch (action) {
		case GO_NORTH:
			return MoveAction.GO_SOUTH;
		case GO_SOUTH:
			return MoveAction.GO_NORTH;
		case GO_WEST:
			return MoveAction.GO_EAST;
		case GO_EAST:
			return MoveAction.GO_WEST;
		default:
			return MoveAction.DO_NOTHING;
		}
	}

	/**
	 * Determines a point which represents the desired position.
	 * The desired position results by performing the given action from the
	 * given currentPosition within the given maze.
	 * @param currentPosition
	 * 			The position from where the action should be taken
	 * @param action
	 * 			The action that should be taken
	 * @param maze
	 * 			The maze in which the action takes place
	 * @return
	 * 			The resulting position
	 */
	protected Point getDesiredPosition(Point currentPosition, MoveAction action, Maze maze) {

		Point desiredPosition = maze.getPointAt(0, 0);

		switch (action) {
		case GO_NORTH:
			desiredPosition = maze.getUpperNeighbor(this.position);
			break;
		case GO_EAST:
			desiredPosition = maze.getRightNeighbor(this.position);
			break;
		case GO_SOUTH:
			desiredPosition = maze.getLowerNeighbor(this.position);
			break;
		case GO_WEST:
			desiredPosition = maze.getLeftNeighbor(this.position);
			break;
		default:
			desiredPosition = currentPosition;
		}

		return desiredPosition;
	}

	/**
	 * Performs the given action in the given maze from the current position of the movable object.
	 * This makes the object to actually move and notfies the environmentChangedListener about the movement.
	 */
	public void performAction(Action nextAction, Maze maze) {
		assert (nextAction != null && maze != null && nextAction instanceof MoveAction);

		Point newPos = null;
		MoveAction action = (MoveAction) nextAction;

		newPos = getDesiredPosition(position, action, maze);
		boolean newPosAllowed = isPositionAllowed(newPos, maze);

		// the action results in a teleport when the new position is allowed,
		// both positions are teleport fields and both positions differ from
		// each other
		boolean isTeleport = newPosAllowed
							 && maze.isTeleportPos(this.position) && maze.isTeleportPos(newPos) 
							 && (this.position.x != newPos.x || this.position.y != newPos.y);

		if (newPosAllowed) {
			setPosition(newPos);
		}

		lastAction = action;
		notifyMovement(newPosAllowed, isTeleport);
	}

	/*
	 * Sets the movable object to the given position within the given maze
	 * without performing any action and only if the given position is allowed.
	 */
	public void teleportTo(Point pos, Maze maze) {
		boolean newPosAllowed = isPositionAllowed(pos, maze);
		if (newPosAllowed) {
			this.position = pos;
		}
		notifyMovement(newPosAllowed, true);
	}

	/**
	 * Sets the objects lastAction to DO_NOTHING
	 */
	public void invalidateLastAction() {
		this.lastAction = MoveAction.DO_NOTHING;
	}

	/**
	 * Notifies the attached EnvironmentChangeListeners about the movement of this object.
	 * @param hasNewPosition
	 * 			whether the movement resulted in a new position
	 * @param isTeleport
	 * 			whether the movement was a teleport
	 */
	protected void notifyMovement(final boolean hasNewPosition, final boolean isTeleport) {
		if (envChangedListeners == null)
			return;

		for (EnvironmentChangeListener listener : envChangedListeners) {
			listener.objectMoved(this, hasNewPosition, isTeleport);
		}
	}

	public void setListeners(List<EnvironmentChangeListener> envChangeListener) {
		this.envChangedListeners = envChangeListener;
	}

	/**
	 * Determines whether the given position of the given maze is allowed to be
	 * moved to by this movable object.
	 * @return true if the object is allowed to move there, false otherwise.
	 */
	protected abstract boolean isPositionAllowed(Point desiredPosition, Maze maze);

}
