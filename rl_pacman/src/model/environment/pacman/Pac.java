/**
 * 
 */
package model.environment.pacman;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import model.environment.MoveAction;

/**
 * This class contains the logic of the pac movement.
 */
public class Pac extends MovableObject {
	private static final int INIT_NUM_OF_LIVES = 3;
	private static final int NEW_LIFE_AT = 10000;

	private Point spawnPosition;
	private List<Dot> eatenDots = new ArrayList<>();
	private int lives;
	private int score;
	private int numOfLivesWon;

	public Pac(Point pacSpawnPos) {
		this.spawnPosition = pacSpawnPos;
		setPosition(pacSpawnPos);
		this.lives = INIT_NUM_OF_LIVES;
		this.score = 0;
		this.numOfLivesWon = 0;
		this.lastAction = MoveAction.DO_NOTHING;
	}

	public int getLives() {
		return lives;
	}

	public String getLivesString() {
		return lives + "";
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public int getScore() {
		return score;
	}

	public String getScoreString() {
		return score + "";
	}

	public void setScore(int score) {
		this.score = score;
	}

	public void setToSpawn() {
		setPosition(spawnPosition);
	}

	public void incrementLives() {
		numOfLivesWon++;
		lives++;
	}

	public void decrementLives() {
		lives = lives > 0 ? lives - 1 : 0;
	}

	public void eat(Dot eatenDot) {
		eatenDots.add(eatenDot);
		this.score += eatenDot.getScoreValue();

		if (this.score > NEW_LIFE_AT * (numOfLivesWon + 1)) {
			incrementLives();
		}
	}

	public void reset() {
		this.score = 0;
		this.lives = INIT_NUM_OF_LIVES;
		this.numOfLivesWon = 0;
		setToSpawn();
		invalidateLastAction();
	}

	/**
	 * @return true if pac is allowed at the given position, false otherwise
	 * */
	@Override
	protected boolean isPositionAllowed(Point desiredPosition, Maze maze) {
		return maze.isWalkable(desiredPosition);
	}
	
	/**
	 * Checks if given action will result into a turn to the wall.
	 * Used to avoid annoying behavior if human player turns pac to
	 * to a wall (although the intention was to turn around the next corner)
	 */
	public boolean willNotRunIntoAWallBy(MoveAction nextPacAction, Maze maze) {
		Point newPos = getDesiredPosition(position, nextPacAction, maze);
		boolean newPosAllowed = isPositionAllowed(newPos, maze);
		return newPosAllowed;
	}

	public Point getSpawnPosition() {
		return spawnPosition;
	}
	
}
