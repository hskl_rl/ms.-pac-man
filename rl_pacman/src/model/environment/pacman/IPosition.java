package model.environment.pacman;

import java.awt.Point;

public interface IPosition {
	public Point getPosition();
}
