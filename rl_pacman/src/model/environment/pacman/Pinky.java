/**
 * 
 */
package model.environment.pacman;

import java.awt.Point;

/**
 * Pinky is the pink ghost. He starts in the box and initially waiting. His
 * target (see Ghost.java) is four steps ahead of where the Pac is facing.
 */
public class Pinky extends Ghost {

	public static final int DIST = 4;

	public Pinky(PacEnvironment env) {
		super(env);
	}

	/**
	 * Provides a start location for pinky and inky. Both ghosts spawn in jail.
	 * 
	 * @return a random point in the ghost area
	 * */
	@Override
	protected Point getStartLocation() {
		return environment.getMaze().getRandomGhostAreaPosition();
	}

	/**
	 * Provides the target of pinky. Pinkys target is four steps ahead of where
	 * the Pac is facing
	 * 
	 * @return the target of the ghost as point object
	 * */
	@Override
	public Point getTarget() {

		Pac pac = environment.getPac();
		Point pacPos = pac.getPosition();
		Maze maze = environment.getMaze();
		
		if (null != pac.getLastAction()){
			switch (pac.getLastAction()) {
			case GO_NORTH:
				return maze.getPointAt(pacPos.x, pacPos.y - DIST);
			case GO_EAST:
				return maze.getPointAt(pacPos.x + DIST, pacPos.y);
			case GO_SOUTH:
				return maze.getPointAt(pacPos.x, pacPos.y + DIST);
			case GO_WEST:
				return maze.getPointAt(pacPos.x - DIST, pacPos.y);
			default:
				break;
			}
		}
		return maze.getPointAt(pacPos.x, pacPos.y);
	}
}
