package model.environment.simulation;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.logging.Logger;

import model.Task;
import model.environment.EnvironmentChangeListener;
import model.environment.MoveAction;
import model.environment.pacman.Dot;
import model.environment.pacman.Maze;
import model.environment.pacman.Pac;
import model.reinforcement_learning.Action;
import model.reinforcement_learning.Environment;
import model.reinforcement_learning.Reward;
import model.reinforcement_learning.State;
import model.reinforcement_learning.value_function.Value;
import model.reinforcement_learning.value_function.ValueFunction;
import model.reinforcement_learning.value_function.ValueParams;

/**
 * Represents the environment of the reinforcement learning task. Holds the
 * model information for pac, ghosts, dots and the maze. The purpose of the
 * environment is to provide states and change the current states according to
 * the actions of the agent. Moreover it delivers the rewards to the agent, thus
 * the reinforcement learning is possible. This specific simulation environment
 * is very simple. In contrast to the original pacman game environment it is
 * fully deterministic and provides a fixed positive and fixed negative reward
 * if the pac reaches a terminal state (pac hit a ghost or reached the dot).
 */
public class SimulationEnvironment implements Environment {

	protected final static Logger log = Logger.getLogger(SimulationEnvironment.class.getName());

	private static final Reward REWARD_NEGATIVE_TERMINAL_STATE = new Reward(-1.0);
	private static final Reward REWARD_POSITIVE_TERMINAL_STATE = new Reward(1.0);
	private static final Reward REWARD_NEUTRAL = new Reward(0.0);

	/**
	 * Since the pos. and neg. terminal states are only 'pre terminal' states we
	 * need this state
	 */
	public static final SimulationState TERMINAL_STATE = new SimulationState(new Point(-1, -1));

	private final List<EnvironmentChangeListener> envChangedListeners = new ArrayList<>();
	public static final List<Action> ALL_ACTIONS = Collections.unmodifiableList(new ArrayList<Action>(EnumSet.allOf(MoveAction.class)));
	public static final List<Action> ALL_ACTIONS_EXCEPT_DO_NOTHING;

	static {
		ArrayList<Action> tmpActionsEceptNothing = new ArrayList<>();
		for (MoveAction a : EnumSet.allOf(MoveAction.class)) {
			if (a != MoveAction.DO_NOTHING)
				tmpActionsEceptNothing.add(a);
		}

		ALL_ACTIONS_EXCEPT_DO_NOTHING = Collections.unmodifiableList(tmpActionsEceptNothing);
	}

	private Pac pac;
	private List<SimulationState> positivePreterminalStates;
	private List<SimulationState> negativePreterminalStates;
	private List<Dot> dotsForMarkingTerminalStates;
	private List<Point> staticGhostsForMarkingTerminalStates;
	private Maze maze;
	private Task currentTask;
	private SimulationState currentState;

	/**
	 * If we are in learning mode then the agent will receive rewards and
	 * updates his value function accordingly. In SimulationEnvironment we are
	 * always in learning mode.
	 */
	private boolean isInLearningMode = true;

	/**
	 * If we are in Training mode then no progress is visually shown to the
	 * user. we are only in traning mode if the user hit the "training" button.
	 * */
	private boolean isInTrainingMode = false;

	private boolean episodeFinished = false;

	/** steps of the current episode. For debugging an testing purposes. **/
	private int stepCounter = 0;

	public boolean isEpisodeFinished() {
		return episodeFinished;
	}

	/**
	 * creates a simulation evironment with the given maze and prepares the
	 * model information of pac, ghosts and dots for starting a new game.
	 */
	public SimulationEnvironment(Maze maze) {
		this.maze = maze;
		setPac();
		positivePreterminalStates = new ArrayList<>();

		loadPreterminalStates();
		populateDots();
		populateStaticGhosts();
		Point pacPos = pac.getPosition();

		currentState = getStateForPos(pacPos.x, pacPos.y);
	}

	/**
	 * @return true if state s is a pre-terminal state in this environment. a
	 *         leaving a pre-state causes the distribution of the earned rewards
	 *         to the agent.
	 * */
	public boolean isPreTerminalState(State s) {

		if (s instanceof SimulationState) {

			SimulationState simState = (SimulationState) s;

			for (SimulationState curState : negativePreterminalStates) {
				if (simState.equals(curState))
					return true;
			}

			for (SimulationState curState : positivePreterminalStates) {
				if (simState.equals(curState))
					return true;
			}
		}

		return false;
	}

	/**
	 * initialises all positive and negative preterminal states lists. the maze
	 * object will provide the information where the positive and negative
	 * preterminal states are located in the maze.
	 */
	private void loadPreterminalStates() {

		positivePreterminalStates = new ArrayList<>();
		negativePreterminalStates = new ArrayList<>();

		for (Point p : maze.getPositiveRewardFields()) {
			positivePreterminalStates.add(new SimulationState(p));
		}

		for (Point p : maze.getNegativeRewardFields()) {
			negativePreterminalStates.add(new SimulationState(p));
		}

	}

	/**
	 * resets pac and dot
	 * */
	public void reset() {
		setPac();
		populateDots();
		populateStaticGhosts();
		currentState = getStateForPos(pac.getPosition().x, pac.getPosition().y);
	}

	public Task getCurrentTask() {
		return currentTask;
	}

	public void setCurrentTask(Task currentTask) {
		this.currentTask = currentTask;
	}

	private SimulationState getStateForPos(int x, int y) {
		return new SimulationState(maze.getPointAt(x, y));
	}

	private void setPac() {
		pac = new Pac(maze.getPacSpawnPos());
		pac.setListeners(envChangedListeners);
	}

	public Pac getPac() {
		return this.pac;
	}

	public Maze getMaze() {
		return this.maze;
	}

	public List<Dot> getDotsForMarkingTerminalStates() {
		return dotsForMarkingTerminalStates;
	}

	public List<Point> getStaticGhostsForMarkingTerminalStates() {
		return staticGhostsForMarkingTerminalStates;
	}

	public void setMaze(Maze maze) {
		this.maze = maze;
	}

	/**
	 * Populates the dot at the same position for every maze
	 */
	public void populateDots() {
		dotsForMarkingTerminalStates = new ArrayList<>();

		for (SimulationState s : positivePreterminalStates) {
			dotsForMarkingTerminalStates.add(new Dot(s.getPosition()));
		}
	}

	/**
	 * Populates the ghost at the negative preterminal states
	 */
	public void populateStaticGhosts() {

		staticGhostsForMarkingTerminalStates = new ArrayList<>();

		for (SimulationState s : negativePreterminalStates) {
			staticGhostsForMarkingTerminalStates.add(s.getPosition());
		}

	}

	/**
	 * is public to cancel an episode from SimulationController if the button
	 * clicked the user "Stop Episode"
	 * */
	public void cancelEpisode() {
		finishEpisode(true);
	}

	/**
	 * is public to cancel an episode from SimulationController if the button
	 * clicked the user "Stop Episode"
	 * */
	private void finishEpisode(boolean canceled) {
		episodeFinished = true; // cancels the thread for episode
		notifyAboutEpisodeEnded(canceled, stepCounter);
		stepCounter = 0;
	}

	@Override
	public void performOneEpisode() {
		episodeFinished = false;

		while (!episodeFinished) {
			performOneTimeStep();

			// if we want to show the progress to the user we have to
			// slow down the pac movement
			if (!isInTrainingMode) {
				try {
					Thread.sleep(200);
				} catch (InterruptedException interrupted) {
					break;
				}
			}
		}
	}

	/**
	 * One time step represents the transition from t to t+1. Invokes the
	 * RL-agent and executes received action.
	 */
	public void performOneTimeStep() {
		State currentState = getCurrentState();
		Action nextAction = currentTask.getAgent().determineNextAction(currentState, getCurrentPossibleActions());
		executeAction(nextAction);
		stepCounter++;
	}

	/**
	 * returns all possible actions for the pac as a list according to his
	 * current position. If the action would lead to a situation where the pac
	 * would move inside a wall this action is not possible and will
	 * not be included in the list.
	 * */
	@Override
	public List<Action> getCurrentPossibleActions() {
		List<Action> resultList = new ArrayList<>();

		// If pac is in a preterminal state only go north is possible because he
		// will reach a terminal state and will be reset afterwards anyway
		for (SimulationState s : positivePreterminalStates) {
			if (pacIsAt(s)) {
				resultList.add(MoveAction.GO_NORTH);
				return resultList;
			}
		}
		for (SimulationState s : negativePreterminalStates) {
			if (pacIsAt(s)) {
				resultList.add(MoveAction.GO_NORTH);
				return resultList;
			}
		}

		// Pac is not at a preterminalstate
		int x = pac.getPosition().x;
		int y = pac.getPosition().y;

		if (maze.isWalkable(x, y - 1))
			resultList.add(MoveAction.GO_NORTH);
		if (maze.isWalkable(x - 1, y))
			resultList.add(MoveAction.GO_WEST);
		if (maze.isWalkable(x, y + 1))
			resultList.add(MoveAction.GO_SOUTH);
		if (maze.isWalkable(x + 1, y))
			resultList.add(MoveAction.GO_EAST);

		return resultList;
	}

	/**
	 * Executes the given action. Caution, this results in a transition from t
	 * to t+1. This should only be called from a controller for a human player.
	 * 
	 * @param nextAction
	 */
	public void executeAction(Action nextAction) {
		boolean dotWasEaten = checkIfDotWasEaten();
		boolean ghostCaught = dotWasEaten ? false : checkIfPacWasCaught();
		boolean needsToResetGame = false;

		// We check if dotWasEaten and ghostCaught because these represents
		// preterminal states
		// and they give another reward. If we would not do this then the agent
		// would become two updates.
		if (dotWasEaten || ghostCaught) {
			finishEpisode(false);
			needsToResetGame = true;
			if (isInLearningMode) {
				rewardPacForReachingTerminalState();
				currentTask.addToTrainingLevel(1);
			}

		} else {
			// Send action to Pac
			pac.performAction(nextAction, this.maze);

			SimulationState previousState = currentState;
			currentState = new SimulationState(pac.getPosition());
			if (isInLearningMode) {
				// Every reward except exiting pos. and neg. states is 0.0
				Reward reward = new Reward(0.0);
				currentTask.getAgent().rewardLastAction(new ValueParams(previousState, nextAction, reward, getCurrentState()));
			}
			notifyAboutStateValueChanged(previousState);
		}

		if (needsToResetGame) {
			reset();
		}

	}

	/**
	 * will return true if the pac is currently at the some position with one of
	 * the remaining dots in the maze. will return false otherwise.
	 */
	private boolean checkIfDotWasEaten() {

		for (Dot dot : dotsForMarkingTerminalStates) {
			// Check if pac is on the same coordinate as a dot
			if (dot.getPosition().equals(pac.getPosition())) {
				pac.eat(dot);
				return true;
			}
		}
		return false;
	}

	/**
	 * Will return true if the pac is currently at the same location as one of
	 * the static ghosts in the maze. will return false otherwise.
	 * */
	private boolean checkIfPacWasCaught() {

		for (Point staticGhost : staticGhostsForMarkingTerminalStates) {
			// Check if pac is on the same coordinate as a ghost
			if (staticGhost.equals(pac.getPosition())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * checks if the pac reached a preteriminal state with its last action and
	 * returns the positive or negative reward accordingly.
	 */
	private Reward getRewardForLastExecutedAction() {
		for (SimulationState s : positivePreterminalStates) {
			if (pacIsAt(s))
				return REWARD_POSITIVE_TERMINAL_STATE;
		}
		for (SimulationState s : negativePreterminalStates) {
			if (pacIsAt(s))
				return REWARD_NEGATIVE_TERMINAL_STATE;
		}
		// All other positions have a reward of 0.0.
		return REWARD_NEUTRAL;
	}

	private boolean pacIsAt(SimulationState s) {
		return pac.getPosition().equals(s.getPosition());
	}

	@Override
	public void addEnvironmentChangedListener(EnvironmentChangeListener listener) {
		envChangedListeners.add(listener);
	}

	@Override
	public void removeEnvironmentChangedListener(EnvironmentChangeListener listener) {
		// We don't need to update Pac because he has the same list
		envChangedListeners.remove(listener);
	}

	private void notifyAboutEpisodeEnded(boolean canceled, int steps) {
		for (EnvironmentChangeListener listener : envChangedListeners) {
			listener.episodeWasFinished(canceled, steps);
		}
	}

	private void notifyAboutStateValueChanged(State currentState) {
		for (EnvironmentChangeListener listener : envChangedListeners) {
			listener.stateValueChanged(currentState);
		}
	}

	public SimulationState getCurrentState() {
		return currentState;
	}

	public Value getValue(int x, int y, Action a) {
		ValueParams params = new ValueParams(getStateForPos(x, y), a);
		ValueFunction val = currentTask.getAgent().getPolicy().getValueFunction();
		return val.getValue(params);
	}

	/**
	 * will send a reward to the agent if the pac reached a preterminal state
	 * with its last action in the maze. the value function of the agent will be
	 * updated accordingly.
	 */
	public void rewardPacForReachingTerminalState() {
		if (isInLearningMode) {
			Reward reward = getRewardForLastExecutedAction();
			MoveAction arbitraryExitAction = MoveAction.GO_NORTH;
			ValueParams params = new ValueParams(currentState, arbitraryExitAction, reward, TERMINAL_STATE);
			currentTask.getAgent().rewardLastAction(params);
		}
		notifyAboutStateValueChanged(currentState);
	}

	@Override
	public void enableLearning(boolean learning) {
		// in simulationmode we are always in learning mode
		isInLearningMode = true;
	}

	@Override
	public boolean isInLearningMode() {
		return isInLearningMode;
	}

	@Override
	public boolean isInTrainingMode() {
		return isInTrainingMode;
	}

	@Override
	public void enableTrainingMode(boolean isInTrainingMode) {
		this.isInTrainingMode = isInTrainingMode;
	}

}
