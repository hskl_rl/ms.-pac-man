package model.environment.simulation;

import java.awt.Point;

import model.reinforcement_learning.State;

/**
 * A Simple State Class for Simulation Environment.
 * A state is equals to a position in a two dimensional
 * coordinate system, e.g. (3,2) is the state with x=3
 * and y=2.
 * */

public class SimulationState extends State {
	
	private Point position;

	public SimulationState(Point position) {
		this.position = position;
	}
	
	public SimulationState(SimulationState stateToCopy) {
		this.position = new Point(stateToCopy.position);
	}
	
	@Override
	public State getCopy() {
		return new SimulationState(this);
	}

	public Point getPosition() {
		return position;
	}

	@Override
	public long getSimpleId() {
		// x into bits 0..31 and y into bits 32..63
		long id = position.x;
		id = id << 32;
		id = id + position.y;
		return id;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return position.x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return position.y;
	}

	@Override
	public int hashCode() {
		return (int) getSimpleId();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (null == obj || obj.getClass() != this.getClass())
			return false;

		SimulationState other = (SimulationState) obj;

		return position.x == other.position.x && position.y == other.position.y;
	}

}
