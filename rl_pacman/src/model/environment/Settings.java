package model.environment;

import java.util.List;

import controller.Controller.ViewFile;
import model.Task;

/**
 * Class for maintaining temporary user settings. It is used to keep and
 * transfer userinput between several controllers. It remembers the last visited
 * view (for proper behaviour of the back button), the current reinforcement
 * learning task (+ a temporary reinforcement learning task, see comment there
 * for more information) and the user decision regarding manual game (if the
 * user starts a manual game in the menu) or game with reinforcement learning.
 */
public class Settings {

	/**
	 * Variable to keep track of the previous shown view. Used to determine from
	 * which screen the user came when entering another controller.
	 */
	private ViewFile lastView = ViewFile.MAINMENU_VIEW;

	/**
	 * Used to track whether the user is currently within the manual game mode
	 * or if he is in the auto play mode.
	 */
	private boolean manualGame = true;

	/**
	 * Keeps track of the current reinforcement learning task chosen by the user
	 * via the TaskOverview screen. This is used, e.g. to access the selected
	 * task from the game screen.
	 */
	private Task rlTask;

	/**
	 * Used to keep track of several rlTasks which have been simultaneously
	 * selected by the user via the TaskOverview screen. This is used, e.g. for
	 * comparing several tasks in the statistics screen.
	 */
	private List<Task> selectedTasks;

	/**
	 * This task is used only used temporary while the game is being played.
	 * With this temporary task, changes like ghostselection or current maze
	 * etc. stay temporary while playing and do not affect the original
	 * (persisted) task when applying these temporary changes withing the
	 * gameview.
	 */
	private Task tempTask;

	/**
	 * @return true if the user is playing a manual game without reinforcement
	 *         learning, false otherwise.
	 */
	public boolean isManualGame() {
		return manualGame;
	}

	public void setManualGame(boolean manualGame) {
		this.manualGame = manualGame;
	}

	public Task getRlTask() {
		return rlTask;
	}

	/**
	 * Creates a copy of the current rlTask and returns it. Both tasks (rlTask
	 * and temporary copy) share the same statistic objects. Thus, we can still
	 * update the statistics of the original (persisted) task although we are
	 * actually playing with the temporary task. See comments on tempTask for
	 * more information.
	 */
	public Task getTempTask() {
		if (null == tempTask && null != rlTask) {
			tempTask = new Task(rlTask);

			// when playing with the tempTask, statistics of the original task
			// should still be updated
			tempTask.setStatisticsPerTrainingLevel(rlTask.getStatisticsPerTrainingLevel());
		}

		return tempTask;
	}

	/**
	 * sets the current reinforcement learning task chosen and configured by the
	 * user
	 */
	public void setRlTask(Task rlTask) {
		this.rlTask = rlTask;
		this.tempTask = null;
	}

	/**
	 * @return the last accessed view. in other words: the view that the user
	 *         came from before landing in the current view.
	 */
	public ViewFile getLastView() {
		return lastView;
	}

	public void setLastView(ViewFile lastView) {
		this.lastView = lastView;
	}

	/**
	 * returns a multi-selection of tasks set by the user in taskoverview.
	 */
	public List<Task> getSelectedTasks() {
		return selectedTasks;
	}

	public void setSelectedTasks(List<Task> selectedTasks) {
		this.selectedTasks = selectedTasks;
	}

	/**
	 * overwrites (persists) the current rlTask.
	 */
	public void saveTask() {
		if (null != rlTask)
			rlTask.overwrite();
	}

}
