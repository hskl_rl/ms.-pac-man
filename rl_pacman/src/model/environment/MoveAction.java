package model.environment;


import model.reinforcement_learning.Action;

/**
 * Enum which defines possible move actions.
 * Move actions can be taken by movable objects of
 * the game environment.
 */
public enum MoveAction implements Action{
	GO_NORTH, GO_EAST, GO_SOUTH, GO_WEST, DO_NOTHING;

	@Override
	public long getSimpleId() {
		return this.ordinal();
	}
	
	public static MoveAction getFrom(Action action) {
		return (MoveAction) action;
	}

	@Override
	public Action getCopy() {
		return this;
	}
	
}
	
