/**
 * 
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import controller.StatisticsListener;
import model.environment.EnvironmentChangeListener;
import model.environment.pacman.Dot;
import model.environment.pacman.Ghost;
import model.environment.pacman.MovableObject;
import model.reinforcement_learning.State;

/**
 * This class saves some metrics about played games for an agent with
 * a fixed number of trained episodes. 
 */
public class Statistics implements EnvironmentChangeListener, Serializable{

	private static final long serialVersionUID = 896401336890765246L;
	
	//some objects that want to get information if the data of this
	//statistic object gets changed
	private transient List<StatisticsListener> statisticsListener;
	
	public void addStatisticsListener(StatisticsListener listener){
		
		if(listener == null)
			return;
		
		if(this.statisticsListener == null){
			this.statisticsListener = new ArrayList<StatisticsListener>();
		}
		this.statisticsListener.add(listener);
	}
	
	public void removeStatisticsListener(StatisticsListener listener){
		if(listener == null || this.statisticsListener == null)
			return;
		
		this.statisticsListener.remove(listener);
	}
	
	private void notifiyStatisticsListener(){
		if(this.statisticsListener == null)
			return;
		for(StatisticsListener l : this.statisticsListener){
			l.statisticsChanged();
		}
	}
	
	
	public final int NR_TRAINED_EPISODES; //we have one statistics object per traininglevel of the task
	
	private int playedGames; //num of played games at this episode level
	private int wonGames; //num of won games at this episode level
	private int caughtsByGhosts; //num of times pac has been caught
	
	private List<Integer> reachedScores = new ArrayList<>();
	// Calculate values permanently. Avoids concurrent modification exceptions
	private int minReachedScores = Integer.MAX_VALUE;
	private int maxReachedScores = 0;
	private long sumOfReachedScores = 0;
	private void updateReachedScoreStatistics(int score) {
		if (score < minReachedScores)
			minReachedScores = score;
		if (score > maxReachedScores)
			maxReachedScores = score;
		sumOfReachedScores += score;
	}
	
	public Statistics(int trainedEpisodes) {
		NR_TRAINED_EPISODES = trainedEpisodes;
	}
	
	public Statistics(Statistics statToCopy){
		this.NR_TRAINED_EPISODES = statToCopy.NR_TRAINED_EPISODES;
		this.playedGames = statToCopy.playedGames;
		this.wonGames = statToCopy.wonGames;
		this.caughtsByGhosts = statToCopy.caughtsByGhosts;
		for(Integer score : reachedScores){
			this.reachedScores.add(score);
			updateReachedScoreStatistics(score);
		}
	}
	
	public void wonAGame() {
		wonGames++;
		playedGames++;
		notifiyStatisticsListener();
	}
	
	public int getWonGames() {
		return wonGames;
	}
	
	public void lostAGame() {
		playedGames++;
		notifiyStatisticsListener();
	}
	
	public int getLostGames() {
		return playedGames - wonGames;
	}
	
	public int getPlayedGames() {
		return playedGames;
	}
	
	public void pacWasCaught() {
		caughtsByGhosts++;
		notifiyStatisticsListener();
	}
	
	public int getNumberOfGhostCaughts() {
		return caughtsByGhosts;
	}
	
	public double getAverageScore(){
		if (0 == reachedScores.size())
			return 0;
		return ((double) sumOfReachedScores) / reachedScores.size(); 
	}
	
	public double getMinScore(){
		return playedGames <= 0 ? 0 : minReachedScores;
	}
	
	public double getMaxScore(){
		return maxReachedScores;
	}	

	@Override
	public void dotWasEaten(Dot eatenDot) {
		notifiyStatisticsListener();
	}

	@Override
	public void lastDotWasEaten(Dot eatenDot, Integer score) {	
		this.wonGames++;
		this.playedGames++;
		reachedScores.add(score);
		updateReachedScoreStatistics(score);
		notifiyStatisticsListener();		
	}

	@Override
	public void pacWasCaughtBy(Ghost ghost) {
		this.caughtsByGhosts++;
		notifiyStatisticsListener();	
	}

	@Override
	public void objectMoved(MovableObject movableObject, boolean hasNewPosition, boolean isTeleport) {
		// nothing to do here		
	}

	@Override
	public void stateValueChanged(State state) {
		// nothing to do here
	}

	@Override
	public void episodeWasFinished(boolean canceled, int steps) {
		// nothing to do here (used for training in simulation)
	}

	@Override
	public void gameOver(Integer score) {
		this.playedGames++;
		reachedScores.add(score);
		updateReachedScoreStatistics(score);
		notifiyStatisticsListener();	
	}

	@Override
	public void populatedDots() {
		// nothing to do here
	}

}
