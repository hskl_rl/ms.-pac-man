package utils;

import java.awt.Point;

/**
 * Collection of mathematic helper functions used within the project
 */
public class MyMath {
	
	private static final double EPSILON = 1e-7;

	/**
	 * Checks whether the given floating point number equals to zero within a certain range epsilon.
	 */
	public static boolean almostEqualsToZero(double a) {
		return almostEquals(a, 0.0);
	}
	
	/**
	 * Checks whether two given floating point numbers equal within a certain range epsilon.
	 */
	public static boolean almostEquals(double a, double b) {
		return Math.abs(a-b) < EPSILON;
	}

	/**
	 * Calculates the distance between 2D points and returns the result as double
	 */
	public static double distanceBetween(Point position, Point position2) {
		Point distanceVector = new Point(
				position2.x -position.x,
				position2.y -position.y);
		double distance = Math.sqrt(distanceVector.x * distanceVector.x + distanceVector.y * distanceVector.y);
		return distance;
	}
	
	/**
	 * Returns the angle between the horizontal vector (1,0) and the given directionVector in radians.
	 * It returns always the angle taken from the horizontal vector and counter clockwise direction.
	 * Caution our coordinate source is in the upper left corner. (y axis is upside down to canonical 
	 * coordinate system)
	 * So given directionVector (0,1) will result in 3*PI/2
	 * directionVector (-1,0) yields PI
	 * directionVector (0,-1) yields PI/2
	 * @param directionVector
	 * @return
	 */
	public static double getAngleToHorizontalLine(Point directionVector) {
		if ( almostEqualsToZero(directionVector.y) ) // equals 0
			if (directionVector.x > 0)
				return 0.0;
			else
				return Math.PI;
		
		// <a|b> = |a|*|b|*cos(\alpha)
		// --> \alpha = cos^{-1}(<a|b> / (|a|*|b|))
		
		// a = (1,0)
		double x = 1.0;
		double y = 0.0;
		// b = directionVector
		double scalarProd = x * directionVector.x + y * directionVector.y;
		
		// |a| = 1.0
		double normB = Math.sqrt(directionVector.x * directionVector.x + directionVector.y * directionVector.y);
		
		double angleBetweenZeroAndPi = Math.acos(scalarProd / normB);
		
		if ( directionVector.y < 0 )
			return angleBetweenZeroAndPi;  // Note the point (0,0) is in the upper left corner
		else
			return angleBetweenZeroAndPi + Math.PI;
	}
	
}
