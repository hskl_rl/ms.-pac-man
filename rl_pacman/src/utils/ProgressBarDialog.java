package utils;

import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;

/**
 * A custom JavaFX Dialog class for displaying progress information to the user.
 * Is used for displaying the progress while training or executing episodes with
 * an reinforcement learning agent. See
 * http://code.makery.ch/blog/javafx-dialogs-official/ for more about javafx
 * dialogs
 * */
public class ProgressBarDialog extends Alert {

	private double progress;
	private ProgressBar bar;
	private ProgressIndicator indicator;

	/**
	 * Creates a custom JavaFX Dialog with a ProgressBar, ProgressIndicator
	 * (will display the percent number of the progress) and a cancel button.
	 * The bar and indicator can be bound to a ReadOnlyDoubleProperty by using
	 * the bindTo() Method for easy handling of the current progress.
	 * */
	public ProgressBarDialog() {

		super(AlertType.INFORMATION);
		this.setTitle("Progress Dialog");
		this.setHeaderText(null);

		Label lbl = new Label();
		lbl.setText("Training Progress:");

		bar = new ProgressBar(0.0);
		indicator = new ProgressIndicator(0.0);

		HBox box = new HBox();
		box.setSpacing(5);
		box.setAlignment(Pos.CENTER);
		box.getChildren().addAll(lbl, bar, indicator);

		// create a cancel button
		ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		this.getButtonTypes().setAll(buttonTypeCancel);
		this.initModality(Modality.APPLICATION_MODAL);

		this.getDialogPane().setContent(box);

	}

	/**
	 * Sets the current progress on the bar and indicator
	 * 
	 * @param progress
	 *            a double value between 0.0 and 1.0
	 * */
	public void setProgress(double progress) {

		if (progress < 0.0)
			progress = 0.0;

		if (progress > 1.0)
			progress = 1.0;

		this.progress = progress;
		bar.setProgress(this.progress);
		indicator.setProgress(this.progress);
	}

	/** returns the current progress displayed in the dialog */
	public double getProgress() {
		return this.progress;
	}

	/**
	 * binds the bar and indicator progress property a ReadOnlyDoubleProperty.
	 * therefore if the ReadOnlyDoubleProperty is changed the bar and indicator
	 * of the dialog will be updated accordingly.
	 */
	public void bindTo(ReadOnlyDoubleProperty progressProperty) {
		bar.progressProperty().bind(progressProperty);
		indicator.progressProperty().bind(progressProperty);
	}

}
