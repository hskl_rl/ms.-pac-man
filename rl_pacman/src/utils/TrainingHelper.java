package utils;

import model.Task;
import model.reinforcement_learning.Environment;
import model.reinforcement_learning.training.Training;
import model.reinforcement_learning.training.TrainingListener;

/**
 * This helperclass is used to train a given number of episodes in a seperate
 * thread and provide a progress dialog at the same time. The progressdialog can
 * be accessed from outside class and can be used to display the progress of the
 * training to the user.
 * 
 * The TrainingHelper can be passed to a new Thread to be executed. On
 * thread.start() the TrainingHelper call() method will be invoked.
 */
public class TrainingHelper extends javafx.concurrent.Task<Void> {

	/**
	 * The environment where the training should be performed in
	 */
	private final Environment env;

	/**
	 * The training object which manages the actual training
	 */
	private final Training training;

	/**
	 * the amount of episodes to be trained when running the task
	 */
	private final int nrOfEpisodesToTrain;

	/**
	 * the progressdialog which is used to show progress of the training to the
	 * user
	 */
	private final ProgressBarDialog progressDialog;

	/**
	 * Instantiates a TrainingHelper object.
	 * 
	 * @param task
	 *            The task to train and to save after training
	 * @param env
	 *            The environment where the training should be performed in
	 * @param nrOfEpisodes
	 *            the amount of episodes that shall be performed when running
	 *            the task
	 * @param listener
	 *            a listener that will be notified about certain training events
	 *            like trainingFinished
	 */
	public TrainingHelper(Task task, Environment env, int nrOfEpisodesToTrain, TrainingListener listener) {
		this.env = env;
		training = new Training(env);
		// we will get notified if training was finished
		training.addTrainingListener(listener);
		this.nrOfEpisodesToTrain = nrOfEpisodesToTrain;

		// Set up dialog
		progressDialog = new ProgressBarDialog();
		progressDialog.bindTo(this.progressProperty());
	}

	public Training getTraining() {
		return training;
	}

	public Environment getEnvironment() {
		return env;
	}

	public ProgressBarDialog getProgressBarDialog() {
		return progressDialog;
	}

	/**
	 * updates the progress of the progress dialog
	 * @param percent
	 */
	public void updateProgress(int percent) {
		updateProgress(percent, 100);
	}

	/**
	 * Performs the training and ends the thread when finished
	 */
	@Override
	public Void call() {
		try {
			training.train(nrOfEpisodesToTrain);
		} catch (final Throwable th) {
			th.printStackTrace();
		}

		return null;

	}

}
