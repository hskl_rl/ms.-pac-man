/**
 * 
 */
package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import model.environment.pacman.Maze;

/**
 * This class is used to convert PNM-Files into the Maze-Datastructure (based on
 * the TableLoader class from MASON) and is responsible for managing the loaded
 * maze objects. It is implemented as singleton and contains maps that store
 * already loaded mazes. The mazes are loaded on demand (lazy) by checking if
 * the desired maze with the given name is already part of the map or not.
 */
public class MazeLoader extends TableLoader {

	private final static Logger log = Logger.getLogger(MazeLoader.class.getName());

	// path for pacman mazes (normal game and RL)
	private static final String MAZE_DIR = System.getProperty("user.dir") + File.separator + "mazes" + File.separator;
	
	// path for simulation mazes (used in simulation mode)
	private static final String MAZE_SIM_DIR = MAZE_DIR + "simulation" + File.separator;

	private final static String MAZE_FILE_EXT = ".pbm";
	private final static FilenameFilter MAZE_FILE_FILTER;
	
	/**
	 * the one and only instance of the MazeLoader class
	 */
	private static MazeLoader instance;

	static {
		// init extension filter
		MAZE_FILE_FILTER = new FilenameFilter() {
			public boolean accept(File dir, String filename) {
				return filename.endsWith(MAZE_FILE_EXT);
			}
		};
	}

	/**
	 * Typical getter method of the singleton pattern. Returns the one and only
	 * instance of the MazeLoader class.
	 */
	public static synchronized MazeLoader getInstance() {
		if (null == instance)
			instance = new MazeLoader();

		return instance;
	}

	/**
	 * Hashmap that stores already loaded pacman mazes for manual and rl game
	 * mode. Thus mazefiles will only be loaded once (on demand) <br/>
	 * <br/>
	 * Mapping: mazename --> mazeobject
	 */
	private HashMap<String, Maze> pacMazes;

	/**
	 * Hashmap that stores already loaded simulation mazes for simulation mode.
	 * Thus mazefiles will only be loaded once (on demand) <br/>
	 * <br/>
	 * Mapping: mazename --> mazeobject
	 */
	private HashMap<String, Maze> simMazes;

	/**
	 * Private constructor to prevent initialization from outside class
	 */
	private MazeLoader() {
		pacMazes = loadMazeNames(MAZE_DIR);
		simMazes = loadMazeNames(MAZE_SIM_DIR);
	}

	/**
	 * returns the pacman maze belonging to the given mazename. loads it from
	 * file, if not yet done (lazy).
	 */
	public Maze getPacMaze(String mazeName) {
		return getMaze(mazeName, MAZE_DIR, pacMazes);
	}
	
	/**
	 * returns the simulation maze belonging to the given mazename. loads it from
	 * file, if not yet done (lazy).
	 */
	public Maze getSimulationMaze(String mazeName){
		return getMaze(mazeName, MAZE_SIM_DIR, simMazes);
	}
	
	/**
	 * returns the maze object belonging to the given mazename. loads it from
	 * file, if not yet stored in the given map (lazy).
	 */
	private Maze getMaze(String mazeName, String mazeDirectoryPath, HashMap<String, Maze> mazesMap){
		if (null == mazesMap.get(mazeName)) {
			Maze m = loadMaze(mazeName, mazeDirectoryPath);
			mazesMap.put(mazeName, m);
		}
		return mazesMap.get(mazeName);
	}

	/**
	 * reads the maze file belonging to the given mazename (filename)
	 * and converts it to a maze object.
	 * @return
	 * 	the created maze object.
	 */
	private Maze loadMaze(String mazeName, String mazeDirectoryPath) {
		String filePath = mazeDirectoryPath + mazeName + MAZE_FILE_EXT;

		// try to load the maze
		int[][] grid = null;
		try {
			InputStream is = new FileInputStream(filePath);
			grid = loadPNMFile(is);
		} catch (Exception e) {
			throw new RuntimeException("Could not load file: " + filePath);
		}

		Maze maze = new Maze(grid);

		log.info("Loaded maze: " + mazeName + " \n" + maze.toString());

		return maze;
	}

	/**
	 * Loads file names from maze directory and puts them as keyset into
	 * a new HashMap<String, Maze> which mazes are instantiated with null.
	 * @return
	 * 	the new generated hashmap
	 */
	private HashMap<String, Maze> loadMazeNames(String mazeDirectoryPath) {

		// get all maze files from directory
		File dir = new File(mazeDirectoryPath);
		File[] mazeFiles = dir.listFiles(MAZE_FILE_FILTER);

		HashMap<String, Maze> mazes = new HashMap<String, Maze>();

		if (null != mazeFiles) {
			for (File f : mazeFiles) {

				// remove file extension
				String name = f.getName();
				int pos = name.lastIndexOf(".");
				if (pos > 0) {
					name = name.substring(0, pos);
				}

				// put mazename to keyset
				mazes.put(name, null);
			}
		} else {
			throw new RuntimeException("No MazeFiles were found in the MazeDirectory: " + mazeDirectoryPath);
		}

		return mazes;
	}
	
	public List<String> getSimulationMazeNames(){
		return new ArrayList<String>(simMazes.keySet());
	}

	public List<String> getPacMazeNames() {
		return new ArrayList<String>(pacMazes.keySet());
	}

	public String getFirstPacMazeName() {
		return pacMazes.keySet().iterator().next();
	}
	
	public String getFirstSimMazeName() {
		return simMazes.keySet().iterator().next();
	}
}
