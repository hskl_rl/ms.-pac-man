/**
 * 
 */
package utils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import controller.Controller;
import model.Task;

/**
 * Singleton which is responsible for (de)serializing Task objects. It is also
 * responsible for managing the loaded task objects. It contains a map that
 * stores already loaded tasks. The tasks are loaded on demand (lazy) by
 * checking if the desired task with the given taskname is already part of the
 * map or not.
 */
public class TaskPersistenceManager {

	protected final static Logger log = Logger.getLogger(Controller.class.getName());
	private static final String TASK_DIR = System.getProperty("user.dir") + File.separator + "tasks" + File.separator;
	private static final String TASK_FILE_EXT = ".task";
	private static final FilenameFilter TASK_FILE_FILTER;

	/**
	 * the one and only instance of the TaskPersistenceManager class
	 */
	private static TaskPersistenceManager instance;

	static {
		// init file extension filter
		TASK_FILE_FILTER = new FilenameFilter() {
			public boolean accept(File dir, String filename) {
				return filename.endsWith(TASK_FILE_EXT);
			}
		};
	}

	/**
	 * Typical getter method of the singleton pattern. Returns the one and only
	 * instance of the TaskPersistenceManager class.
	 */
	public static synchronized TaskPersistenceManager getInstance() {
		if (null == instance)
			instance = new TaskPersistenceManager();
		return instance;
	}

	/**
	 * Hashmap that stores already loaded tasks. Thus taskfiles will only be
	 * loaded once (on demand) <br/>
	 * <br/>
	 * Mapping: taskname --> taskobject
	 */
	private HashMap<String, Task> tasks;

	/**
	 * Private constructor to prevent initialization from outside class
	 */
	private TaskPersistenceManager() {
		tasks = new HashMap<String, Task>();
		loadTaskNames();
	}

	/**
	 * Returns the names of all available tasks as ArrayList<String>
	 */
	public List<String> getTaskNames() {
		return new ArrayList<String>(tasks.keySet());
	}

	/**
	 * Returns the task object which belongs to the given name. If the map does
	 * not yet contain the task, it tries to load it from file (lazy).
	 * 
	 * @param taskName
	 * @return taskObject which belongs to the given taskName
	 */
	public Task getTask(String taskName) {
		if (null == tasks.get(taskName))
			loadTask(taskName);

		return tasks.get(taskName);
	}

	/*
	 * stores or updates the task in the map and writes the given task to file.
	 * alters the name of the task if a task with the same name already exists.
	 */
	public void saveTask(Task task) {
		// prevent overwriting
		int i = 2;
		String originalName = task.getName();
		while (tasks.containsKey(task.getName()))
			task.setName(originalName + " (" + (i++) + ")");

		// save task
		tasks.put(task.getName(), task);
		serializeTask(task);
	}

	/**
	 * Deletes the task file of the given task and creates a new one under the
	 * (eventually new) name of the task.
	 */
	public void overwriteTask(Task task) {
		if (tasks.containsValue(task)) {
			deleteTask(task);
		}

		saveTask(task);
	}

	/**
	 * removes the given task from map and deletes its file
	 */
	public void deleteTask(Task task) {
		tasks.remove(task.getName());
		deleteTaskFile(task.getName());
	}

	/**
	 * Loads file names from task directory and puts them to the keyset of the
	 * task map
	 */
	private void loadTaskNames() {
		createTaskDirIfNotExists();

		// get all task files from directory
		File dir = new File(TASK_DIR);
		File[] taskFiles = dir.listFiles(TASK_FILE_FILTER);

		if (null != taskFiles) {
			for (File f : taskFiles) {

				// remove file extension
				String name = f.getName();
				int pos = name.lastIndexOf(".");
				if (pos > 0) {
					name = name.substring(0, pos);
				}

				// put taskname to keyset
				tasks.put(name, null);
			}
		}
	}

	/**
	 * deserializes the taskfile with the given name and puts it to map
	 */
	private void loadTask(String taskName) {
		Task t1 = deserializeTask(taskName);
		t1.setName(taskName);
		tasks.put(taskName, t1);
	}

	/**
	 * deserializes the taskfile with the given name to a task object and
	 * returns it
	 */
	private Task deserializeTask(String taskName) {
		createTaskDirIfNotExists();

		FileInputStream fileIn = null;
		ObjectInputStream objectIn = null;
		Task t = null;
		try {
			String filePath = TASK_DIR + taskName + TASK_FILE_EXT;
			fileIn = new FileInputStream(filePath);
			objectIn = new ObjectInputStream(fileIn);
			t = (Task) objectIn.readObject();
			log.info("Deserialized " + filePath);
		} catch (IOException i) {
			i.printStackTrace();
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
		} finally {
			if (null != fileIn)
				try {
					fileIn.close();
				} catch (IOException e) {
					log.warning(e.toString());
				}
			if (null != objectIn)
				try {
					objectIn.close();
				} catch (IOException e) {
					log.warning(e.toString());
				}
		}

		return t;
	}

	/**
	 * serializes the given task to file. the filename of the generated file
	 * equals the name of the task
	 */
	private void serializeTask(Task task) {
		createTaskDirIfNotExists();

		FileOutputStream fileOut = null;
		ObjectOutputStream objectOut = null;

		try {
			String filePath = TASK_DIR + task.getName() + TASK_FILE_EXT;
			fileOut = new FileOutputStream(filePath);
			objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(task);
			log.info("Serialized task to " + filePath);
		} catch (IOException i) {
			i.printStackTrace();
		} finally {
			if (null != fileOut)
				try {
					fileOut.close();
				} catch (IOException e) {
					log.warning(e.toString());
				}
			if (null != objectOut)
				try {
					objectOut.close();
				} catch (IOException e) {
					log.warning(e.toString());
				}
		}
	}

	/**
	 * deletes the taskfile found under the given taskname
	 */
	private void deleteTaskFile(String taskName) {
		if (!taskDirectoryExists())
			return;

		String filePath = TASK_DIR + taskName + TASK_FILE_EXT;
		File file = new File(filePath);
		if (file.exists()) {
			boolean deleted = file.delete();
			if (deleted)
				log.info("Deleted taskfile " + filePath);
			else
				log.warning("Could not delete taskfile " + filePath);
		}
	}

	/**
	 * creates the task file directory if it doesn't exist
	 */
	private void createTaskDirIfNotExists() {
		File dir = new File(TASK_DIR);
		if (dir.isDirectory())
			return;

		boolean created = dir.mkdir();
		if (!created) {
			log.warning("Could not create task file directory " + dir.getAbsolutePath());
		}
	}

	/**
	 * checks whether the task file directory exists
	 */
	private boolean taskDirectoryExists() {
		File dir = new File(TASK_DIR);
		return dir.isDirectory();
	}
}
