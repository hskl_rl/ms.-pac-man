package utils;

import static org.junit.Assert.*;

import java.awt.Point;

import org.junit.Test;

/**
 * This class serves as unit test for the MyMath class.
 * It tests whether the getAngleToHorizontalLine() method returns the expected result.
 */
public class MyMathUnitTest {

	@Test
	public void testAngleToHorizontalLine() {
		Point p = new Point(0, 1);
		double expectedAngle = 3*Math.PI / 2;  // Note y-axis is upside down. (0,0) is in the upper left corner
		
		assertTrue(
				MyMath.almostEquals(expectedAngle, MyMath.getAngleToHorizontalLine(p) )
				   );
		
		p = new Point(-1, 0);
		expectedAngle = Math.PI; 
		
		assertTrue(
				MyMath.almostEquals(expectedAngle, MyMath.getAngleToHorizontalLine(p) )
				   );
		
		p = new Point(0, -1);
		expectedAngle = Math.PI / 2;
		
		assertTrue(
				MyMath.almostEquals(expectedAngle, MyMath.getAngleToHorizontalLine(p) )
				   );
	}

}
