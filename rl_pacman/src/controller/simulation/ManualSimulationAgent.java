package controller.simulation;

import java.util.List;

import model.reinforcement_learning.Action;
import model.reinforcement_learning.Agent;
import model.reinforcement_learning.State;

public class ManualSimulationAgent extends Agent {
	
	private Action nextAction;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8618812820212528748L;

	public ManualSimulationAgent(Agent agent) {
		super(agent.getPolicy());
	}
	
	public void setNext(Action action) {
		nextAction = action;
	}

	@Override
	public Action determineNextAction(State state, List<Action> possibleActions) {
		// This also saves possible actions for rewarding and updating transition model
		Action actionFromAgent = super.determineNextAction(state, possibleActions);
		if (null == nextAction)
			return actionFromAgent;
		else
			return nextAction;
	}
	
	
	
}
