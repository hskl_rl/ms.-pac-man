package controller.timing;

public interface TimerFirable {

	void fire();

}
