package controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import model.Task;
import model.environment.pacman.Maze;
import model.environment.pacman.PacEnvironment.DotMode;
import model.reinforcement_learning.Agent;
import model.reinforcement_learning.AgentFactory;
import model.reinforcement_learning.policy.ActionSelection;
import model.reinforcement_learning.policy.EpsGreedyActionSelection;
import model.reinforcement_learning.policy.Policy;
import model.reinforcement_learning.policy.SoftMaxActionSelection;
import model.reinforcement_learning.value_function.ApproximateQLearning;
import model.reinforcement_learning.value_function.ValueFunction;
import utils.TextfieldConverterHelper;
import utils.MazeLoader;

/**
 * Controller for the task configuration in pac-man reinforcement learning
 * program. Allows the user the create a totally customized reinforcement
 * learning task with all kind of parameters like information about the
 * environment (maze, ghosts, rewards, penalties) and training algorithms
 * including the action selection type.
 * */
public class TaskConfigurationController extends Controller implements Initializable {

	protected final static Logger log = Logger.getLogger(TaskConfigurationController.class.getName());

	// javafx ui elements. the names refer the to id in the javafx fxml file.
	@FXML
	private CheckBox cbClyde, cbPinky, cbBlinky, cbInky; // ghosts

	@FXML
	public ComboBox<String> combbTrainingMethod, combbActionSelection, combbMaze, combbDots;

	@FXML
	private AnchorPane mazePreviewHolder;

	// Rewards & Penalties

	@FXML
	public TextField tfTaskName, tfDeathPenalty, tfStepPenalty, tfWonReward, tfDotReward, tfWallHitPenalty;

	@FXML
	public TextField tfEpsilon, tfAlpha, tfGamma, tfEpisodes;

	@FXML
	public Label lbEpsilon;
	
	/** contains all other task related ui elements. */
	@FXML
	private GridPane taskGrid;

	/** UI Element for the small maze preview, created on runtime */
	private GridPane gpMazePreview;

	/**
	 * remember the user input. will be saved in settings if the user hits the
	 * save button.
	 */
	private Task currentTask;

	/**
	 * specifies whether or not we are in edit mode. is true if we are currently
	 * editing a task. only a few parameters like epsilon, alpha, ... can be
	 * changed in edit mode.
	 */
	private boolean editOnly = false;

	/**
	 * is called while initialising the view. it would be possible to add
	 * dynamically created ui elements here before displaying anything to the
	 * user. in case of this view we need our settings object, but the settings
	 * object is not accessible in initialize because creating the view is not
	 * completed. therefore we use the afterInit method, which is called right
	 * after initialisation of the view has finished.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// settings are not accessible here
	}

	/**
	 * is called right after initialisation of the view has finished. We use the
	 * settings object to create a new task or fetch an existing one and
	 * initialise the mode of the view (editmode if we received an existing task
	 * or not otherwise). The method fills all ui elements and loads a first
	 * maze preview.
	 * */
	@Override
	public void afterInit() {

		assert (settings != null);

		if (settings.getRlTask() == null) {
			currentTask = new Task();
		} else {
			currentTask = settings.getRlTask();
			disableFields();
			editOnly = true;
		}

		disableOptionalUIElements(currentTask.getAgent().getPolicy().getActionSelectionMethod().getName());

		fillAllTextFields();

		initComboBoxes();
		initCheckBoxes();

		// show maze preview
		loadMazePreview(currentTask.getMazeName());
	}

	/**
	 * Disables some optional ui elements depending on the current action
	 * selection name. For example, the field epsilon is only enabled if the
	 * current action selections uses this parameter.
	 * 
	 * @param actionSelectionName
	 *            the string read from the Combobox for ActionSelection
	 * */
	private void disableOptionalUIElements(String actionSelectionName) {
		boolean epsDisabled = (actionSelectionName.equals(ActionSelection.GREEDY));
		
		// TODO add condition if new policies are added
		boolean alphaDisabled = false; 
		boolean deltaDisabled = false;

		// enable / disable the necessary fields in UI
		tfEpsilon.setDisable(epsDisabled);
		tfAlpha.setDisable(alphaDisabled);
		tfGamma.setDisable(deltaDisabled);
	}

	/**
	 * Fills the textfields of the ui with the values from currentTask object.
	 * */
	private void fillAllTextFields() {

		cbClyde.setSelected(currentTask.isClydeSelected());
		cbPinky.setSelected(currentTask.isPinkySelected());
		cbBlinky.setSelected(currentTask.isBlinkySelected());
		cbInky.setSelected(currentTask.isInkySelected());

		if (currentTask.getName() != null)
			tfTaskName.setText(currentTask.getName());

		tfDeathPenalty.setText(Double.toString(currentTask.getDeathPenalty().getValue()));
		tfStepPenalty.setText(Double.toString(currentTask.getStepPenalty().getValue()));
		tfWonReward.setText(Double.toString(currentTask.getWonReward().getValue()));
		tfDotReward.setText(Double.toString(currentTask.getDotReward().getValue()));		
		tfWallHitPenalty.setText(Double.toString(currentTask.getWallHitPenalty().getValue()));

		if (currentTask.getAgent().getPolicy() != null) {

			Policy currentPolicy = currentTask.getAgent().getPolicy();

			if (currentPolicy.getValueFunction() != null) {

				// Default Values for Algorithms which doesn't use this fields
				// at all
				double discountRate = 0.0;
				double learningRate = 0.0;

				ValueFunction currentValueFunction = currentPolicy.getValueFunction();

				// TODO add new algorithms here if necessary
				if (currentValueFunction instanceof ApproximateQLearning) {
					ApproximateQLearning approvQVf = (ApproximateQLearning) currentValueFunction;
					discountRate = approvQVf.getDiscountRate();
					learningRate = approvQVf.getLearningRate();
				}

				tfGamma.setText(Double.toString(discountRate));
				tfAlpha.setText(Double.toString(learningRate));
			}

			if (currentPolicy.getActionSelectionMethod() != null) {

				double epsilon = 0.2;

				ActionSelection as = currentPolicy.getActionSelectionMethod();

				// TODO add new Action Selection Methods here if necessary
				if (as instanceof EpsGreedyActionSelection) {
					epsilon = ((EpsGreedyActionSelection) as).getEpsilon();
				} else if (as instanceof SoftMaxActionSelection) {
					epsilon = ((SoftMaxActionSelection) as).getTemp();
				}

				tfEpsilon.setText(Double.toString(epsilon));
			}
		}
	}

	/**
	 * Initialises all comboboxes depending on the settings in the currentTask
	 * and adds listeners to the ui elements
	 * */
	private void initComboBoxes() {

		// MAZE COMBOBOX
		ObservableList<String> mazeNames = FXCollections.observableArrayList(MazeLoader.getInstance().getPacMazeNames());
		combbMaze.setItems(mazeNames);

		if (currentTask.getMazeName() != null)
			combbMaze.getSelectionModel().select(currentTask.getMazeName());
		combbMaze.getSelectionModel().selectedItemProperty().addListener(new MazeChangedListener());

		// DOT MODE COMBOBOX
		List<String> helperList = new ArrayList<String>();
		ObservableList<String> dotmodes = FXCollections.observableList(helperList);
		for (DotMode dm : DotMode.ALL_DOT_MODES) {
			dotmodes.add(dm.getLabel());
		}
		combbDots.setItems(dotmodes);

		if (currentTask.getDotMode() != null)
			combbDots.getSelectionModel().select(currentTask.getDotMode().getLabel());

		// VALUE FUNCTION COMBOBOX
		ObservableList<String> valueFunctionNames = FXCollections.observableArrayList(ValueFunction.approximateValueFunctions);
		combbTrainingMethod.setItems(valueFunctionNames);

		if (currentTask.getAgent() != null && currentTask.getAgent().getPolicy() != null
				&& currentTask.getAgent().getPolicy().getValueFunction() != null)
			combbTrainingMethod.getSelectionModel().select(ValueFunction.approximateValueFunctions.get(0));

		// POLICY COMBOBOX
		ObservableList<String> actionSelectionNames = FXCollections.observableArrayList(ActionSelection.ACTION_SELECTION_METHODS);
		combbActionSelection.setItems(actionSelectionNames);
		combbActionSelection.getSelectionModel().selectedItemProperty().addListener(new ActionSelectionNameChangedListener());

		if (currentTask.getAgent() != null && currentTask.getAgent().getPolicy() != null) {
			String actionSelectionName = currentTask.getAgent().getPolicy().getActionSelectionMethod().getName();
			combbActionSelection.getSelectionModel().select(actionSelectionName);
		}

	}

	/**
	 * Listener for the combobox with maze selection. Removes the old maze and
	 * loads the new one if the value of the combobox changed.
	 */
	private class MazeChangedListener implements ChangeListener<String> {
		@Override
		public void changed(ObservableValue<? extends String> observable, String oldMazeName, String newMazeName) {
			if (null != newMazeName) {
				unsetMazePreview();
				loadMazePreview(newMazeName);
			}
		}
	}

	/**
	 * Listener for the combobox with action selection method. Disables or
	 * enabled the optional ui elements with parameters for the action selection
	 * methods if necessary.
	 */
	private class ActionSelectionNameChangedListener implements ChangeListener<String> {
		@Override
		public void changed(ObservableValue<? extends String> observable, String oldActionSelectionName, String newActionSelectionName) {
			if (null != newActionSelectionName) {
				if(newActionSelectionName.equals(ActionSelection.SOFTMAX))
					lbEpsilon.setText("Temperature");
				else
					lbEpsilon.setText("Epsilon");
				
				disableOptionalUIElements(newActionSelectionName);
			}
		}
	}

	/**
	 * Disables all ui elements which are not necessary for edit mode.
	 * */
	private void disableFields() {
		cbClyde.setDisable(true);
		cbPinky.setDisable(true);
		cbBlinky.setDisable(true);
		cbInky.setDisable(true);
		tfTaskName.setDisable(true);
		tfDeathPenalty.setDisable(true);
		tfStepPenalty.setDisable(true);
		tfWonReward.setDisable(true);
		tfDotReward.setDisable(true);		
		tfWallHitPenalty.setDisable(true);
		combbMaze.setDisable(true);
		combbDots.setDisable(true);
		combbTrainingMethod.setDisable(true);
	}

	/**
	 * Disables or enables the checkbox of inky depending on the status of the
	 * checkbox of blinky. Note: it is not possible to select inky without
	 * blinky (because inky uses the opposite target of blinky, see also inky
	 * class)
	 * */
	private void initCheckBoxes() {

		// inky is only selectable when blinky has been selected
		cbBlinky.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				cbInky.setDisable(!newValue);
				if (!newValue)
					cbInky.setSelected(false);
			}
		});

	}

	/**
	 * Depending on the mode of the view (editmode or otherwise) the current
	 * values in the task configuration view will be checked and saved to the
	 * current task. Otherwise a message with a hint will be displayed to the
	 * user (e.g. if the input values are not valid).
	 */
	private boolean checkAndSaveValues() {
		try {

			// if we are editing an existing task
			if (editOnly) {
				try {
					String actionSelectionName = (String) combbActionSelection.getSelectionModel().getSelectedItem().toString();
					double epsilon = 0;
					switch(actionSelectionName){
					case ActionSelection.EPSGREEDY:
						 epsilon = TextfieldConverterHelper.getPositiveDoubleSmallerOne(tfEpsilon, "Epsilon");
						 break;
					case ActionSelection.SOFTMAX:
						 epsilon = TextfieldConverterHelper.getPositiveDoubleGreaterZero(tfEpsilon, "Temperature");
						 break;
					default:
						epsilon = TextfieldConverterHelper.getPositiveDouble(tfEpsilon, "Epsilon");
						break;
					}
					
					double learningRate = TextfieldConverterHelper.getPositiveDoubleSmallerOne(tfAlpha, "Learning Rate");
					double discountRate = TextfieldConverterHelper.getPositiveDoubleSmallerOne(tfGamma, "Discount Rate");
					AgentFactory.updateAgentActionSelectionWith(currentTask.getAgent(), actionSelectionName, epsilon);
					AgentFactory.updateAgentValueFunctionWith(currentTask.getAgent(), discountRate, learningRate);

					//overwrite the physical file for this task with TaskPersistenceManager
					currentTask.overwrite();
				} catch (Exception e) {
					showMessage("Message from PacMan", "Can't edit Task", e.getMessage());
					return false;
				}
			}
			// if we created a new task
			else {

				float deathPenalty = TextfieldConverterHelper.getNegativeFloat(tfDeathPenalty, "Death Penalty");
				float stepPenalty = TextfieldConverterHelper.getNegativeFloat(tfStepPenalty, "Step Penalty");
				float wonReward = TextfieldConverterHelper.getPositiveFloat(tfWonReward, "Win Reward");
				float dotReward = TextfieldConverterHelper.getPositiveFloat(tfDotReward, "Dot Reward");
				float wallHitPenalty = TextfieldConverterHelper.getNegativeFloat(tfWallHitPenalty, "Wall Hit Penalty");
				
				String actionSelectionName = (String) combbActionSelection.getSelectionModel().getSelectedItem();
				double epsilon = 0;
				switch(actionSelectionName){
				case ActionSelection.EPSGREEDY:
					 epsilon = TextfieldConverterHelper.getPositiveDoubleSmallerOne(tfEpsilon, "Epsilon");
					 break;
				case ActionSelection.SOFTMAX:
					 epsilon = TextfieldConverterHelper.getPositiveDoubleGreaterZero(tfEpsilon, "Temperature");
					 break;
				default:
					epsilon = TextfieldConverterHelper.getPositiveDouble(tfEpsilon, "Epsilon");
					break;
				}
				
				double learningRate = TextfieldConverterHelper.getPositiveDoubleSmallerOne(tfAlpha, "Learning Rate");
				double discountRate = TextfieldConverterHelper.getPositiveDoubleSmallerOne(tfGamma, "Discount Rate");

				String dotModeName = (String) combbDots.getSelectionModel().getSelectedItem();
				String mazeName = (String) combbMaze.getSelectionModel().getSelectedItem();
				String learningMethodName = (String) combbTrainingMethod.getSelectionModel().getSelectedItem();

				//Task name must not be empty and has to be unique (will be ensured by taskpersistancemanager)
				String taskName = TextfieldConverterHelper.getNotEmptyString(tfTaskName, "Taskname");

				currentTask.setDeathPenalty(deathPenalty);
				currentTask.setStepPenalty(stepPenalty);
				currentTask.setWonReward(wonReward);
				currentTask.setDotReward(dotReward);
				currentTask.setWallHitPenalty(wallHitPenalty);

				currentTask.setDotMode(DotMode.getDotModeByString(dotModeName));
				currentTask.setMazeName(mazeName);

				currentTask.setClydeSelected(cbClyde.isSelected());
				currentTask.setPinkySelected(cbPinky.isSelected());
				currentTask.setBlinkySelected(cbBlinky.isSelected());
				currentTask.setInkySelected(cbInky.isSelected());

				currentTask.setName(taskName);

				Agent agent = AgentFactory.createAPacManAgentWith(learningMethodName, actionSelectionName, epsilon, learningRate, discountRate);
				currentTask.setAgent(agent);
				
				//create a physical file for this task with TaskPersistenceManager
				currentTask.save();
			}

			// store data in settings
			settings.setRlTask(currentTask);

		} catch (Exception e) {
			showMessage("Message from PacMan", "Can't save Task", e.getMessage());
			return false;
		}

		return true;
	}

	/**
	 * load the maze preview by mazeLoader using the mazeName and adds the
	 * preview to the ui
	 * 
	 * @param mazeName
	 *            the name of the maze. the name must be available for
	 *            mazeloader (means it should be a corresponding maze .pbm file
	 *            available.)
	 */
	private void loadMazePreview(String mazeName) {

		if (mazeName == null)
			return;

		// maybe we have to remove an old instance
		unsetMazePreview();

		// create game field
		Maze maze = MazeLoader.getInstance().getPacMaze(mazeName);

		if (maze != null) {
			// calculate block size of the maze
			double blockWidth = mazePreviewHolder.getWidth() / maze.getWidth();
			double blockHeight = mazePreviewHolder.getHeight() / maze.getHeight();
			double blockSize = Math.min(blockWidth, blockHeight);
			gpMazePreview = PacEnvUIController.createGridPaneByMaze(maze, blockSize);

			// add maze to UI
			mazePreviewHolder.getChildren().add(gpMazePreview);
		}
	}

	/** removes the maze preview from the maze preview holder. */
	private void unsetMazePreview() {
		if (gpMazePreview != null) {
			if (mazePreviewHolder.getChildren().contains(gpMazePreview))
				mazePreviewHolder.getChildren().remove(gpMazePreview);
		}
	}

	/** will send the user back to the task configuration overview. */
	@FXML
	public void backToOverview() {
		switchView(ViewFile.TASKCONFIGURATION_VIEW, ViewFile.TASKOVERVIEW_VIEW);
	}

	/**
	 * saves the task if possible and sends the user back to the task
	 * configuration overview if action was successful.
	 */
	@FXML
	public void save() {
		if (checkAndSaveValues())
			backToOverview();
	}

}