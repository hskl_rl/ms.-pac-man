package controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import utils.EpisodeHelper;
import utils.MazeLoader;
import utils.TextfieldConverterHelper;
import view.ReinforcementGridControl;
import view.TaskUIModel;
import model.Statistics;
import model.environment.MoveAction;
import model.environment.pacman.PacEnvironment;
import model.environment.pacman.PacEnvironment.DotMode;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.WindowEvent;

/**
 * Controller class for displaying the pacman game either in reinforcement
 * learning or 'just playing pacman' mode without any reinforcement agent /
 * task. Only controls the common ui elements for statistic, task information
 * and the 'play Episodes' buttons and fields. The maze itself is hold and
 * controlled by PacEnvUIController. However the PacEnvUIController will get
 * notifications by this controller if the user changed the maze by selecting
 * another value in the combobox or other relevant info. Implements
 * StatisticsListener so it can update the ui if the statistic information of
 * the task is updated by the environment while the agent is executing actions
 * (e.g. if the pac wins an episode the corresponding label for 'won games' must
 * be updated).
 * */
public class GameController extends Controller implements Initializable, StatisticsListener {

	protected final static Logger log = Logger.getLogger(GameController.class.getName());

	private class GhostCheckBoxListener implements ChangeListener<Boolean> {
		@Override
		public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2) {
			changeGhosts();
		}
	}

	// UI Elements of the FXML File
	@FXML
	private AnchorPane environmentHolder, reinforcementHolder;

	@FXML
	private ComboBox<String> combbMaze, combbDots;

	@FXML
	private Button btnBack, btnStartStop, btnDoStep, btnDoEpisodes, btnStartAutoPlay, btnStopAutoPlay;

	/**
	 * Checkbox for ghost. Tells whether the corresponding ghost is currently in
	 * the game or not.
	 */
	@FXML
	private CheckBox cbClyde, cbPinky, cbBlinky, cbInky;

	@FXML
	private Label lbLives, lbScore, lbEpisodesCurrent, lbWonCurrent, lbLostCurrent, lbBestScoreCurrent, lbAverageScoreCurrent;

	@FXML
	private TextField tfEpisodeNumber;

	/**
	 * gridpane with the ui elements for handling the 'simulation' of episodes
	 * and actions
	 */
	@FXML
	private GridPane simulationHolder;

	/** Stores the data of the task which should be displayed to the user */
	private TaskUIModel taskUIModel;

	// UI Elements created on runtime
	private PacEnvUIController envUI;

	/** displays the data from taskUIModel to the user */
	private ReinforcementGridControl reinforcementLearningGrid;

	/** Properties for model and view connection */
	private BooleanProperty gameIsRunning = new SimpleBooleanProperty(false);
	private BooleanProperty blinkyIsSelected = new SimpleBooleanProperty(false);
	private BooleanProperty inkyIsSelected = new SimpleBooleanProperty(false);
	private BooleanProperty pinkyIsSelected = new SimpleBooleanProperty(false);
	private BooleanProperty clydeIsSelected = new SimpleBooleanProperty(false);
	private BooleanProperty isAutoPlaying = new SimpleBooleanProperty(false);
	
	/**
	 * is called while initialising the view. it would be possible to add
	 * dynamically created ui elements here before displaying anything to the
	 * user. in case of this view we need our settings object, but the settings
	 * object is not accessible in initialize because creating the view is not
	 * completed. therefore we use the afterInit method, which is called right
	 * after initialisation of the view has finished.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// stage and settings not accessible here
	}

	/**
	 * is called right after initialisation of the view has finished. We use the
	 * settings object to create the maze, the reinforcement grid and fill the
	 * other ui elements.
	 * */
	@Override
	public void afterInit() {

		// GAME FIELD
		loadEnvUI();

		// add the reinforcement learning information only if this is not a
		// manual game
		if (!settings.isManualGame() && reinforcementLearningGrid == null) {
			taskUIModel = new TaskUIModel(settings.getRlTask());
			reinforcementLearningGrid = new ReinforcementGridControl();
			reinforcementLearningGrid.bindActionSelectionMethod(taskUIModel.getActionSelectionMethod());
			reinforcementLearningGrid.bindTaskName(taskUIModel.getName());
			reinforcementLearningGrid.bindMazeName(taskUIModel.getMaze());
			reinforcementLearningGrid.bindTotalEpisodes(taskUIModel.getTotalEpisodes());
			reinforcementLearningGrid.bindTrainingMethod(taskUIModel.getTrainingMethod());
			reinforcementHolder.getChildren().add(reinforcementLearningGrid);
		}

		bindUIElements();
		initComboBoxes();
		setCheckBoxListener();
		updateStatisticLabels();

		// listen to statistics, so the labels will be updated when the
		// statistic values such as number of won games or lost games are
		// changed
		settings.getRlTask().getStatisticsForCurrentTrainingLevel().addStatisticsListener(this);

		// enable manual controls
		if (settings.isManualGame()) {
			setupKeyHandler();
		}
		// or enable simulation controls
		else {
			simulationHolder.setVisible(true);
			btnStartStop.setVisible(false);
		}

		setupCloseHandler();
	}

	/**
	 * if the statistics object of the current task was updated in background
	 * the corresponding labels in the ui gets updated by this method
	 */
	private void updateStatisticLabels() {

		// this statistics will also be available if we started a manual game,
		// but the values will be 0
		Statistics stats = settings.getRlTask().getStatisticsForCurrentTrainingLevel();
		Platform.runLater(() -> {
			lbLives.setText(envUI.getPacLives());
			lbScore.setText(envUI.getPacScore());
			lbEpisodesCurrent.setText(Integer.toString(stats.getPlayedGames()));
			lbWonCurrent.setText(Integer.toString(stats.getWonGames()));
			lbLostCurrent.setText(Integer.toString(stats.getLostGames()));
			lbBestScoreCurrent.setText(Double.toString(stats.getMaxScore()));
			lbAverageScoreCurrent.setText(Double.toString(stats.getAverageScore()));
		});

	}

	/**
	 * Removes the old maze if necessary and adds a new gridPane with the new
	 * maze provided by PacEnvUIController
	 */
	private void loadEnvUI() {

		log.info("loadEnvUI ENTER");

		// maybe we have to remove an old instance of the envUI
		if (envUI != null && envUI.getGridPane() != null) {
			if (environmentHolder.getChildren().contains(envUI.getGridPane())) {
				environmentHolder.getChildren().remove(envUI.getGridPane());
			}
		}

		// create game field
		envUI = new PacEnvUIController(settings, environmentHolder);

		// add game field to UI
		environmentHolder.getChildren().add(envUI.getGridPane());

		// force redraw to put elements to the right position
		stage.addEventHandler(WindowEvent.WINDOW_SHOWN, new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				envUI.afterInit();
				stage.removeEventHandler(WindowEvent.WINDOW_SHOWN, this);				
			}
		});
		stage.hide();
		stage.show();
	}

	/**
	 * Binds UI Elements to the relevant model information
	 * */
	private void bindUIElements() {
		bindGhostCheckboxes();
		bindSimControls();
	}

	/**
	 * Bind the ghost checkbox selectedProperty bidirectional to the properties
	 * defined as class members and adds listener for the properties. Thus if
	 * the property will change the corresponding values in the settings will
	 * change accordingly. Moreover it is ensured that the checkboxes are
	 * disabled if the game is currently running.
	 * */
	private void bindGhostCheckboxes() {
		// Ghost checkboxes
		blinkyIsSelected = new SimpleBooleanProperty(settings.getTempTask().isBlinkySelected());
		blinkyIsSelected.addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				settings.getTempTask().setBlinkySelected(newValue);
			}
		});
		inkyIsSelected = new SimpleBooleanProperty(settings.getTempTask().isInkySelected());
		inkyIsSelected.addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				settings.getTempTask().setInkySelected(newValue);
			}
		});
		pinkyIsSelected = new SimpleBooleanProperty(settings.getTempTask().isPinkySelected());
		pinkyIsSelected.addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				settings.getTempTask().setPinkySelected(newValue);
			}
		});
		clydeIsSelected = new SimpleBooleanProperty(settings.getTempTask().isClydeSelected());
		clydeIsSelected.addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				settings.getTempTask().setClydeSelected(newValue);
			}
		});

		cbClyde.selectedProperty().bindBidirectional(clydeIsSelected);
		cbPinky.selectedProperty().bindBidirectional(pinkyIsSelected);
		cbBlinky.selectedProperty().bindBidirectional(blinkyIsSelected);
		cbInky.selectedProperty().bindBidirectional(inkyIsSelected);

		// inky can only exist if blinky exists because inky has the oposite
		// target of blinky
		cbBlinky.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (!newValue) {
					cbInky.setSelected(false);
				}
			}
		});
		cbInky.disableProperty().bind(Bindings.or(gameIsRunning, cbBlinky.selectedProperty().not()));

		// preferences are disabled if game is running at the moment
		combbMaze.disableProperty().bind(gameIsRunning);
		cbClyde.disableProperty().bind(gameIsRunning);
		cbPinky.disableProperty().bind(gameIsRunning);
		cbBlinky.disableProperty().bind(gameIsRunning);
		combbDots.disableProperty().bind(gameIsRunning);
	}

	/**
	 * Binds the simulation ui controls to the properties defined as class
	 * members. Thus if the state of the game view changed (e.g. autoplay is
	 * started) the proper ui elements will be enabled or disabled.
	 * */
	private void bindSimControls() {
		btnStartAutoPlay.disableProperty().bind(gameIsRunning);
		btnStopAutoPlay.disableProperty().bind(isAutoPlaying.not());
		btnDoStep.disableProperty().bind(gameIsRunning);
		btnDoEpisodes.disableProperty().bind(gameIsRunning);
		tfEpisodeNumber.disableProperty().bind(gameIsRunning);
	}

	/**
	 * Adds GhostCheckBoxListeners to the ghost checkboxes. Ghosts will be added
	 * to the maze if the checkboxes are selected.
	 * */
	private void setCheckBoxListener() {
		cbClyde.selectedProperty().addListener(new GhostCheckBoxListener());
		cbPinky.selectedProperty().addListener(new GhostCheckBoxListener());
		cbBlinky.selectedProperty().addListener(new GhostCheckBoxListener());
		cbInky.selectedProperty().addListener(new GhostCheckBoxListener());
	}

	/**
	 * Initialises the comboboxes for maze, action selection and value function
	 * with the available values (e.g. the available mazes or value functions)
	 * and adds listeners to the ui controls
	 * */
	private void initComboBoxes() {

		ObservableList<String> mazeNames = FXCollections.observableArrayList(MazeLoader.getInstance().getPacMazeNames());
		combbMaze.setItems(mazeNames);
		combbMaze.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> selected, String oldValue, String newValue) {
				changeMaze(newValue);
			}
		});

		// select the option that is given by the attached rlTask
		combbMaze.getSelectionModel().select(settings.getTempTask().getMazeName());

		List<String> helperList = new ArrayList<String>();
		ObservableList<String> dotmodes = FXCollections.observableList(helperList);
		for (DotMode dm : DotMode.ALL_DOT_MODES) {
			dotmodes.add(dm.getLabel());
		}
		combbDots.setItems(dotmodes);
		combbDots.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> selected, String oldValue, String newValue) {
				changeDotMode(newValue);
			}
		});

		// select the option that is default entry in settings
		combbDots.getSelectionModel().select(settings.getTempTask().getDotMode().getLabel());
	}

	/** Updates the ui if the ghosts where changed. */
	private void changeGhosts() {
		// reload is necessary to draw changes correctly.
		loadEnvUI();
	}

	/**
	 * updates the ui if the maze was changed
	 * 
	 * @param newMazeName
	 *            the name of the new maze.
	 */
	private void changeMaze(String newMazeName) {
		settings.getTempTask().setMazeName(newMazeName);
		// reload is necessary to draw changes correctly.
		loadEnvUI();
	}

	/**
	 * Changes the dotmode according to the parameter value. If the dotmode
	 * string does not exists in settings no changes are made.
	 * 
	 * @param newDotMode
	 *            the new dotMode as String. should be the string from the
	 *            correspoding combobox.
	 * */
	private void changeDotMode(String newDotMode) {
		for (DotMode dm : DotMode.ALL_DOT_MODES) {
			if (dm.getLabel().equals(newDotMode)) {
				settings.getTempTask().setDotMode(dm);
			}
			// reload is necessary to draw changes correctly.
			loadEnvUI();
		}

	}

	/**
	 * Handles the user keyboard input. the left, right, up, down arrows are
	 * used for navigating the pacman in the maze and for starting the game if
	 * not already started.
	 * 
	 * **/
	private void setupKeyHandler() {
		stage.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				switch (event.getCode()) {
				case UP:
					envUI.setNext(MoveAction.GO_NORTH);
					break;
				case RIGHT:
					envUI.setNext(MoveAction.GO_EAST);
					break;
				case DOWN:
					envUI.setNext(MoveAction.GO_SOUTH);
					break;
				case LEFT:
					envUI.setNext(MoveAction.GO_WEST);
					break;
				default:
					break;
				}

				// if the game is not running but the user indicates that he
				// wants
				// to play by pressing the move keys, the game is started
				if (!gameIsRunning.getValue()) {
					startStop();
				}
			}
		});
	}

	/**
	 * Created a close handler for the current stage. if the window is closed
	 * all current timers will be stopped.
	 */
	private void setupCloseHandler() {
		stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent window) {
				envUI.stopTimers();
			}
		});
	}

	/**
	 * Cancels all current timers and sends the user back to the last view. the
	 * last view is saved in settings.
	 */
	@FXML
	public void cancel() {
		envUI.cancelAnimations();
		envUI.stopTimers();
		
		// send back to menu directly
		if (settings.isManualGame()) {
			switchView(ViewFile.GAME_VIEW, ViewFile.MAINMENU_VIEW);
		} else {
			// switch to the task overview
			switchView(ViewFile.GAME_VIEW, ViewFile.TASKOVERVIEW_VIEW);
		}
	}

	/**
	 * used in manual game mode to start / stop game
	 */
	@FXML
	private void startStop() {

		// stop game
		if (gameIsRunning.getValue()) {
			envUI.cancelAnimations();
			envUI.stopTimers();
			btnStartStop.setText("Start Game");

			// start game
		} else {
			envUI.cancelAnimations();
			envUI.startTimers();
			btnStartStop.setText("Stop Game");
		}

		gameIsRunning.set(!gameIsRunning.getValue());

		log.info("startStop LEAVE. is game running:" + gameIsRunning.getValue());
	}

	/**
	 * Used in reinforcement learning with a current task. this method will
	 * automatically perform one time step for the user.
	 * */
	@FXML
	public void doStep() {
		envUI.cancelAnimations();
		envUI.fire();
	}

	/**
	 * used in reinforcement learning mode with a current task. this method will
	 * automatically perform a specific number of episodes for the user in a
	 * background thread. One episode will end if the pac lost or won the game.
	 * It is possible for the user cancel the process if the pac is stuck in an
	 * infinite loop or it took too much time.
	 * */
	@FXML
	public void doEpisodes() {

		// check userinput
		int numOfEpisodes = -1;
		try {
			// validate input
			numOfEpisodes = TextfieldConverterHelper.getPositiveInt(tfEpisodeNumber, "Number of Episodes");
		} catch (Exception e) {
			showMessage("Input Error", "Input Error", e.getMessage());
		}

		envUI.cancelAnimations();

		EpisodeHelper episodeHelper = new EpisodeHelper(envUI.getEnvironment(), numOfEpisodes, settings.getRlTask());
		PacEnvironment env = envUI.getEnvironment();

		// show progress Dialog
		episodeHelper.getProgressBarDialog().setOnCloseRequest(new EventHandler<DialogEvent>() {
			public void handle(DialogEvent de) {
				// We have to cancel the execution
				episodeHelper.abort();
				gameIsRunning.set(false);
				updateStatisticLabels();
				envUI.afterInit();
				env.addEnvironmentChangedListener(envUI);
			}
		});
		episodeHelper.getProgressBarDialog().show();

		// Disable UI events
		env.removeEnvironmentChangedListener(envUI);

		// perform episodes
		gameIsRunning.set(true);
		Thread thread = new Thread(episodeHelper);
		thread.setName("Episodes");
		thread.start();
	}

	/**
	 * Starts an autoplay process. the pac will move and play episodes until the
	 * user stops the autoplay.
	 */
	@FXML
	public void startAutoPlay() {
		gameIsRunning.set(true);
		isAutoPlaying.set(true);

		envUI.cancelAnimations();
		envUI.startTimers();
	}

	/** Stops the autoplay process. */
	@FXML
	public void stopAutoPlay() {
		gameIsRunning.set(false);
		isAutoPlaying.set(false);

		envUI.cancelAnimations();
		envUI.stopTimers();
	}

	/**
	 * gets called by statistic object if the values of the statistic object
	 * where changed. we need to update the ui elements in this case. e.g. if
	 * the number of won episodes was increased we want to show this to the
	 * user.
	 */
	@Override
	public void statisticsChanged() {
		updateStatisticLabels();
	}

}