package controller;

import java.net.URL;
import java.util.ResourceBundle;

import model.Task;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * Controller for the main menu of the application. Allows the user to play in a
 * simple simulation environment, play the pac man game without reinforcement
 * learning or use a reinforcement learning task.
 * */
public class MainMenuController extends Controller implements Initializable {

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		log.info("initialize");
	}

	/**
	 * starts the reinforcement learning with a user defined reinforcement
	 * learning tasks
	 * */
	@FXML
	public void startReinforcementLearning(ActionEvent event) {
		// switch to task overview
		settings.setManualGame(false);
		switchView(ViewFile.MAINMENU_VIEW, ViewFile.TASKOVERVIEW_VIEW);
	}

	/**
	 * starts the pac-man game without reinforcement learning and nothing more.
	 * */
	@FXML
	public void startGame(ActionEvent event) {
		// switch to game view
		settings.setManualGame(true);
		Task task = new Task();
		settings.setRlTask(task);
		switchView(ViewFile.MAINMENU_VIEW, ViewFile.GAME_VIEW);
	}

	/** closes the application */
	@FXML
	public void quit(ActionEvent event) {
		Platform.exit();
	}

	/**
	 * sends the user to a view with a simple reinforcement learning
	 * environment, with just a pac and stationary ghosts. ideal for figuring
	 * out how the reinforcement learning algorithms work.
	 */
	@FXML
	public void startSimulation(ActionEvent event) {
		settings.setManualGame(true);
		switchView(ViewFile.MAINMENU_VIEW, ViewFile.SIMULATION_VIEW);
	}
}