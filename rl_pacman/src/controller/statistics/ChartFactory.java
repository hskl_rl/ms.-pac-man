package controller.statistics;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.logging.Logger;

import model.Statistics;
import model.Task;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Side;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;

/**
 * Helper class for creating JavaFX charts. read more information about
 * charts here:
 * http://docs.oracle.com/javafx/2/charts/chart-overview.htm#CJAHHJCB and <br>
 * https://docs.oracle.com/javafx/2/charts/line-chart.htm and <br>
 * http://docs.oracle.com/javafx/2/charts/css-styles.htm  <br>
 * */
public class ChartFactory {

	private static final List<ChartType> ALL_CHART_TYPES = new ArrayList<ChartType>(EnumSet.allOf(ChartType.class));

	/**
	 * names for the different chart types. these names are displayed in the combobox in statistic view. 
	 * Should not be used accessed directly (therefore private). use ChartType.getChartTypesAsList instead.
	 */
	private static final String MAX_SCORE_PER_TRAINING_LEVEL_NAME = "max score per training level";
	private static final String AVG_SCORE_PER_TRAINING_LEVEL_NAME = "avg score per training level";
	private static final String PERC_WINS_PER_TRAINING_LEVEL_NAME = "percentage of wins per training level";

	/**
	 * a small enum helper type. stores the chart types including the
	 * corresponding x- and y-axis label value
	 */
	public enum ChartType {
		//TODO currently we only have 3 different chart types. add more if necessary.
		MAX_SCORE_PER_TRAINING_LEVEL(MAX_SCORE_PER_TRAINING_LEVEL_NAME, "training level", "max score"), 
		AVG_SCORE_PER_TRAINING_LEVEL(AVG_SCORE_PER_TRAINING_LEVEL_NAME, "training level", "avg score"), 
		PERC_WINS_PER_TRAINING_LEVEL(PERC_WINS_PER_TRAINING_LEVEL_NAME, "training level", "percentage of wins");

		public final String name;
		public final String xLabel;
		public final String yLabel;

		private ChartType(final String name, String xLabel, String yLabel) {
			this.name = name;
			this.xLabel = xLabel;
			this.yLabel = yLabel;
		}

		private static String getXLabelByName(String name) {
			for (ChartType t : ALL_CHART_TYPES) {
				if (t.name.equals(name))
					return t.xLabel;
			}
			throw new RuntimeException("ChartType getXLabelByName(): No ChartType with name=" + name + " found!");
		}

		private static String getYLabelByName(String name) {
			for (ChartType t : ALL_CHART_TYPES) {
				if (t.name.equals(name))
					return t.yLabel;
			}
			throw new RuntimeException("ChartType getYLabelByName(): No ChartType with name=" + name + " found!");
		}

		public static ObservableList<String> getChartTypesAsList() {
			List<String> strings = new ArrayList<String>();
			for (ChartType t : ALL_CHART_TYPES) {
				strings.add(t.name);
			}
			ObservableList<String> typesList = FXCollections.observableArrayList(strings);
			return typesList;
		}

		public final String getName() {
			return name;
		}
	}

	/** The default chart type. MAX_SCORE_PER_TRAINING_LEVEL in this case. */
	public static final ChartType DEFAULT_CHART_TYPE = ChartType.MAX_SCORE_PER_TRAINING_LEVEL;

	protected final static Logger log = Logger.getLogger(ChartFactory.class.getName());

	/**
	 * creates a linechart ui object of the currently selected tasks. see
	 * https://docs.oracle.com/javafx/2/charts/line-chart.htm and
	 * http://docs.oracle.com/javafx/2/charts/css-styles.htm for more
	 * information
	 * 
	 * @param width
	 *            width of the resulting linechart
	 * @param height
	 *            height if the resulting linechart
	 * @param tasks
	 *            the tasks which should be used for the linechart
	 * @param chartName
	 *            the name of the chart (will be displayed above the chart)
	 * 
	 * @return a linechart object which can be added to the ui
	 * */
	public static LineChart<Number, Number> createLineChart(double width, double height, List<Task> tasks, String chartName) {

		if (chartName == null)
			return null;

		final NumberAxis xAxis = new NumberAxis();
		final NumberAxis yAxis = new NumberAxis();

		String xAxisLabel = ChartType.getXLabelByName(chartName);
		String yAxisLabel = ChartType.getYLabelByName(chartName);

		if (xAxisLabel != null)
			xAxis.setLabel(xAxisLabel);
		if (yAxisLabel != null)
			yAxis.setLabel(yAxisLabel);

		// creating the chart
		final LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);

		lineChart.setTitle(chartName);

		lineChart.setLegendSide(Side.BOTTOM);
		lineChart.setPrefSize(width, height);
		lineChart.setAlternativeRowFillVisible(true);

		Series<Number, Number> currentSeries = null;

		for (Task t : tasks) {
			currentSeries = getSeriesFromStatistics(t.getStatisticsPerTrainingLevel(), t.getName(), chartName);
			if (currentSeries != null)
				lineChart.getData().add(currentSeries);
		}

		return lineChart;
	}

	/**
	 * A helper method which creates a series (a specific line in a chart) from
	 * some given statistics objects
	 * 
	 * @param taskStatistics
	 *            the list ob statistics which should be displayed in the chart
	 *            later
	 * @param seriesName
	 *            the name of the series, e.g. the task name (will be used for
	 *            labeling the specific line in the chart)
	 * @param chartName
	 *            the name of the chart
	 * 
	 */
	private static Series<Number, Number> getSeriesFromStatistics(List<Statistics> taskStatistics, String seriesName, String chartName) {

		if (taskStatistics == null || taskStatistics.size() <= 0) {
			return null;
		}

		XYChart.Series<Number, Number> series = new XYChart.Series<Number, Number>();

		double xAxisValue = 0;
		double yAxisValue = 0.0;

		for (Statistics currentStatistic : taskStatistics) {
			xAxisValue = getXAxisValue(currentStatistic, chartName);
			yAxisValue = getYAxisValue(currentStatistic, chartName);
			series.getData().add(new XYChart.Data<Number, Number>(xAxisValue, yAxisValue));
		}

		series.setName(seriesName);

		return series;
	}

	/**
	 * Returns an double value which should be used as y axis value for a series
	 * object
	 * 
	 * @param currentStatistic
	 *            the statistic object
	 * @param chartName
	 *            chart name is necessary to determine which value in the
	 *            statistics object should be used for the axis.
	 * @return the y axis value as double
	 * 
	 * */
	private static double getYAxisValue(Statistics currentStatistic, String chartName) {
		double value = 0.0;

		//TODO add more chart types here if necessary
		switch (chartName) {
		case MAX_SCORE_PER_TRAINING_LEVEL_NAME:
			value = currentStatistic.getMaxScore();
			break;
		case AVG_SCORE_PER_TRAINING_LEVEL_NAME:
			value = currentStatistic.getAverageScore();
			break;
		case PERC_WINS_PER_TRAINING_LEVEL_NAME:
			value = (double) (currentStatistic.getWonGames() * 100.0) / currentStatistic.getPlayedGames();
			break;
		default:
			throw new RuntimeException("ChartFactory - Missing Chart Type '" + chartName + "' in getYAxisValue.");
		}
		return value;
	}

	/**
	 * Returns an double value which should be used as x axis value for a series
	 * object
	 * 
	 * @param currentStatistic
	 *            the statistics object
	 * @param chartName
	 *            chart name is necessary to determine which value in the
	 *            statistics object should be used for the axis.
	 * @return the x axis value as double
	 * 
	 * */
	private static double getXAxisValue(Statistics currentStatistic, String chartName) {
		double value = 0.0;

		switch (chartName) {
		case MAX_SCORE_PER_TRAINING_LEVEL_NAME:
		case AVG_SCORE_PER_TRAINING_LEVEL_NAME:
		case PERC_WINS_PER_TRAINING_LEVEL_NAME:
			value = currentStatistic.NR_TRAINED_EPISODES;
			break;
		default:
			throw new RuntimeException("ChartFactory - Missing Chart Type '" + chartName + "' in getXAxisValue.");
		}
		return value;
	}

}
