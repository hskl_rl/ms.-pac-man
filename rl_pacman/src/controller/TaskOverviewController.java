package controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import model.Task;
import model.environment.pacman.Maze;
import model.environment.pacman.PacEnvironment;
import model.reinforcement_learning.training.TrainingListener;
import utils.MazeLoader;
import utils.TaskPersistenceManager;
import utils.TextfieldConverterHelper;
import utils.TrainingHelper;
import view.TaskUIModel;

/**
 * Controller for the Task Overview. Displays all currently existing tasks to
 * the user. Shows information about the tasks and a preview of the
 * corresponding mazes. The user can create new tasks, edit existing ones or
 * displaying statistic information for one ore more tasks.
 * */
public class TaskOverviewController extends Controller implements Initializable, TrainingListener {

	private TaskUIModel selectedUITask;

	private ObservableList<SimpleStringProperty> allTaskNames;

	@FXML
	private AnchorPane mazePreviewHolder;

	@FXML
	private GridPane gpTaskInfo;

	@FXML
	private Button btnNew, btnSelect, btnCancel, btnShow, btnStatistic, btnDelete, btnTrain;

	/** Input field for the number of episodes to train. */
	@FXML
	private TextField tfTrain;

	/** holds the names of all currently existing tasks in a table. */
	@FXML
	private TableView<SimpleStringProperty> tableViewTasks;

	@FXML
	private TableColumn<SimpleStringProperty, String> columnTaskName;

	@FXML
	private Label lbTrainingMethod, lbActionSelection, lbMaze, lbEpisodes, lbHint;

	// UI Elements created on runtime
	private GridPane gpMazePreview;

	/**
	 * is called while initialising the view. it would be possible to add
	 * dynamically created ui elements here before displaying anything to the
	 * user. in case of this view we need our settings object, but the settings
	 * object is not accessible in initialize because creating the view is not
	 * completed. therefore we use the afterInit method, which is called right
	 * after initialisation of the view has finished.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	/**
	 * is called right after initialisation of the view has finished. We use the
	 * settings object to preselect the last user-chosen task, show the task
	 * information and preview the maze. If no task was chosen previously we
	 * will select the first task in the task-list (or none if the list is
	 * empty).
	 * */
	public void afterInit() {
		initTaskTable();

		// if we already had selected a task last time
		if (settings.getRlTask() != null && settings.getRlTask().getName() != null) {
			selectTask(settings.getRlTask().getName());
		}
		// else try to select the first task
		else if (!allTaskNames.isEmpty()) {
			selectTask(allTaskNames.get(0).get());
		}
		// else we have nothing to select
	}

	/**
	 * Loads all available task names by using the TaskPersistenceManager
	 * without loading all the task data from disc. Displays the names in the
	 * tableView. The other task data will be 'lazy-loaded' if the task is
	 * actually selected by the user.
	 * */
	private void initTaskTable() {

		// Get the names of all task files to show in UI
		allTaskNames = FXCollections.observableArrayList();
		for (String taskName : TaskPersistenceManager.getInstance().getTaskNames())
			allTaskNames.add(new SimpleStringProperty(taskName));

		columnTaskName.setSortType(TableColumn.SortType.ASCENDING);
		columnTaskName.setCellValueFactory(cellData -> cellData.getValue());
		tableViewTasks.setItems(allTaskNames);
		tableViewTasks.getSortOrder().add(columnTaskName);

		// For statistic display
		tableViewTasks.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		// set selectlistener to the listview
		tableViewTasks.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<SimpleStringProperty>() {
			@Override
			public void changed(ObservableValue<? extends SimpleStringProperty> observable, SimpleStringProperty oldTaskName,
					SimpleStringProperty newTaskName) {
				if (null != newTaskName)
					selectTask(newTaskName.getValue());
			}
		});
	}

	/**
	 * Is called if an element of the tableView is clicked. Loads all
	 * information for the selected task by using TaskPersistenceManager and
	 * sets the task object to its corresponding taskUI container for displaying
	 * the task info.
	 * 
	 * @param taskName
	 *            the name of the selected task. should not be null. otherwise
	 *            the current selection is removed and no task is loaded.
	 */
	public void selectTask(String taskName) {
		if (taskName == null) {
			deselect();
			return;
		}

		// execute selection in table view
		for (SimpleStringProperty currentTaskName : allTaskNames) {
			if (currentTaskName.get().equals(taskName)) {
				tableViewTasks.getSelectionModel().select(currentTaskName);
			}
		}

		Task taskModel = TaskPersistenceManager.getInstance().getTask(taskName);
		if (taskModel == null)
			return;

		if (null == selectedUITask)
			selectedUITask = new TaskUIModel(taskModel);
		else
			selectedUITask.setTask(taskModel);

		disableTaskUIControls(false);

		// refresh labels
		refreshTaskInfo();

		// show maze preview
		loadMazePreview();

		// remember the selected task in settings object
		settings.setRlTask(selectedUITask.getTask());
	}

	/**
	 * disables the task ui controls. deselects all currently selected elements
	 * in the table view and unsets the maze preview.
	 * 
	 * */
	private void deselect() {
		disableTaskUIControls(true);
		selectedUITask = null;
		tableViewTasks.getSelectionModel().select(null);
		unsetMazePreview();
	}

	/**
	 * disables or enabled all buttons and textfields in task overview.
	 * 
	 * @param disable
	 *            true if the ui elements should be disabled, false otherwise
	 * */
	private void disableUIControls(boolean disable) {
		btnNew.setDisable(disable);
		disableTaskUIControls(disable);
	}

	/**
	 * Disables or enabled all ui elements excluding the new button below the
	 * task table
	 * 
	 * @param disable
	 *            true if the ui elements should be disabled, false otherwise
	 * */
	private void disableTaskUIControls(boolean disable) {
		btnSelect.setDisable(disable);
		btnShow.setDisable(disable);
		btnStatistic.setDisable(disable);
		btnDelete.setDisable(disable);
		btnTrain.setDisable(disable);
		tfTrain.setDisable(disable);

		// edit can only be active if exactly one task is selected
		boolean multibleTasksSelected = 1 != tableViewTasks.getSelectionModel().getSelectedItems().size();
		btnShow.setDisable(multibleTasksSelected || disable);
	}

	/** Sends the user to the game view with the currently selected task */
	@FXML
	public void select() {

		if (settings.getRlTask() == null)
			return;

		// switch to the game view
		switchView(ViewFile.TASKOVERVIEW_VIEW, ViewFile.GAME_VIEW);
	}

	/**
	 * Sends the user back to the menu. The last selected task will be
	 * remembered anyway, because we save it everytime a task is selected in the
	 * list. This will guarantee that the task is selected if the user displays
	 * the overview again.
	 */
	@FXML
	public void backToMenu() {
		switchView(ViewFile.TASKOVERVIEW_VIEW, ViewFile.MAINMENU_VIEW);
	}

	/**
	 * deletes the currently selected tasks. the task above the last deleted
	 * task will be selected afterwards. does nothing if no task is selected.
	 * */
	@FXML
	public void deleteTasks() {

		List<Task> tasksToDelete = getSelectedTasks();
		if (null == tasksToDelete)
			return;

		int indexOfTask = -1;
		// remove from listView
		for (Task taskToDelete : tasksToDelete) {

			// reset for next check
			indexOfTask = -1;

			// find the task in the list of tasknames and remove it
			for (SimpleStringProperty taskProperty : allTaskNames) {
				if (taskToDelete.getName().equals(taskProperty.getValue())) {
					indexOfTask = allTaskNames.indexOf(taskProperty);
					break;
				}
			}
			if (indexOfTask != -1)
				allTaskNames.remove(indexOfTask);
			// remove physical task file from file system via PersistanceManager
			taskToDelete.delete();

		}

		refreshTasksTableView();

		// unset selected task
		deselect();

		// select next task
		if (!allTaskNames.isEmpty()) {
			// try to select the element on top of the last deleted task
			int indexToSelect = ((indexOfTask - 1) < 0) ? 0 : indexOfTask - 1;
			selectTask(allTaskNames.get(indexToSelect).get());
		}

		refreshTaskInfo();
	}

	/** send the user to the statistic view including the current selected tasks */
	@FXML
	public void showStatistic() {
		List<Task> statisticTasks = getSelectedTasks();

		if (statisticTasks != null && statisticTasks.size() > 0) {
			settings.setSelectedTasks(statisticTasks); // all selected tasks
			// switch to statistic view
			switchView(ViewFile.TASKOVERVIEW_VIEW, ViewFile.STATISTICS_VIEW);
		}
	}

	/**
	 * sends the user to the task configuration view including the current
	 * selected task. Only some of the values of the task can be edited in task
	 * config.
	 */
	@FXML
	public void editTask() {
		if (settings.getRlTask() == null)
			return;

		// switch to config view
		switchView(ViewFile.TASKOVERVIEW_VIEW, ViewFile.TASKCONFIGURATION_VIEW);
	}

	/**
	 * sends the user to the task configuration without any selected task. the
	 * user will be able to create a new task in the config view.
	 */
	@FXML
	public void newTask() {
		// we want to create a new task. set the current one to null so we
		// can notice we want to create a new task in configuration view
		settings.setRlTask(null);
		// switch to config view
		switchView(ViewFile.TASKOVERVIEW_VIEW, ViewFile.TASKCONFIGURATION_VIEW);

	}

	/** Training helper object. */
	private TrainingHelper trainingHelper;

	/**
	 * Will train the current Task by playing a specific amount of episodes with
	 * the agent. The training will be executed in a background task and may be
	 * canceled by the user if necessary. The total number of episodes is stated
	 * by the user in the textfield tfTrain.
	 */
	@FXML
	public void trainTask() {

		int numToTrain = -1;
		try {
			// check for valid user input
			numToTrain = TextfieldConverterHelper.getPositiveInt(tfTrain, "Train Episodes");
		} catch (Exception e) {
			showMessage("Input Error", "Input Error", e.getMessage());
		}

		PacEnvironment env = new PacEnvironment(selectedUITask.getTask());
		env.enableLearning(true);
		env.enableTrainingMode(true);
		trainingHelper = new TrainingHelper(selectedUITask.getTask(), env, numToTrain, this);

		// Show progress dialog to user
		trainingHelper.getProgressBarDialog().setOnCloseRequest(new EventHandler<DialogEvent>() {
			public void handle(DialogEvent de) {
				// if the user clicked the close button -> cancel training.
				// trainingFinished will be executed afterwards
				trainingHelper.getTraining().cancel();
			}
		});
		trainingHelper.getProgressBarDialog().show();

		disableUIControls(true);

		// Start training in a separate thread
		Thread thread = new Thread(trainingHelper);
		thread.setName("Training");
		thread.start();
	}

	/**
	 * Gets called as soon as traininghelper has finished the training (or was
	 * interrupted by the user)
	 * 
	 * @param canceled
	 *            true if the training was canceled by the user, false if it was
	 *            completed
	 * @param trainingEpisodes
	 *            the number of finished episodes. could be less than the value
	 *            in the textfield tfTrain if the user canceled the training.
	 * */
	@Override
	public void trainingFinished(boolean canceled, int trainingEpisodes) {
		if (trainingHelper == null)
			return;

		Platform.runLater(() -> {
			trainingHelper.getEnvironment().enableTrainingMode(false);
			trainingHelper.getEnvironment().enableLearning(false);

			// in case the user didn't close the dialog
			if (trainingHelper != null && trainingHelper.getProgressBarDialog() != null && trainingHelper.getProgressBarDialog().isShowing()) {
				trainingHelper.getProgressBarDialog().close();
			}
			selectedUITask.updateEpisodesLabel();
			disableUIControls(false);
		});
		saveTask();
	}

	/**
	 * Is called periodically by the Training class while the training proceeds.
	 * Will not be called a 100 times, but only for every 5% of progress.
	 * 
	 * @param percent
	 *            a integer between 0 and 100. progress percent)
	 * */
	@Override
	public void reportTrainingProgress(int percent) {
		if (trainingHelper != null)
			trainingHelper.updateProgress(percent);
	}

	/**
	 * Overwrites the physical file of the current Task by using the
	 * TaskPersistanceManager
	 */
	private void saveTask() {
		selectedUITask.getTask().overwrite();
	}

	@SuppressWarnings("rawtypes")
	private void refreshTasksTableView() {
		for (TableColumn col : tableViewTasks.getColumns()) {
			col.setVisible(false);
			col.setVisible(true);
		}
	}

	/**
	 * Refreshed the displayed task information for the currently selected task.
	 * for example if a new task was selected in the table view.
	 */
	private void refreshTaskInfo() {
		lbHint.setVisible(true);
		gpTaskInfo.setVisible(false);
		if (null == selectedUITask)
			return;

		// set the connection between the UIModel and the fxml file elements
		lbTrainingMethod.textProperty().bind(selectedUITask.getTrainingMethod());
		lbActionSelection.textProperty().bind(selectedUITask.getActionSelectionMethod());
		lbMaze.textProperty().bind(selectedUITask.getMaze());
		lbEpisodes.textProperty().bind(selectedUITask.getEpisodes());

		lbHint.setVisible(false);
		gpTaskInfo.setVisible(true);
	}

	/**
	 * load the maze preview by mazeLoader using the mazeName in the currently
	 * selected task and adds the preview to the ui
	 */
	private void loadMazePreview() {

		// maybe we have to remove an old instance
		unsetMazePreview();

		if (null == selectedUITask)
			return;

		// create game field
		Maze maze = MazeLoader.getInstance().getPacMaze(selectedUITask.getTask().getMazeName());

		if (maze != null) {
			// calc block size of the maze
			double blockWidth = mazePreviewHolder.getWidth() / maze.getWidth();
			double blockHeight = mazePreviewHolder.getHeight() / maze.getHeight();
			double blockSize = Math.min(blockWidth, blockHeight);
			gpMazePreview = PacEnvUIController.createGridPaneByMaze(maze, blockSize);

			// add maze preview to UI
			mazePreviewHolder.getChildren().add(gpMazePreview);
		}
	}

	/**
	 * removes the maze preview from mazePreviewHolder if a preview is currently
	 * displayed
	 */
	private void unsetMazePreview() {
		if (gpMazePreview != null) {
			if (mazePreviewHolder.getChildren().contains(gpMazePreview))
				mazePreviewHolder.getChildren().remove(gpMazePreview);
		}
	}

	/**
	 * Helper method that creates a Task list from the currently selected items
	 * in the task overview table. this tasks can be used for showing the
	 * statistic information or deleting the task or other purposes.
	 * */
	private List<Task> getSelectedTasks() {

		ObservableList<SimpleStringProperty> selectedItems = tableViewTasks.getSelectionModel().getSelectedItems();

		// no tasks selected, cancel here
		if (selectedItems == null || selectedItems.size() <= 0) {
			return null;
		}

		List<Task> tasks = new ArrayList<Task>();

		// crate a list with the selected tasks
		Task currentTask;
		for (SimpleStringProperty taskNameProperty : selectedItems) {
			if (taskNameProperty == null)
				break;
			currentTask = TaskPersistenceManager.getInstance().getTask(taskNameProperty.get());
			if (currentTask != null)
				tasks.add(currentTask);
		}

		return tasks;
	}

}