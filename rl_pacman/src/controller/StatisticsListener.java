package controller;

/**
 * Listener interface for statistics objects. can be used for controller
 * classes, which should listener to an statistic object in order to update some
 * labels or ui stuff.
 * */
public interface StatisticsListener {
	public void statisticsChanged();
}
