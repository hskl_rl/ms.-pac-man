package view;


import model.Statistics;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class StatisticsUIModel {

	private SimpleDoubleProperty max = new SimpleDoubleProperty(0);
	private SimpleDoubleProperty min = new SimpleDoubleProperty(0);
	private SimpleDoubleProperty avg = new SimpleDoubleProperty(0);
	private SimpleIntegerProperty won = new SimpleIntegerProperty(0);
	private SimpleIntegerProperty nrGames = new SimpleIntegerProperty(0);
	private SimpleIntegerProperty trainedEpisodes = new SimpleIntegerProperty(0);

	private Statistics correspondingStatistic;

	public StatisticsUIModel(Statistics statistic) {
		setStatistics(statistic);		
	}
	
	public void setStatistics(Statistics statistic){
		// if statistic is null we only can use the default values (see above)
		if (statistic == null)
			return;

		correspondingStatistic = statistic;
		
		setMax(statistic.getMaxScore());
		setMin(statistic.getMinScore());
		setAvg(statistic.getAverageScore());
		setWon(statistic.getWonGames());
		setNrGames(statistic.getPlayedGames());
		setTrainedEpisodes(statistic.NR_TRAINED_EPISODES);

	}

	/**
	 * @return the max
	 */
	public double getMax() {
		return max.get();
	}

	/**
	 * @param max the max to set
	 */
	public void setMax(double max) {
		this.max.set(max);
	}

	/**
	 * @return the min
	 */
	public double getMin() {
		return min.get();
	}

	/**
	 * @param min the min to set
	 */
	public void setMin(double min) {
		this.min.set(min);;
	}

	/**
	 * @return the avg
	 */
	public double getAvg() {
		return avg.get();
	}

	/**
	 * @param avg the avg to set
	 */
	public void setAvg(double avg) {		
		this.avg.set(avg);
	}

	

	/**
	 * @return the won
	 */
	public double getWon() {
		return won.get();
	}

	/**
	 * @param won the won to set
	 */
	public void setWon(int won) {
		this.won.set(won);
	}	

	/**
	 * @return the correspondingStatistic
	 */
	public Statistics getCorrespondingStatistic() {
		return correspondingStatistic;
	}

	/**
	 * @param correspondingStatistic the correspondingStatistic to set
	 */
	public void setCorrespondingStatistic(Statistics correspondingStatistic) {
		this.correspondingStatistic = correspondingStatistic;
	}

	public int getNrGames() {
		return nrGames.get();
	}

	public void setNrGames(int nrGames) {
		this.nrGames.set(nrGames);
	}
	
	public void setTrainedEpisodes(int trainedEpisodes) {
		this.trainedEpisodes.set(trainedEpisodes);
	}
	
	public int getTrainedEpisodes() {
		return this.trainedEpisodes.get();
	}

}
