package view;

import java.awt.Point;

import javafx.animation.Timeline;
import model.reinforcement_learning.Action;

public interface MovableUI {
	
	/**
	 * Returns the position of the ui element within the parent ui-grid
	 */
	public Point getGridPosition();
	
	/**
	 * Returns the width of the ui element
	 */
	public double getWidth();
	
	/**
	 * Returns the height of the ui element
	 */
	public double getHeight();
	
	/**
	 * Switches the current appearance of the ui according to the given action.
	 * This makes the  UI appear "turning" into a certain direction.
	 */
	public void turnTo(Action action);
	
	/**
	 * Should be used to animate the smooth transition from the UIs current position to the new one
	 */
	public void moveTo(double screenX, double screenY);
	
	/**
	 * Should be used to animate the instant transition from the UIs current position to the new one
	 */
	public void teleportTo(double screenX, double screenY);
	
	/**
	 * Plays the ui-elements blink animation once
	 */
	public void playBlinkAnimation();
	
	/**
	 * Stops the ui-elements blink animation if running
	 */
	public void stopBlinkAnimation();
	
	/**
	 * Returns the ui-elements blink animation
	 */
	public Timeline getBlinkAnimation();
	
	/**
	 * Determines whether the ui-elements blink animation is running
	 */
	public boolean blinkAnimationIsRunning();
}
