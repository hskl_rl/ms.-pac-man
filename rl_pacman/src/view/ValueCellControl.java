package view;

import java.io.IOException;
import java.text.DecimalFormat;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ValueCellControl extends StackPane {

	@FXML
	private Label lblValue;
	@FXML
	private Rectangle recBackground;

	private DecimalFormat formatter = new DecimalFormat("0.00");

	public ValueCellControl() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ValueCell.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	public void setText(String text){
		lblValue.setText(text);
	}
	
	public ValueCellControl(double value) {
		this();
		this.setValue(value);
	}

	public String getValue() {
		return this.lblValue.getText();
	}

	public void setValue(double value) {
		String sValue = formatter.format(value);
		recBackground.setFill(getColorByValue(value));
		recBackground.toBack();

		if (Math.abs(value) > 0.5) {
			lblValue.setStyle("-fx-text-fill: black");
		} else {
			lblValue.setStyle("-fx-text-fill: white");
		}

		setText(sValue);
	}

	public void setBackgroundColor(Color c) {
		if (c != null)
			recBackground.setFill(c);
	}

	/*
	 * creates a color object depending of a given double value. if the double
	 * value is negative the color will be red. otherwise the color will be
	 * green. opacity is set to value.
	 * 
	 * @param value should be a double value below or equal 1.0
	 */
	private Color getColorByValue(double value) {

		double b = 0.0;
		double r = (value > 0) ? 0.0 : 1.0;
		double g = (value > 0) ? 1.0 : 0.0;

		double opacity = Math.abs(value);
		if (opacity > 1.0)
			opacity = 1.0;

		return Color.color(r, g, b, opacity);
	}

}
