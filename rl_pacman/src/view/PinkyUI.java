/**
 * 
 */
package view;

import model.environment.pacman.Pinky;

/**
 *
 * This class is responsible for the appearence of
 * {@link model.environment.pacman.Pinky} and corresponding animations.
 */
public class PinkyUI extends GhostUI {

	public PinkyUI(Pinky pinky, double blockSize, long moveDuration) {
		super(blockSize, moveDuration, "pinkyl.png", "pinkyr.png", "pinkyu.png", "pinkyd.png");

		assert (pinky != null);

		ghost = pinky;

		turnTo(this.getGhost().getLastAction());
	}
	

}
