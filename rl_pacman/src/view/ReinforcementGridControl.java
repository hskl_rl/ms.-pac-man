package view;

import java.io.IOException;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.util.converter.NumberStringConverter;
import javafx.beans.property.IntegerProperty;

/**
 * A small JavaFX custom control for handling and displaying reinforcement
 * learning task information.
 * */
public class ReinforcementGridControl extends GridPane {

	/** The corresponding javafx fxml file containing the view information **/
	private static final String FXML_FILE = "ReinforcementGrid.fxml";

	/**
	 * Label for the Task Name in the view file. refers to the id of the label
	 * in the fxml file.
	 */
	@FXML
	private Label lblTaskName;

	/**
	 * Label for the maze name in the view file. refers to the id of the label
	 * in the fxml file.
	 */
	@FXML
	private Label lblMazeName;

	/**
	 * Label for the training method in the view file. refers to the id of the
	 * label in the fxml file.
	 */
	@FXML
	private Label lblTrainingMethod;

	/**
	 * Label for the action selection method in the view file. refers to the id
	 * of the label in the fxml file.
	 */
	@FXML
	private Label lblActionSelectionMethod;

	/**
	 * Label for the number of total episodes in the view file. refers to the id
	 * of the label in the fxml file.
	 */
	@FXML
	private Label lblTotalEpisodes;

	/**
	 * Creates a custom control for displaying the reinforcement learning
	 * information in a task.
	 * */
	public ReinforcementGridControl() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(FXML_FILE));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	/**
	 * binds the textproperty of the task name label to a string property
	 * 
	 * @param value
	 *            the string property which should be bind to the label
	 * */
	public void bindTaskName(StringProperty value) {
		lblTaskName.textProperty().bind(value);
	}

	/**
	 * binds the textproperty of the maze name label to a string property
	 * 
	 * @param value
	 *            the string property which should be bind to the label
	 * */
	public void bindMazeName(StringProperty value) {
		lblMazeName.textProperty().bind(value);
	}

	/**
	 * binds the textproperty of the training method label to a string property
	 * 
	 * @param value
	 *            the string property which should be bind to the label
	 * */
	public void bindTrainingMethod(StringProperty value) {
		lblTrainingMethod.textProperty().bind(value);
	}

	/**
	 * binds the textproperty of the action selection method label to a string
	 * property
	 * 
	 * @param value
	 *            the string property which should be bind to the label
	 * */
	public void bindActionSelectionMethod(StringProperty value) {
		lblActionSelectionMethod.textProperty().bind(value);
	}

	/**
	 * binds the textproperty of the total episodes label to a integer property
	 * 
	 * @param value
	 *            the integer property which should be bind to the label
	 * */
	public void bindTotalEpisodes(IntegerProperty value) {
		lblTotalEpisodes.textProperty().bindBidirectional(value, new NumberStringConverter());
	}
}
