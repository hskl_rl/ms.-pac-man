package view;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

/**
 * Objects of this class represent a non-movable ghost UI.
 * This is just a stationary rectangle with the appearance of a random ghost.
 * Used within simulation mazes to display the negative reward fields.
 */
public class StaticGhostUI extends Rectangle {
	
	private static final Random rnd = new Random();
	private static final List<ImagePattern> ghostImages = new ArrayList<>();
	private static final ImagePattern blinky = new ImagePattern(new Image(GhostUI.GHOST_IMAGE_DIRECTORY_PATH + "blinkyd.png"));
	private static final ImagePattern inky = new ImagePattern(new Image(GhostUI.GHOST_IMAGE_DIRECTORY_PATH + "inkyd.png"));
	private static final ImagePattern pinky = new ImagePattern(new Image(GhostUI.GHOST_IMAGE_DIRECTORY_PATH + "pinkyd.png"));
	private static final ImagePattern clyde = new ImagePattern(new Image(GhostUI.GHOST_IMAGE_DIRECTORY_PATH + "clyded.png"));
	
	static{
		//used to determine random ghost image
		ghostImages.add( blinky );
		ghostImages.add( inky );
		ghostImages.add( pinky );
		ghostImages.add( clyde );
	}
	
	private Point gridPosition;
	
	public StaticGhostUI(Point gridPosition, double blockSize){
		super(blockSize, blockSize);
		
		this.gridPosition = gridPosition;
		
		//give it a random ghost image
		ImagePattern ghostImage = ghostImages.get(rnd.nextInt(ghostImages.size()));
		this.setFill(ghostImage);
	}
	
	public Point getGridPosition(){
		return this.gridPosition;
	}
}
