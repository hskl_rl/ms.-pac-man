/**
 * 
 */
package view;

import model.environment.pacman.Clyde;

/**
 *
 * This class is responsible for the appearence of
 * {@link model.environment.pacman.Clyde} and corresponding animations.
 */
public class ClydeUI extends GhostUI {

	public ClydeUI(Clyde clyde, double blockSize, long moveDuration) {
		super(blockSize, moveDuration, "clydel.png", "clyder.png", "clydeu.png", "clyded.png");

		assert (clyde != null);

		ghost = clyde;

		turnTo(this.getGhost().getLastAction());
	}

}
