/**
 * 
 */
package view;

import model.environment.pacman.Blinky;

/**
 *
 * This class is responsible for the appearence of
 * {@link model.environment.pacman.Blinky} and corresponding animations.
 */
public class BlinkyUI extends GhostUI {

	public BlinkyUI(Blinky blinky, double blockSize, long moveDuration) {
		super(blockSize, moveDuration, "blinkyl.png", "blinkyr.png", "blinkyu.png", "blinkyd.png");

		assert (blinky != null);

		ghost = blinky;

		turnTo(this.getGhost().getLastAction());
	}

}
