package view;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import model.Task;
import model.reinforcement_learning.AgentFactory;

public class TaskUIModel {

	private SimpleStringProperty name = new SimpleStringProperty("");
	private SimpleStringProperty episodes = new SimpleStringProperty("");
	private SimpleStringProperty trainingMethod = new SimpleStringProperty("");
	private SimpleStringProperty actionSelectionMethod = new SimpleStringProperty("");
	private SimpleStringProperty maze = new SimpleStringProperty("");
	private SimpleIntegerProperty totalEpisodes = new SimpleIntegerProperty(0);

	private Task correspondingTask;

	public TaskUIModel(Task task) {

		setTask(task);
		
	}
	
	public void setTask(Task task){
		// if task is null we only can use the default values (see above)
		if (task == null)
			return;

		correspondingTask = task;

		name = new SimpleStringProperty(task.getName() != null ? task.getName() : "");
		episodes = new SimpleStringProperty(task.getCurrentTrainingLevel()+"");
		
		if (task.getAgent() != null && task.getAgent().getPolicy() != null){
			actionSelectionMethod = new SimpleStringProperty(task.getAgent().getPolicy().toString());
			if (task.getAgent().getPolicy().getValueFunction() != null)
				trainingMethod = new SimpleStringProperty(task.getAgent().getPolicy().getValueFunction().toString());
		}
		
		if (task.getMazeName() != null)
			maze = new SimpleStringProperty(task.getMazeName());

		int currentTrainingLevel = task.getCurrentTrainingLevel();

		totalEpisodes = new SimpleIntegerProperty(currentTrainingLevel);
	}

	public SimpleStringProperty getName() {
		return name;
	}

	public SimpleStringProperty getEpisodes() {
		return episodes;
	}
	
	public void setName(SimpleStringProperty name) {
		if (name == null)
			return;

		this.name = name;
		correspondingTask.setName(this.name.get());
	}

	public SimpleStringProperty getTrainingMethod() {
		return trainingMethod;
	}

	public void setTrainingMethod(SimpleStringProperty trainingMethod) {
		if (trainingMethod == null || trainingMethod.get() == null)
			return;

		this.trainingMethod = trainingMethod;
		saveAgent();
	}

	public SimpleStringProperty getActionSelectionMethod() {
		return actionSelectionMethod;
	}

	public void setActionSelectionMethod(SimpleStringProperty actionSelectionMethod) {

		if (actionSelectionMethod == null || actionSelectionMethod.get() == null)
			return;

		this.actionSelectionMethod = actionSelectionMethod;
		
		saveAgent();
	}

	/**
	 * Creates a new agent based on actionSelectionMethod and TrainingMethod.
	 */
	private void saveAgent() {
		correspondingTask.setAgent(AgentFactory.createAPacManAgentWith(trainingMethod.toString(), actionSelectionMethod.toString()));
	}

	public SimpleStringProperty getMaze() {
		return maze;
	}

	public void setMaze(SimpleStringProperty maze) {
		if (maze == null || maze.get() == null)
			return;

		this.maze = maze;
		correspondingTask.setMazeName(maze.get());
	}

	/**
	 * @return the totalEpisodes
	 */
	public SimpleIntegerProperty getTotalEpisodes() {
		return totalEpisodes;
	}

	public void updateEpisodesLabel() {
		episodes.set(Integer.toString(correspondingTask.getCurrentTrainingLevel()));
	}

	public Task getTask() {
		return correspondingTask;
	}

	

}
